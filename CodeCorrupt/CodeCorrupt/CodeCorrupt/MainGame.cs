﻿#region Using Statements
using CodeCorrupt.Gameplay.Puzzle;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using CodeCorrupt.Gameplay.Character;
using CodeCorrupt.LevelEditor;
using System;
using System.Xml;
using System.Collections.ObjectModel;
using System.Text;
using System.IO;
#endregion

namespace CodeCorrupt
{
    public enum GameState { TitleScreen, NewGame,LoadGame,Puzzle, Dungeon, GameOver };
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MainGame : Game
    {
        public GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        //GameObject topwall; declaring a game object
        Texture2D puzzleScreen;
        Room room;
        Puzzle_IDE puzzleIDE;
        Vector2 ideGridOrigin = new Vector2();
        Vector2 inventoryDisplayOrigin = new Vector2(776, 89); //set top left hand corner origin for inventory
        Vector2 codeGridDisplayOrigin = new Vector2(0, 90);
        PuzzleButton newGameButton, loadGameButton, exitButton,cntButton;
        Rectangle EmptyBlock = new Rectangle(1, 247, 40, 40); //temporary coordinates
        bool dp = true;
        bool pp = true;
        SpriteFont spriteFont;
        SoundEffect MainDungeon,Puzzle;
        MouseState curState;
        Rectangle mouseRect;
        MouseState prevState;

        //inventory struct to keep 

        public struct Inventory
        {
            public string codeText;
            public int puzzleNum;
            public int lvlNum;
        }
        Inventory[] mInventory = new Inventory[10]; //temporary intialization
        public GameState gameState = GameState.TitleScreen;// GameState.TitleScreen;
        Character mainCharacter;
        Character shield;
        DungeonManager dungeonManager;

        Viewport viewportGameplay;
        Viewport viewportTitle;
        Script script;
        MessagePopUp dialog;
        private string controlsifo = "Controls:\nUse the Arrows Keys to Move\nPress SPACE to Interact with objects\nPress D to Attack\nUse the MOUSE to solve Puzzles\nPress ESC to Quit Game",
            storyinfo = "\nStory so far ...\nHello you are \"N\" an AntiVirus program.\nYour duty is to protect this Computer System\nfrom \"HackMan\". He has injected numerous viruses\ninto the system and removed Code from programs.\nOnly you can stop him!";
        public MainGame()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //KEEP THE BACKBUFFER Sizes to PROPER SCREEN RATIOS!!
            this.graphics.PreferredBackBufferWidth = 1024; //1368 Originally
            this.graphics.PreferredBackBufferHeight = 720; //700 Originally
            //this.graphics.IsFullScreen = true;
            graphics.GraphicsDevice.Viewport = new Viewport(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            puzzleIDE = new Puzzle_IDE(this, graphics);
            mainCharacter = new Character(this);
            shield = new Character(this);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            graphics.ApplyChanges();
            dungeonManager = new DungeonManager(this,this);
            Components.Add(dungeonManager);
            //Mouse.WindowHandle = this.Window.Handle;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            puzzleIDE.LoadContent(this);
            dungeonManager.LoadContent(this);
            spriteFont = Content.Load<SpriteFont>(@"CodePuzzle/big_font");
            MainDungeon = Content.Load<SoundEffect>("SoundFX/Dungeon");
            Puzzle = Content.Load<SoundEffect>("SoundFX/Puzzle (1)");
            newGameButton = new PuzzleButton(new Rectangle((graphics.GraphicsDevice.Viewport.Width / 2) - 100, (graphics.GraphicsDevice.Viewport.Height / 2) - 100, 200, 100),null);
            newGameButton.setTexture(Content.Load<Texture2D>(@"Images/new_game_button"));

            loadGameButton = new PuzzleButton(new Rectangle(newGameButton.Rect.X, newGameButton.Rect.Bottom + 10, 200, 100), null);
            loadGameButton.setTexture(Content.Load<Texture2D>(@"Images/load_game_button"));

            exitButton = new PuzzleButton(new Rectangle(loadGameButton.Rect.X, loadGameButton.Rect.Bottom + 10, 200, 100), null);
            exitButton.setTexture(Content.Load<Texture2D>(@"Images/exit_button"));

            cntButton = new PuzzleButton(exitButton.Rect,null);
            cntButton.setTexture(Content.Load<Texture2D>(@"Images/blank_button"));
            cntButton.setText("Continue", spriteFont,new Rectangle(cntButton.Rect.X + cntButton.Rect.Width / 6, cntButton.Rect.Y + cntButton.Rect.Height / 4, cntButton.Rect.Width, cntButton.Rect.Height));
            

            dialog = new MessagePopUp(viewportGameplay,this);  //make message box into a component so update method isn't necessary to call
            Components.Add(dialog);
            
            //room = new Room(Content); 
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            dungeonManager.Dispose();
        }


        //Read in scripts to be used in message trigger objects
        public Script ReadScript(string scriptName)
        {
            Script newScript;
            XmlDocument input = new XmlDocument();
            //Open the filestream for script file

             FileStream fs = new FileStream(@scriptName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);  

            input.Load(fs); //load filestream script file
            XmlNodeList dialogs = input.GetElementsByTagName("Dialog");
            Collection<Dialog> newDialogs = new Collection<Dialog>();

            foreach (XmlNode node in dialogs)
            {
                string name = "";
                string text = "";
                Vector2 arrowLoc = new Vector2();
                List<DialogHandler> handlers = new List<DialogHandler>();
                Dialog newDialog;
                foreach (XmlNode innerNode in node.ChildNodes) //looks through every child node of top node for name, test, or handler attribute
                {
                    if (innerNode.Name == "Name")
                        name = innerNode.Attributes["Text"].Value;
                    if (innerNode.Name == "Text")
                        text = innerNode.InnerText;
                    if (innerNode.Name == "Location") //location for arrows/tutorial messages
                    {
                        arrowLoc.X = Convert.ToInt32(innerNode.Attributes["lx"].Value);
                        arrowLoc.Y = Convert.ToInt32(innerNode.Attributes["ly"].Value);
                    }
                    if (innerNode.Name == "Handlers")
                    {
                        handlers = ReadHandlers(innerNode);
                    }
                }
                newDialog = new Dialog(name, text, arrowLoc, handlers.ToArray());
                newDialogs.Add(newDialog);
            }
            Dialog[] dialogsToAdd = new Dialog[newDialogs.Count];
            for (int i = 0; i < newDialogs.Count; i++) //create array for all dialogs to add to script
                dialogsToAdd[i] = newDialogs[i];
            newScript = new Script(dialogsToAdd);
            fs.Close();
            return newScript;
        }

        private List<DialogHandler> ReadHandlers(XmlNode innerNode) //returns list of all handlers in dialogue
        {
            List<DialogHandler> handlers = new List<DialogHandler>();
            foreach (XmlNode node in innerNode)
            {
                string text = "";
                string actions = "";
                if (node.Name == "Handler")
                {
                    text = node.Attributes["Text"].Value;
                    actions = node.Attributes["Actions"].Value;
                    List<DialogAction> dialogActions = new List<DialogAction>();
                    string[] methods = actions.Split(';'); //interpret 1 or more actions
                    string methodName = "";
                    object[] parameters = null;
                    foreach (string m in methods) //parse action string
                    {
                        if (m.Contains(":"))
                        {
                            string[] newActions = m.Split(':');
                            methodName = newActions[0];
                            parameters = (object[])newActions[1].Split(',');
                        }
                        else
                        {
                            methodName = m;
                            parameters = null;
                        }
                        dialogActions.Add(new DialogAction(methodName, parameters));
                    }
                    DialogHandler handler = new DialogHandler(text,
                        (DialogAction[])dialogActions.ToArray());
                    handlers.Add(handler);
                }
            }
            return handlers;
        }


        TimeSpan gameEnd = TimeSpan.Zero;
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        ///
        protected override void Update(GameTime gameTime)
        {

            prevState = curState;
            curState = Mouse.GetState();
            mouseRect = new Rectangle(curState.X, curState.Y, 5, 5);
            SoundEffectInstance instanceA = MainDungeon.CreateInstance();
            SoundEffectInstance instanceB = Puzzle.CreateInstance();
            // Was the mouse button pressed this frame?  
            bool mouseDown = curState.LeftButton == ButtonState.Pressed && prevState.LeftButton == ButtonState.Released;
            bool mouseUp = curState.LeftButton == ButtonState.Released && prevState.LeftButton == ButtonState.Pressed;

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (gameState == GameState.Puzzle)
            {
                //Window.Title = " " + Mouse.GetState().X + ", " + Mouse.GetState().Y;
                if (pp == true)
                {
                    instanceA.Volume = 0;
                    //instanceB.Play();
                    pp = false;
                    dp = true;
                }
                Window.Title = " " + puzzleIDE.BackEndUpdate(this);
                puzzleIDE.Update(gameTime, this);
            }
            else if (gameState == GameState.Dungeon)
            {
                // TODO: Add your update logic here
               // instanceB.IsLooped = false;
                if (dp == true)
                {
                    instanceB.Volume = 0;
                    instanceA.Play();
                    dp = false;
                    pp = true;
                }
                dungeonManager.Update(gameTime, this);
                if (dungeonManager.timeToSwitch == true)
                {
                    puzzleIDE.LoadPuzzle(this, dungeonManager.PuzzleFile, dungeonManager.getMainCharacter().getInventory());
                    gameState = GameState.Puzzle;
                    dungeonManager.timeToSwitch = false;
                }

            }
            else if (gameState == GameState.TitleScreen)
            {
                newGameButton.Update(mouseDown, mouseRect);
                if (newGameButton.isPressed)//( || Keyboard.GetState().IsKeyDown(Keys.Enter))
                {
                    gameState = GameState.NewGame;

                }
                loadGameButton.Update(mouseDown, mouseRect);
                exitButton.Update(mouseDown, mouseRect);
                if (exitButton.isPressed)
                    Exit();

            }
            else if (gameState == GameState.GameOver)
            {
                gameEnd += gameTime.ElapsedGameTime;
                TimeSpan delayTime = TimeSpan.FromSeconds(8);
                if (gameEnd > delayTime) //switch gamestate from gameOver back to title screen after 8 seconds
                    gameState = GameState.TitleScreen;
                if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.Y))
                {
                    dungeonManager = new DungeonManager(this,this);
                    dungeonManager.Initialize();
                    dungeonManager.LoadContent(this);

                    gameState = GameState.TitleScreen;
                }
                if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.N))
                {
                    gameState = GameState.GameOver;
                }


            }
            else if (gameState == GameState.NewGame)
            {
                cntButton.Update(mouseDown, mouseRect);
                if (cntButton.isPressed)// || Keyboard.GetState().IsKeyDown(Keys.Enter))
                {
                    gameState = GameState.Dungeon;
                }
            }
            //puzzle game is over by exit button
            if (puzzleIDE.puzzleEnd == true)
            {
                gameState = GameState.Dungeon;
                if (puzzleIDE.puzzleComplete)
                {
                    dungeonManager.onPuzzleCompletion();
                }
                else
                    dungeonManager.puzzleFail=puzzleIDE.puzzleFailed;
                puzzleIDE.puzzleEnd = false;
            }
            base.Update(gameTime);
        }
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            viewportGameplay = graphics.GraphicsDevice.Viewport;
            GraphicsDevice.Clear(Color.CornflowerBlue);

            if (gameState == GameState.TitleScreen)
            {
                spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied);
                // Draw background image
                GraphicsDevice.Clear(Color.SlateBlue);
                spriteBatch.DrawString(spriteFont, "Code Corrupt Ver 0.5", new Vector2(newGameButton.Rect.X - 200, newGameButton.Rect.Top - 120), Color.Black, 0, new Vector2(0), 2, SpriteEffects.None, 1);
                newGameButton.Draw(spriteBatch);
                loadGameButton.Draw(spriteBatch);
                exitButton.Draw(spriteBatch);
                spriteBatch.End();
            }
            else if (gameState == GameState.Puzzle)
            {

                //enable transparency and scissoring
                spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied);
                puzzleIDE.Draw(spriteBatch, this, viewportGameplay, gameTime);
                //rasterizer_state.ScissorTestEnable = false;
                spriteBatch.End();
            }
            else if (gameState == GameState.Dungeon)
            {
                dungeonManager.Draw(gameTime);
            }
            else if (gameState == GameState.GameOver)
            {
                spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied);
                spriteBatch.DrawString(spriteFont, "GAME OVER.... PLAY AGAIN (Y or N)", new Vector2(viewportGameplay.Width / 2 - 100, viewportGameplay.Height / 2), Color.White);
                spriteBatch.End();
            }
            else if (gameState == GameState.NewGame)
            {
                spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied);
                GraphicsDevice.Clear(Color.SlateBlue);
                spriteBatch.DrawString(spriteFont, "Controls:\nUse the Arrows Keys to Move\nPress SPACE to Interact with objects\nPress D to Attack\nUse the MOUSE to solve Puzzles\nPress ESC to Quit Game", new Vector2(viewportGameplay.Width / 3, viewportGameplay.Height / 3), Color.Black, 0, new Vector2(0), 0.8f, SpriteEffects.None, 0f);
                cntButton.Draw(spriteBatch);
                spriteBatch.End();
            }
            base.Draw(gameTime);
        }
    }

}
﻿#region Using Statements
using CodeCorrupt.Gameplay.Puzzle;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using CodeCorrupt.Gameplay.Character;
using CodeCorrupt.LevelEditor;
#endregion

namespace CodeCorrupt.LevelEditor
{
    public class DungeonManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        Level level;
        Room currentRoom;
        Character mainCharacter;
        Character Character_Attack; // Only one instance Should have list of attacks soon that do different things
        Enemy enemy;
        EvasiveClass malware;
        Rectangle charbox = new Rectangle(50, 50, 200, 100);
        Game game;
        MainGame mGame;
        const int knockbackvelocity = 7;
        ContentManager content;
        List<SpriteClass> spriteList = new List<SpriteClass>();
        List<Shot> shotList = new List<Shot>();
        List<Turret> turrets = new List<Turret>(); //TEMPORARY
        List<Spawner> spawners = new List<Spawner>();//TEMPORARY
        List<TripWire> trips = new List<TripWire>(); //TEMORARY
        bool enteringRoom = false;
        MouseState previousMouseState, mouseState;
        KeyboardState currentKeyboardState, previousKeyboardState;
        Texture2D healthbar, healthbar2, background, fieldBackground, pausePic, e_hlthind, c_hlthind, hp_mid, hp_crit;
        bool isenemyhit = false;
        bool ischarhit = false; // indicate if character or enemy is hit affects draw function
        Vector2 temp_char, temp_enemy;
        SpriteBatch mBatch;
        int i = 0;
        SoundEffect hit, shoot;
        Vector2 vector, e_vector,m_vector;
        int x, y; // for the x and y coordinates
        public bool timeToSwitch = false, paused = false,puzzleFail=false;
        List<Terminal> terminalObjects = new List<Terminal>(); //list of terminals in current room
        int wander_dis = 0;
        MessagePopUp messageBox;
        messageTriggerObj saveMsgTrigger;
        string[] tutorialMessages = { "null", "null" };
        bool showDialog = true;
        SpriteFont spriteFont, font_large; // The Sprite Font assest used to draw text via spriteBatch
        /***Variables used to create CodePuzzle ****/
        string puzzlefname = "";

        //Temporary Bool flags to enable HPbar & cannon
        bool enableHp = false, enableCannon = false;
        
        enum EnemyAIState
        {
            PURSUE, // Follow the enemy around
            WANDER,
            EVADE, //Run away from the enemy
            DIE
        }


        public DungeonManager(Game ingame, MainGame mgame)
            : base(ingame)
        {
            game = ingame;
            mGame = mgame;
            mainCharacter = new Character(game);
            Character_Attack = new Character(game);
            content = ingame.Content;
        }

        public override void Initialize()
        {
            level = new Level();
            level.getRooms().Add(new Room(content, "Tutorial1.xml", game));
            level.getNames().Add("Tutorial1.xml");
            currentRoom = level.getRooms().ToArray()[level.getNames().IndexOf("Tutorial1.xml")];
            messageBox = new MessagePopUp((game as MainGame).GraphicsDevice.Viewport,game); //700,100
            //turrets.Add(new Turret(game.Content.Load<Texture2D>(@"LevelObjects/turretSprite"), new Point(50, 50), new Point(700, 100), 4000, game)); //TEMPORARY
            //spawners.Add(new InfiniteSpawner(new Point(700, 500), new Enemy(game), game, 10000, 5000, true)); //TEMPORARY
            //spawners.Add(new Spawner(new Point(700, 500), new Enemy(game), game)); //TEMPORARY
            //spawners.Add(new Spawner(new Point(700, 300), new Enemy(game), game)); //TEMPRARY
            //spawners.Add(new Spawner(new Point(700, 100), new Enemy(game), game)); //TEMPORARY
            //trips.Add(new TripWire(spawners, new Rectangle(250, 490, 200, 20))); //Temporary
            //trips.Add(new TripWire(spawners, new Rectangle(250, 660, 200, 20))); //Temporary
            base.Initialize();
        }

        public void LoadContent(Game game)
        {
            spriteBatch = new SpriteBatch(game.GraphicsDevice);
            mainCharacter.LoadContent(game);
            messageBox.LoadContent(game);
            //malware.LoadContent(game);
            Character_Attack.LoadContent(game);
            mBatch = new SpriteBatch(game.GraphicsDevice);
            healthbar = game.Content.Load<Texture2D>(@"Images/healthbar"); // Loaging the sprite
            healthbar2 = game.Content.Load<Texture2D>(@"Images/healthbar2"); // Loaging the sprite
            hp_mid = game.Content.Load<Texture2D>(@"Images/hp_mid"); // Loaging the sprite
            hp_crit = game.Content.Load<Texture2D>(@"Images/hp_crit"); // Loaging the sprite
            font_large = game.Content.Load<SpriteFont>(@"CodePuzzle/big_font");
            background = game.Content.Load<Texture2D>(@"LevelObjects/bkg1-2");
            fieldBackground = game.Content.Load<Texture2D>(@"LevelObjects/bkglayer2");
            pausePic = game.Content.Load<Texture2D>(@"LevelObjects/pauseScreen");
            e_hlthind = game.Content.Load<Texture2D>(@"Images/Enemy hit");
            c_hlthind = game.Content.Load<Texture2D>(@"Images/Player hit");
            mainCharacter.ringsPosition = new Vector2(55f, 418f);
            hit = game.Content.Load<SoundEffect>("SoundFX/hit");
            shoot = game.Content.Load<SoundEffect>("SoundFX/shot");
            SoundEffectInstance instanceA = hit.CreateInstance();
            SoundEffectInstance instanceB = shoot.CreateInstance();
        }

        public void Update(GameTime gameTime, MainGame mGame)
        {
            previousKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();
            //malware.e_22pos.X = 500;
            //malware.e_22pos.Y = 500; // temporary testing 
            //malware.Update(gameTime);
            if (paused)
            {
                if (currentKeyboardState.IsKeyUp(Keys.P) && previousKeyboardState.IsKeyDown(Keys.P))
                {
                    paused = false;
                }
                return;
            }
            else
            {
                if (currentKeyboardState.IsKeyUp(Keys.P) && previousKeyboardState.IsKeyDown(Keys.P))
                {
                    paused = true;
                }
            }
            if (i < 1) { mainCharacter.ringsPosition.X = 350; mainCharacter.ringsPosition.Y = 150; i++; }

            game.Window.Title = "[ " + mouseState.X.ToString() + " " + mouseState.Y.ToString() + " ]";

            previousMouseState = mouseState;
            mouseState = Mouse.GetState();
            if (messageBox.ShowMessage == false)
            { //stop update for player if tutorial is shown
                mainCharacter.Update(gameTime);
                temp_char.X = mainCharacter.ballPosition.X;
                temp_char.Y = mainCharacter.ballPosition.Y;
                if (currentKeyboardState.IsKeyUp(Keys.W) && previousKeyboardState.IsKeyDown(Keys.W))
                {
                    foreach (RoomEntrance door in currentRoom.GetEntrances())
                    {
                        if (door.GetSide() == "entranceRight")
                        {
                            currentRoom = new Room(content, door.GetFileName(), game);
                            if (door.GetFileName() == "Tutorial1.xml")
                            {
                                tutorialMessages[0] = "null";
                            }
                            else
                            {
                                tutorialMessages[0] = "stuff.";
                            }
                        }
                    }
                }
                if (currentKeyboardState.IsKeyUp(Keys.Q) && previousKeyboardState.IsKeyDown(Keys.Q))
                {
                    foreach (RoomEntrance door in currentRoom.GetEntrances())
                    {
                        if (door.GetSide() == "entranceLeft")
                        {
                            currentRoom = new Room(content, door.GetFileName(), game);
                            if (door.GetFileName() == "Tutorial1.xml")
                            {
                                tutorialMessages[0] = "null";
                            }
                            else
                            {
                                tutorialMessages[0] = "stuff.";
                            }
                        }
                    }
                }
                //handle player input function
                if (enableCannon && currentKeyboardState.IsKeyUp(Keys.D) && previousKeyboardState.IsKeyDown(Keys.D))
                {
                    if (mainCharacter.canShoot)
                    {
                        switch (mainCharacter.shoot_direction)
                        {
                            case "up":
                                //shotList.Add(new Shot(game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"), new Vector2(mainCharacter.ringsPosition.X + (mainCharacter.getFrameSize().X / 2) - (68 / 2), mainCharacter.ringsPosition.Y - 5), new Vector2(0, -1), 25, 500, game));
                                shotList.Add(new Shot(game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"), new Vector2(mainCharacter.ringsPosition.X + (mainCharacter.getFrameSize().X / 2) - (68 / 2), mainCharacter.ringsPosition.Y - 5), new Point(68, 69), new Point(50, 50), new Vector2(0, -1), 25, 500, game, false, true));
                                break;
                            case "down":
                                //shotList.Add(new Shot(game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"), new Vector2(mainCharacter.ringsPosition.X + (mainCharacter.getFrameSize().X / 2) - (68 / 2), mainCharacter.ringsPosition.Y + mainCharacter.getFrameSize().Y / 2), new Vector2(0, 1), 25, 500, game));
                                shotList.Add(new Shot(game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"), new Vector2(mainCharacter.ringsPosition.X + (mainCharacter.getFrameSize().X / 2) - (68 / 2), mainCharacter.ringsPosition.Y + mainCharacter.getFrameSize().Y / 2), new Point(68, 69), new Point(50, 50), new Vector2(0, 1), 25, 500, game, false, true));
                                // 0,-1 x = char x +  (char width)/2  - (shotw/2) ... y+ char height + 5
                                break;
                            case "left":
                                //shotList.Add(new Shot(game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"), new Vector2(mainCharacter.ringsPosition.X - 5, mainCharacter.ringsPosition.Y + (mainCharacter.getFrameSize().Y / 2) - (69 / 2)), new Vector2(-1, 0), 25, 500, game));
                                shotList.Add(new Shot(game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"), new Vector2(mainCharacter.ringsPosition.X - 5, mainCharacter.ringsPosition.Y + (mainCharacter.getFrameSize().Y / 2) - (69 / 2)), new Point(68, 69), new Point(50, 50), new Vector2(-1, 0), 25, 500, game, false, true));
                                // -1,0 x = char.x - 5 ...  y = char.y + (char height)/2 - (shot height/2)
                                break;
                            case "right":
                                //shotList.Add(new Shot(game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"), new Vector2(mainCharacter.ringsPosition.X + mainCharacter.getFrameSize().X / 2, mainCharacter.ringsPosition.Y + (mainCharacter.getFrameSize().Y / 2) - (69 / 2)), new Vector2(1, 0), 25, 500, game));
                                shotList.Add(new Shot(game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"), new Vector2(mainCharacter.ringsPosition.X + mainCharacter.getFrameSize().X / 2, mainCharacter.ringsPosition.Y + (mainCharacter.getFrameSize().Y / 2) - (69 / 2)), new Point(68, 69), new Point(50, 50), new Vector2(1, 0), 25, 500, game, false, true));
                                // 1,0 x = char.x + charwidth + 5 y = char.y + (char height)/2 - (shot height/2)
                                break;
                                
                        }
                        mainCharacter.canShoot = false;
                        mainCharacter.rechargeLeft = mainCharacter.rechargeTime;
                    }
                }
                foreach (Shot s in shotList)
                {
                    s.Update(gameTime);
                    if (s.shotExists == false)
                    {
                        shotList.Remove(s);
                        break;
                    }

                    if (s.hitsPlayer)
                    {
                        if (mainCharacter.getRectangle().Intersects(s.Shot_Rec))
                        {
                            hit.Play();
                            mainCharacter.c_health -= s.Damage;
                            mainCharacter.ringsPosition += 30 * Vector2.Normalize(new Vector2(mainCharacter.ringsPosition.X - s.Shot_Rec.X, mainCharacter.ringsPosition.Y - s.Shot_Rec.Y));
                            shotList.Remove(s);
                            break;
                        }
                    }
                }

                foreach (LevelObject obj in currentRoom.GetObjects())
                {
                    if (mainCharacter.Collide(obj))
                    {
                        if (obj is PuzzlePiece)
                        {
                            mainCharacter.getInventory().Add((PuzzlePiece)obj);
                            currentRoom.GetObjects().Remove(obj);
                            break;
                        }
                        if (currentKeyboardState.IsKeyUp(Keys.G))
                        {
                            mainCharacter.ringsPosition = mainCharacter.prevPosition;
                        }

                    }

                    if (obj is Terminal)
                    {
                        if (mainCharacter.getRectangle().Intersects((obj as Terminal).getVicinity()))
                        {
                            if (currentKeyboardState.IsKeyUp(Keys.Space) && previousKeyboardState.IsKeyDown(Keys.Space) && (currentRoom.GetEnemies().Count < 1 || currentRoom.allowAllTerminal))
                            {
                                //Get the file name of CodePuzzle 
                                currentRoom.CurrentTerminal = ((Terminal)obj);
                                puzzlefname = ((Terminal)obj).FileName;
                                timeToSwitch = true;
                            }
                        }
                        //Code for changing door status based upon if a certain puzzle has been completed
                        //also code for opening a locked door.
                        (obj as Terminal).checkPuzzle(mainCharacter, gameTime, game);
                        if (puzzleFail)
                        {
                            currentRoom.GetEnemies().AddRange((obj as Terminal).activateSpawner());
                            puzzleFail = false;
                        }
                        if ((obj as Terminal).enableCannon)
                            enableCannon = true;
                        if ((obj as Terminal).enableHp)
                            enableHp = true;
                    }

                    if (obj is SaveFileHandler)
                    {
                        if (mainCharacter.getRectangle().Intersects((obj as SaveFileHandler).Vicinity))
                        {
                            if (currentKeyboardState.IsKeyUp(Keys.Space) && previousKeyboardState.IsKeyDown(Keys.Space) && (currentRoom.GetEnemies().Count < 1 || currentRoom.allowAllTerminal))
                            {
                                (obj as SaveFileHandler).saveCharacterData(mainCharacter.getRingsPosition(), currentRoom.roomFileName,mainCharacter.getInventory());
                            }
                        }
                        //Message Trigger startdialog

                    }

                    if (obj is Turret)// doesn't actually exist yet
                    {
                        if (((Turret)obj).readyToFire)
                        {
                            shotList.Add(((Turret)obj).fireAtTarget(mainCharacter));
                            ((Turret)obj).suspendFire();
                        }
                        foreach (Shot s in shotList)
                        {
                            if (s.hitsEnemy)
                            {
                                
                                if (obj.GetDrawBox().Intersects(s.Shot_Rec))
                                {
                                    hit.Play();
                                    ((Turret)obj).damageTurret(s.Damage);
                                    shotList.Remove(s);
                                    break;
                                }
                            }
                        }
                        ((Turret)obj).update(gameTime); //doesn't actually exist yet that is turrets as parts of a room.
                        if (!((Turret)obj).getExists())
                        {
                            currentRoom.GetObjects().Remove(obj);
                            break;
                        }


                    }

                }
            }
            foreach (Turret t in turrets)//TEMPORARY
            {
                if (t.readyToFire)
                {
                    shotList.Add(t.fireAtTarget(mainCharacter));
                    t.suspendFire();
                }
                t.update(gameTime); //doesn't actually exist yet that is turrets as parts of a room.
            }//TEMPORARY
            foreach (Spawner IS in currentRoom.GetSpawns())//TEMPORARY
            {
                if (IS is InfiniteSpawner)
                {
                    if ((IS as InfiniteSpawner).spawnNow)
                    {
                        currentRoom.GetEnemies().Add(IS.spawnEnemy());
                        (IS as InfiniteSpawner).suspendSpawn();
                    }
                    (IS as InfiniteSpawner).update(gameTime);
                }
            }//TEMPORARY
            foreach (TripWire wire in currentRoom.GetTrips()) //Temporary
            {
                if (!wire.tripped && mainCharacter.getRectangle().Intersects(wire.getTripArea()))
                {
                    foreach (Spawner spawn in wire.getSpawnList())
                    {
                        currentRoom.GetEnemies().Add(spawn.spawnEnemy());
                    }
                    wire.tripped = true;
                }
            } //TEMPORARY
            foreach (messageTriggerObj mObj in currentRoom.GetMessageObjects())
            {
                mObj.HandlePlayerInteraction(mainCharacter);  //handle collision of character with message trigger
                mObj.Update(gameTime);
                if (mObj.showingDialog)
                {
                    //Messages to be displayed by messagebox during tutorials 
                    //UpdateTutorialMsgs(roomName);
                    messageBox.ShowMessage = true;
                    break;
                }
                else
                    messageBox.ShowMessage = false;
            }             //showDialog = messageBox.Update(showDialog);



            if (mainCharacter.c_health <= 0) //set gameOver state
            {
                mainCharacter.c_health = mainCharacter.max_health;
                mGame.gameState = GameState.GameOver;
            }

            foreach (Wall w in currentRoom.GetWalls())
            {
                if (lineRecIntersect(mainCharacter.getRectangle(), w.point1, w.point2))
                {
                    if (currentKeyboardState.IsKeyUp(Keys.G))
                        mainCharacter.ringsPosition = mainCharacter.prevPosition;
                }
                foreach (Shot s in shotList) //show stops at wall
                {
                    if (lineRecIntersect(s.Shot_Rec, w.point1, w.point2))
                    {
                        shotList.Remove(s);
                        break;
                    }
                }
            }
            //CHECKING THE ENTRANCES TO SEE IF WE NEED TO DO SOME ROOM SWITCHING!
            foreach (RoomEntrance e in currentRoom.GetEntrances())
            {
                /* if (level.getNames()[0]== "Tutorial1.txt")
                 {
                             tutorialMessages[0] = "You've arrived inside the mainframe. The door seems locked.";
                             tutorialMessages[1] = "Pick up that yellow code piece.  \nUse that terminal to open the door (Access using spacebar).";
                             messageBox.messages = tutorialMessages;
                 }*/

                if (e.isLockStatus == false && e.GetDrawBox().Intersects(new Rectangle((int)mainCharacter.ringsPosition.X, (int)mainCharacter.ringsPosition.Y, mainCharacter.getFrameSize().X, mainCharacter.getFrameSize().Y)))
                {
                    if (!enteringRoom)
                    {
                        //save the type of door you just used so you know which side of the room to come in on
                        string side = e.GetSide();
                        switch (side)
                        {
                            case "entranceBottom":
                                side = "entranceTop";
                                break;
                            case "entranceTop":
                                side = "entranceBottom";
                                break;
                            case "entranceLeft":
                                side = "entranceRight";
                                break;
                            case "entranceRight":
                                side = "entranceLeft";
                                break;
                        }

                        string roomName = e.GetFileName();
                        //SUPER TEMPORARY till i can get a better background tile thing
                        if (roomName == "Tutorial1.xml")
                        {
                            tutorialMessages[0] = "null";
                        }
                        else
                        {
                            tutorialMessages[0] = "stuff.";
                        }
                        //SUPER TEMPORARY
                        if (!level.getNames().Contains(e.GetFileName()) || e.GetFileName() == "Tutorial1") //attribute for room needs to be made that will indicate if room is the first room of the level
                        {
                            //if room has not been visitied before
                            level.getNames().Add(e.GetFileName());
                            level.getRooms().Add(new Room(content, e.GetFileName(), game));
                            currentRoom = level.getRooms().ToArray()[level.getNames().IndexOf(e.GetFileName())];
                        }
                        else
                        {
                            //switch to current room
                            currentRoom = level.getRooms().ToArray()[level.getNames().IndexOf(e.GetFileName())];
                        }
                        shotList.Clear();
                        //set the characters position to the proper point on the screen to be coming in the right door.
                        foreach (RoomEntrance p in currentRoom.GetEntrances())
                        {
                            if (p.GetSide() == side)
                            {
                                switch (p.GetSide())
                                {
                                    case "entranceLeft":
                                        mainCharacter.ringsPosition.X = p.location.X + p.dimensions.X;
                                        mainCharacter.ringsPosition.Y = p.location.Y + p.dimensions.Y / 2 - mainCharacter.getFrameSize().Y / 2;
                                        break;
                                    case "entranceRight":
                                        mainCharacter.ringsPosition.X = p.location.X - mainCharacter.getFrameSize().X;
                                        mainCharacter.ringsPosition.Y = p.location.Y + p.dimensions.Y / 2 - mainCharacter.getFrameSize().Y / 2;
                                        break;
                                    case "entranceBottom":
                                        break;
                                    case "entranceTop":
                                        break;
                                }
                                //mainCharacter.ringsPosition.X = p.location.X + p.dimensions.X / 2 - mainCharacter.getFrameSize().X / 2;
                                //mainCharacter.ringsPosition.Y = p.location.Y + p.dimensions.Y / 2 - mainCharacter.getFrameSize().Y / 2;
                            }
                        }
                        enteringRoom = true;
                        break;
                    }
                }
            }

            if (enteringRoom)
            {
                enteringRoom = false;
                foreach (RoomEntrance d in currentRoom.GetEntrances())
                {
                    if (d.GetDrawBox().Intersects(new Rectangle((int)mainCharacter.ringsPosition.X, (int)mainCharacter.ringsPosition.Y, mainCharacter.getFrameSize().X, mainCharacter.getFrameSize().Y)))
                        enteringRoom = true;
                }
            }

            foreach (Enemy e in currentRoom.GetEnemies())
            {
                //if (!showDialog)  //if dialog is not showing, enemies can move.
                    e.Update(gameTime);
                //e.Evade_Hys(mainCharacter);
                e.Pursue_Hys(mainCharacter);

                if (mainCharacter.E_Collide(e) && e.E_IsAlive == true) // E_IsAlive is function
                {
                    //mainCharacter.ringsPosition = Vector2.Zero;
                    hit.Play();
                    float t_x = mainCharacter.getRingsPosition().X; // temporary x and y variables to make sure there are no glitches
                    float t_y = mainCharacter.getRingsPosition().Y;
                    ischarhit = true;
                    mainCharacter.c_health = mainCharacter.c_health - 10;
                    //vector = Vector2.Reflect(mainCharacter.getRingsPosition(), e.enemyPos);
                    vector = Vector2.Normalize(new Vector2(mainCharacter.getRingsPosition().X - e.enemyPos.X, mainCharacter.getRingsPosition().Y - e.enemyPos.Y)); 
                    //e_vector = Vector2.Reflect(e.enemyPos, mainCharacter.getRingsPosition());
                    e_vector = vector * -1;
                    //vector.X = vector.X * -1;
                    //vector.Y = vector.Y * -1; // not changing this
                    mainCharacter.setRingsPosition(e_vector.X, e_vector.Y);
                    foreach (LevelObject o in currentRoom.GetObjects())
                    {
                        if (mainCharacter.Collide(o))
                        {
                            mainCharacter.setRingsPosition(t_x, t_y);
                        }
                    }
                    e.set_e_pos(e_vector);
                }
                foreach (Shot s in shotList)
                {
                    if (s.hitsEnemy && e.Collide(s))
                    {
                        hit.Play();
                        isenemyhit = true;
                        temp_enemy.X = e.enemyPos.X;
                        temp_enemy.Y = e.enemyPos.Y;
                        float te_x = e.enemyPos.X; // temporary x and y variables to make sure there are no glitches
                        float te_y = e.enemyPos.Y;
                        e.health = e.health - 10;
                        shotList.Remove(s);

                        if (e.health <= 0)
                        {
                            e.E_IsAlive = false;
                        }
                        else
                        {
                            //e_vector = Vector2.Reflect(e.enemyPos, s.Position);
                            e_vector = Vector2.Normalize(new Vector2(e.enemyPos.X - s.Position.X, e.enemyPos.Y - s.Position.Y));
                            e.set_e_pos(e_vector); // segmen *3
                            foreach (LevelObject ob in currentRoom.GetObjects())
                            {
                                if (e.Collide(ob))
                                {
                                    e.enemyPos = e.e_prevpos;
                                }
                            }
                        }

                        break;
                    }
                }
                if (e.E_IsAlive == false)
                {
                    currentRoom.GetEnemies().Remove(e);
                    break;
                }
                foreach (Wall w in currentRoom.GetWalls())
                {
                    if(lineRecIntersect(e.getRectangle(), w.point1, w.point2))
                    {
                        e.enemyPos = e.e_prevpos;
                        
                    }
                }
                foreach (LevelObject o in currentRoom.GetObjects())
                {
                    if (e.Collide(o))
                    {
                        e.enemyPos = e.e_prevpos;
                    }
                }
            }
            base.Update(gameTime);

        }

        public void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Gray);
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied); //Enable transparency
            //messageBox.Draw(spriteBatch);
            spriteBatch.Draw(background, new Rectangle(0, 0, game.Window.ClientBounds.Width, game.Window.ClientBounds.Height), new Rectangle(0, 0, background.Width, background.Height), Color.White, 0f, Vector2.Zero, SpriteEffects.None, .999f);
            //make this draw only  as a test for the first room
            if (tutorialMessages[0] == "null")
                spriteBatch.Draw(fieldBackground, new Rectangle(160, 80, 740, 580), new Rectangle(0, 0, fieldBackground.Width, fieldBackground.Height), Color.White, 0f, Vector2.Zero, SpriteEffects.None, .998f);
            if (mainCharacter.c_health > 0)
            {
                currentRoom.Draw(gameTime, spriteBatch);
                mainCharacter.Draw(gameTime, spriteBatch);
                if (enableHp)
                {

                    if ((mainCharacter.c_health >= (.3 * mainCharacter.max_health)) && (mainCharacter.c_health < (.6 * mainCharacter.max_health)))
                    {
                        healthbar = hp_mid;
                        spriteBatch.Draw(healthbar2, new Rectangle(10, 10, (game.Window.ClientBounds.Width - 800) * mainCharacter.max_health / mainCharacter.max_health, healthbar.Bounds.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.11f);
                        spriteBatch.Draw(healthbar, new Rectangle(10, 10, (game.Window.ClientBounds.Width - 800) * mainCharacter.c_health / mainCharacter.max_health, healthbar.Bounds.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.10f);
                    }

                    else if (mainCharacter.c_health < (.3 * mainCharacter.max_health))
                    {
                        healthbar = hp_crit;
                        spriteBatch.Draw(healthbar2, new Rectangle(10, 10, (game.Window.ClientBounds.Width - 800) * mainCharacter.max_health / mainCharacter.max_health, healthbar.Bounds.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.11f);
                        spriteBatch.Draw(healthbar, new Rectangle(10, 10, (game.Window.ClientBounds.Width - 800) * mainCharacter.c_health / mainCharacter.max_health, healthbar.Bounds.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.10f);
                    }
                    else
                    {
                        spriteBatch.Draw(healthbar2, new Rectangle(10, 10, (game.Window.ClientBounds.Width - 800) * mainCharacter.max_health / mainCharacter.max_health, healthbar.Bounds.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.11f);
                        spriteBatch.Draw(healthbar, new Rectangle(10, 10, (game.Window.ClientBounds.Width - 800) * mainCharacter.c_health / mainCharacter.max_health, healthbar.Bounds.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.10f);
                        //spriteBatch.DrawString(font_large, mainCharacter.getHealth().ToString(), new Vector2((int)mainCharacter.getRingsPosition().X, (int)mainCharacter.getRingsPosition().Y - 20), Color.WhiteSmoke, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.9f);
                    }
                }
                foreach (Enemy e in currentRoom.GetEnemies())
                {
                    e.Draw(gameTime, spriteBatch);
                }
                if (enableCannon)
                {
                    foreach (Shot s in shotList)
                    {
                        s.Draw(spriteBatch);

                    }
                }
                foreach (Turret t in turrets)
                {
                    t.Draw(spriteBatch);
                }
                if (ischarhit == true)
                {
                    for (int a = 0; a < 4; a++)
                    {
                        spriteBatch.Draw(c_hlthind, new Rectangle((int)temp_char.X, (int)temp_char.Y, 71, 64), Color.White);
                    }
                    ischarhit = false;
                }
                if (isenemyhit == true)
                {
                    for (int b = 0; b < 4; b++)
                    {
                        spriteBatch.Draw(e_hlthind, new Rectangle((int)temp_enemy.X, (int)temp_enemy.Y, 71, 64), Color.White);
                    }
                    isenemyhit = false;
                }
                if (paused)
                {
                    spriteBatch.Draw(pausePic, new Rectangle(game.Window.ClientBounds.Width / 2 - pausePic.Width / 2, game.Window.ClientBounds.Height / 2 - pausePic.Height / 2, pausePic.Width, pausePic.Height), new Rectangle(0, 0, pausePic.Width, pausePic.Height), Color.White, 0f, Vector2.Zero, SpriteEffects.None, .001f);
                }
                spriteBatch.End();

                base.Draw(gameTime);
            }
        }

        bool lineRecIntersect(Rectangle rectangle, Vector2 point1, Vector2 point2)
        {
            Vector2 side1, side2, intersect;
            //check left side
            side1.X = rectangle.X;
            side1.Y = rectangle.Y;
            side2.X = rectangle.X;
            side2.Y = rectangle.Y + rectangle.Height;
            if (Intersects(side1, side2, point1, point2, out intersect))
                return true;
            //check top side
            side1.X = rectangle.X;
            side1.Y = rectangle.Y;
            side2.X = rectangle.X + rectangle.Width;
            side2.Y = rectangle.Y;
            if (Intersects(side1, side2, point1, point2, out intersect))
                return true;
            //check right side
            side1.X = rectangle.X + rectangle.Width;
            side1.Y = rectangle.Y;
            side2.X = rectangle.X + rectangle.Width;
            side2.Y = rectangle.Y + rectangle.Height;
            if (Intersects(side1, side2, point1, point2, out intersect))
                return true;
            //check bottom side
            side1.X = rectangle.X;
            side1.Y = rectangle.Y + rectangle.Height;
            side2.X = rectangle.X + rectangle.Width;
            side2.Y = rectangle.Y + rectangle.Height;
            if (Intersects(side1, side2, point1, point2, out intersect))
                return true;
            return false;
        }

        //copy pasted line-segment to line-segment intersection
        // a1 is line1 start, a2 is line1 end, b1 is line2 start, b2 is line2 end
        bool Intersects(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2, out Vector2 intersection)
        {
            intersection = Vector2.Zero;

            Vector2 b = a2 - a1;
            Vector2 d = b2 - b1;
            float bDotDPerp = b.X * d.Y - b.Y * d.X;

            // if b dot d == 0, it means the lines are parallel so have infinite intersection points
            if (bDotDPerp == 0)
                return false;

            Vector2 c = b1 - a1;
            float t = (c.X * d.Y - c.Y * d.X) / bDotDPerp;
            if (t < 0 || t > 1)
                return false;

            float u = (c.X * b.Y - c.Y * b.X) / bDotDPerp;
            if (u < 0 || u > 1)
                return false;

            intersection = a1 + t * b;

            return true;
        }

        /// <summary>
        /// Gets the file name of the code puzzle from the last terminal MainCharacter interacted with
        /// </summary>
        public string PuzzleFile
        {
            get { return puzzlefname; }
        }

        public void onPuzzleCompletion()
        {
            if (currentRoom.CurrentTerminal != null)
                currentRoom.CurrentTerminal.isPuzzleComplete = true;
        }

        /// <summary>
        /// Gets the MainCharacters Code Inventory
        /// </summary>
        public Character getMainCharacter()
        {
            return mainCharacter;
        }
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace CodeCorrupt.LevelEditor
{
    class TreasureChest : LevelObject
    {
        Rectangle vicinity;
        public TreasureChest(Texture2D Texture, Point Dimensions, Point Location)
            : base(Texture, Dimensions, Location)
        {
            vicinity = new Rectangle(Location.X-50, Location.Y-50, Dimensions.X+100, Dimensions.Y+100);
        }
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using CodeCorrupt.LevelEditor;
using CodeCorrupt.Gameplay.Character;
#endregion

namespace CodeCorrupt.LevelEditor
{
    public class Spawner
    {
        Point spawnPoint;
        Enemy enemy;
        Game game;

        public Spawner(Point SpawnPoint, Enemy Badguy, Game inputGame)
        {
            spawnPoint = SpawnPoint;
            enemy = Badguy;
            game = inputGame;
        }

        public Enemy spawnEnemy()
        {
            return new Enemy(game, new Vector2(spawnPoint.X, spawnPoint.Y), enemy.getEnemyTexture(), enemy.getEnemyFrameSize());
        }
    }
}

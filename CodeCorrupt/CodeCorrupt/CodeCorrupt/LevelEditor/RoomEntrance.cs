﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace CodeCorrupt.LevelEditor
{
    class RoomEntrance : LevelObject
    {
        string entranceSide;
        string adjacent;
        bool doorLocked = true;
        Texture2D unlockedTexture;

        public RoomEntrance(Texture2D Texture, Point Dimensions, Point Location)
            : base(Texture, Dimensions, Location)
        {
        }

        public RoomEntrance(Texture2D lockTexture, Point Dimensions, Point Location, string fileName, string side, bool locked)
            : base(lockTexture, Dimensions, Location)
        {
            adjacent = fileName;
            entranceSide = side;
            doorLocked = locked;
        }

        public RoomEntrance(Texture2D lockTexture, Texture2D unlockedText, Point Dimensions, Point Location, string fileName, string side, bool locked)
            : base(lockTexture, Dimensions, Location)
        {
            adjacent = fileName;
            entranceSide = side;
            doorLocked = locked;
            unlockedTexture = unlockedText;
            if (locked == false)
                texture = unlockedText;
        }

        public void SetFileName(string fileName)
        {
            adjacent = fileName;
        }

        public string GetFileName()
        {
            return adjacent;
        }

        public string GetSide()
        {
            return entranceSide;
        }

        public bool isLockStatus
        {
            set { doorLocked = value; }
            get { return doorLocked; }
        }

        public new void updateAfterPuzzle()
        {
            doorLocked = false;
            texture = unlockedTexture;
        }
    }
}
﻿#region Using Statements
using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace CodeCorrupt.LevelEditor
{
    public class Wall
    {
        public Vector2 point1, point2;
        public float width;
        public Color color;
        public Texture2D texture;

        public Wall(Texture2D Texture, Vector2 Point1, Vector2 Point2, float Width, Color inColor)
        {
            texture = Texture;
            point1 = Point1;
            point2 = Point2;
            width = Width;
            color = inColor;
        }

        public void Draw(SpriteBatch batch, Texture2D blank,
              float width, Color color, Vector2 point1, Vector2 point2)
        {
            float angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            float length = Vector2.Distance(point1, point2);

            batch.Draw(blank, point1, null, color,
                       angle, Vector2.Zero, new Vector2(length, width),
                       SpriteEffects.None, .99f);
        }
    }
}

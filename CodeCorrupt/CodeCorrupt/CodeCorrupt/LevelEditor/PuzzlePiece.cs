﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using CodeCorrupt.Gameplay.Puzzle;
#endregion

namespace CodeCorrupt.LevelEditor
{
    public class PuzzlePiece : LevelObject
    {
        public string name, value, type,displayText;
        //By Default CodePieces will show Type Name and Value
        public bool showType = true, showName = true, showValue = true;

        public PuzzlePiece(Texture2D Texture, Point Dimensions, Point Location)
            : base(Texture, Dimensions, Location)
        { }

        public PuzzlePiece(Texture2D Texture, Point Dimensions, Point Location,string _name,string _value,string _type)
            : base(Texture, Dimensions, Location)
        {
            name = _name; value = _value; type = _type;
        }

        public PuzzlePiece(Texture2D Texture, Point Dimensions, Point Location, string _name, string _value, string _type,string _display)
            : base(Texture, Dimensions, Location)
        {
            name = _name; value = _value; type = _type; displayText = _display;
        }

        public PuzzlePiece(Texture2D Texture, Point Dimensions, Point Location, string _name, string _value, string _type, bool sType, bool sName, bool sValue)
            : base(Texture, Dimensions, Location)
        {
            name = _name; value = _value; type = _type;
            showType = sType; showName = sName; showValue = sValue;
        }

        public PuzzlePiece(Texture2D Texture, Point Dimensions, Point Location, string _name, string _value, string _type, string _display, bool sType,bool sName,bool sValue)
            : base(Texture, Dimensions, Location)
        {
            name = _name; value = _value; type = _type; displayText = _display;
            showType = sType; showName = sName; showValue = sValue;
        }

        public void setDataShown(bool sType,bool sName,bool sValue)
        {
            showType = sType; showName = sName; showValue = sValue;
        }
    }
}

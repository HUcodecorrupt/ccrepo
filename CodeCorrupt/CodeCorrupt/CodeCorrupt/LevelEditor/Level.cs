﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeCorrupt.LevelEditor
{
    class Level
    {
        List<Room> Rooms;
        List<string> Names;
        public Level()
        {
            Rooms = new List<Room>();
            Names = new List<string>();
        }
        public List<Room> getRooms()
        { return Rooms; }
        public List<string> getNames()
        { return Names; }
    }
}

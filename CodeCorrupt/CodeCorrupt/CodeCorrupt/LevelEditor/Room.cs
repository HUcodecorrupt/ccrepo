﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Xml;
using CodeCorrupt.LevelEditor;
using CodeCorrupt.Gameplay.Character;
using CodeCorrupt.Gameplay.Puzzle;
using CodeCorrupt.Gameplay;
using System.Collections.ObjectModel;
#endregion

namespace CodeCorrupt.LevelEditor
{
    class Room
    {
        struct EnemyHolder
        {
            public Vector2 location;
            public Enemy enemy;
            public Enemy getEnemy()
            { return enemy; }
            public Vector2 getlocation()
            { return location; }
            public void setEnemy(Enemy villian)
            { enemy = villian; }
            public void setlocation(Vector2 position)
            { location = position; }
        };
        //List<EnemyHolder> Enemies = new List<EnemyHolder>();
        List<Enemy> Enemies = new List<Enemy>();
        List<LevelObject> objects = new List<LevelObject>();
        List<RoomEntrance> doors = new List<RoomEntrance>();
        List<Wall> walls = new List<Wall>();
        List<TripWire> trips = new List<TripWire>();
        List<Spawner> spawns = new List<Spawner>();
        List<messageTriggerObj> messageTriggers = new List<messageTriggerObj>();
        int triggerCount; //number of trigger objects
        Game game;
        //MainGame thisGame;
        public string roomFileName = "";
        Rectangle gameWindow;
        public float xOffset, yOffset;
        Terminal currentTerminal;
        public bool allowAllTerminal = false;
        public Terminal CurrentTerminal
        {
            set { currentTerminal = value; }
            get { return currentTerminal; }
        }

        public Room(ContentManager Content, string fileName, Game inGames)
        {
            game = inGames;
            Initialize();
            xOffset = .5f * gameWindow.Width - .5f * 800;
            yOffset = .5f * gameWindow.Height - .5f * 720;
            XmlDocument doc = new XmlDocument();
            doc.Load(@"Content/LevelObjects/LevelFiles/" + fileName);
            XmlNode root = doc.ChildNodes[1];
            LoadObjectsFromXmlFile(Content, root.ChildNodes);
            roomFileName = fileName;
            /*convertToXml("Content/LevelObjects/LevelFiles/Tutorial5.txt");
            convertToXml("Content/LevelObjects/LevelFiles/Tutorial3.txt");
            convertToXml("Content/LevelObjects/LevelFiles/Tutorial4.txt");*/
        }

        //create Message Object
        void createMsgObjects(ContentManager Content,Script script, string dialogName, MessagePopUp messageBox,int dx, int dy, int lx, int ly)
        {
            //place dialog names inside of another class.  make dialog Names Array unique to every room
            messageBox.LoadContent(game);
            //Use invisible_point or invisible_line sprite for texture
            messageTriggers.Add(new messageTriggerObj(Content.Load<Texture2D>(@"LevelObjects/blackBlock"), new Point(dx, dy), new Point(lx, ly), script, messageBox,game));
            messageTriggers[triggerCount].DialogName = dialogName;
            /*for (int i = 0; i < (game as MainGame).dialogNames.Length; i++)
            {
                //draw transparent trigger object, that initializes when player touches it
                messageTriggers.Add(new messageTriggerObj(Content.Load<Texture2D>(@"LevelObjects/invisible_point"), new Point(dx, dy), new Point(lx, ly), script, messageBox, game));
                messageTriggers[i].DialogName = (game as MainGame).dialogNames[i];
            }*/
        }

        /// <summary>
        /// To load the objects in a Room from an XML file
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="nodeList"></param>
        private void LoadObjectsFromXmlFile(ContentManager Content, XmlNodeList nodeList)
        {
            Texture2D blank = new Texture2D(game.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            blank.SetData(new[] { Color.White });
            objects.Clear();
            doors.Clear();
            walls.Clear();
            Enemies.Clear();
            trips.Clear();
            spawns.Clear();
            MessagePopUp messageBox = null;
            Script script;

            foreach (XmlNode node in nodeList)
            {
                int lx = 0, ly = 0, dx = 0, dy = 0, width = 0;
                string PuzzlefileName = "", name = "", value = "", type = "",displayText="", scriptFilename="", dialogName="";
                List<LevelObject> lvlObjects = new List<LevelObject>();
                List<Spawner> spList = new List<Spawner>();//the Spawn List to be added to the terminal
                bool showType = true, showName = true, showValue = true;
                if (node.NodeType == XmlNodeType.Element)
                {
                    lx = Convert.ToInt32(node.Attributes.GetNamedItem("lx").Value);
                    ly = Convert.ToInt32(node.Attributes.GetNamedItem("ly").Value);
                    dx = Convert.ToInt32(node.Attributes.GetNamedItem("dx").Value);
                    dy = Convert.ToInt32(node.Attributes.GetNamedItem("dy").Value);
                    lx += (int)xOffset;
                    ly += (int)yOffset;

                    switch (node.Name)
                    {
                        case "wall":
                            width = Convert.ToInt32(node.Attributes.GetNamedItem("thickness").Value);
                            dx += (int)xOffset;
                            dy += (int)yOffset;
                            walls.Add(new Wall(blank, new Vector2(lx, ly), new Vector2(dx, dy), width, Color.DarkBlue));
                            break;
                        case "terminal":
                            int hp = 0, cnAtk = 0, cnFire = 0, spd = 0, armr = 0;
                            bool enableHP = false, enableCannon = false;
                            PuzzlefileName = node.Attributes.GetNamedItem("xmlfilename").Value;

                            foreach (XmlNode child in node.ChildNodes)
                            {
                                if (child.Name == "Bonus")
                                {
                                    hp = Convert.ToInt32(child.Attributes.GetNamedItem("hp").Value);
                                    cnAtk = Convert.ToInt32(child.Attributes.GetNamedItem("cnAtk").Value);
                                    cnFire = Convert.ToInt32(child.Attributes.GetNamedItem("cnFire").Value);
                                    spd = Convert.ToInt32(child.Attributes.GetNamedItem("spd").Value);
                                    armr = Convert.ToInt32(child.Attributes.GetNamedItem("armr").Value);
                                }
                                else if (child.Name == "entrance" || child.Name == "turret")
                                    lvlObjects.Add(creatTerminalObjectFromXmlFile(Content, child));
                                else if (child.Name == "spawner")
                                {
                                    int locX = Convert.ToInt32(child.Attributes.GetNamedItem("lx").Value);
                                    int locY = Convert.ToInt32(child.Attributes.GetNamedItem("ly").Value);
                                    int sizeX = Convert.ToInt32(child.Attributes.GetNamedItem("dx").Value);
                                    int sizeY = Convert.ToInt32(child.Attributes.GetNamedItem("dx").Value);
                                    string enemyPic = "";
                                    foreach(XmlNode grandchild in child.ChildNodes)
                                    {
                                        if(grandchild.Name == "enemy")
                                        {
                                            enemyPic = grandchild.Attributes.GetNamedItem("texture").Value;
                                        }
                                    }
                                    Enemy badGuy = (new Enemy(game, new Vector2(lx, ly), Content.Load<Texture2D>(@enemyPic), new Point(sizeX, sizeY)));
                                    Spawner spawnPoint = new Spawner(new Point(locX, locY), badGuy, game);
                                    bool activateSpawners = true;
                                    spList.Add(spawnPoint);
                                    spawns.Add(spawnPoint);
                                }
                                else if (child.Name.ToUpper() == "ALLOWALLTERMINAL")
                                    allowAllTerminal = true;
                                else if (child.Name.ToUpper() == "ENABLEHP")
                                    enableHP = true;
                                else if (child.Name.ToUpper() == "ENABLECANNON")
                                    enableCannon = true;
                            }

                            Terminal term = new Terminal(Content.Load<Texture2D>(@"LevelObjects/terminal"), new Point(dx, dy), new Point(lx, ly), PuzzlefileName, lvlObjects);
                            term.setBonus(hp, cnAtk, cnFire, spd, armr);
                            term.setSpawner(spList);
                            term.hasEnableCannon = enableCannon;
                            
                            term.hasEnableHP = enableHP;
                            objects.Add(term);
                            break;
                        case "turret":
                            int chargeTime, shotSizeX, shotSizeY, shotSpeed, shotTime, damage, initialTime;
                            chargeTime = Convert.ToInt32(node.Attributes.GetNamedItem("chargeTime").Value);
                            shotSizeX = Convert.ToInt32(node.Attributes.GetNamedItem("shotSizeX").Value);
                            shotSizeY = Convert.ToInt32(node.Attributes.GetNamedItem("shotSizeY").Value);
                            shotSpeed = Convert.ToInt32(node.Attributes.GetNamedItem("shotSpeed").Value);
                            shotTime = Convert.ToInt32(node.Attributes.GetNamedItem("shotTime").Value);
                            damage = Convert.ToInt32(node.Attributes.GetNamedItem("damage").Value);
                            initialTime = Convert.ToInt32(node.Attributes.GetNamedItem("startTime").Value);
                            Turret t = new Turret(Content.Load<Texture2D>(@"LevelObjects/turretSprite"), new Point(dx, dy), new Point(lx, ly), chargeTime, initialTime, new Point(shotSizeX, shotSizeY), shotSpeed, shotTime, damage, game);
                            objects.Add(t);
                            break;
                        case "tripWire":
                            List<Spawner> spawnList = new List<Spawner>();
                            foreach (XmlNode child in node.ChildNodes)
                            {
                                if(child.Name=="spawner")
                                {
                                    int locX = Convert.ToInt32(child.Attributes.GetNamedItem("lx").Value);
                                    int locY = Convert.ToInt32(child.Attributes.GetNamedItem("ly").Value);
                                    int sizeX = Convert.ToInt32(child.Attributes.GetNamedItem("dx").Value);
                                    int sizeY = Convert.ToInt32(child.Attributes.GetNamedItem("dx").Value);
                                    string enemyPic = "";
                                    foreach (XmlNode grandchild in child.ChildNodes)
                                    {
                                        if (grandchild.Name == "enemy")
                                        {
                                            enemyPic = grandchild.Attributes.GetNamedItem("texture").Value;
                                        }
                                    }
                                    Enemy badGuy = (new Enemy(game, new Vector2(locX, locY), Content.Load<Texture2D>(@enemyPic), new Point(sizeX, sizeY)));
                                    Spawner spawnPoint = new Spawner(new Point(locX, locY), badGuy, game);
                                    spawnList.Add(spawnPoint);
                                }
                            }
                            TripWire trip = new TripWire(spawnList, new Rectangle(lx, ly, dx, dy));
                            trips.Add(trip);
                            break;
                        case "infiniteSpawner":
                            int spawnX = Convert.ToInt32(node.Attributes.GetNamedItem("lx").Value);
                            int spawnY = Convert.ToInt32(node.Attributes.GetNamedItem("ly").Value);
                            Enemy villain = new Enemy(game);
                            int startTime = Convert.ToInt32(node.Attributes.GetNamedItem("startTime").Value);
                            int interval = Convert.ToInt32(node.Attributes.GetNamedItem("interval").Value);
                            bool isActive = Convert.ToBoolean(node.Attributes.GetNamedItem("isActive").Value); // check this later
                            InfiniteSpawner spawnAlot = new InfiniteSpawner(new Point(spawnX, spawnY), villain, game, startTime, interval, isActive);
                            spawns.Add(spawnAlot);
                            break;
                        case "dialog":
                            scriptFilename = node.Attributes.GetNamedItem("scriptfilename").Value;
                            dialogName = node.Attributes.GetNamedItem("name").Value;
                            script = (game as MainGame).ReadScript(scriptFilename); //read script from file
                            messageBox = new MessagePopUp((game as MainGame).GraphicsDevice.Viewport, game); //create new istance of message box that shows message
                            createMsgObjects(Content, script, dialogName, messageBox, dx, dy, lx, ly);
                            break;
                        case "puzzlePiece":
                            name = node.Attributes.GetNamedItem("name").Value;
                            value = node.Attributes.GetNamedItem("value").Value;
                            type = node.Attributes.GetNamedItem("type").Value;
                            if (node.Attributes.GetNamedItem("showT") != null && node.Attributes.GetNamedItem("showT").Value.ToUpper() == "FALSE")
                            {
                                showType = false;
                            }
                            if (node.Attributes.GetNamedItem("showV") != null && node.Attributes.GetNamedItem("showV").Value.ToUpper() == "FALSE")
                            {
                                showValue = false;
                            }
                            if (node.Attributes.GetNamedItem("showN") != null && node.Attributes.GetNamedItem("showN").Value.ToUpper() == "FALSE")
                            {
                                showName = false;
                            }
                            objects.Add(new PuzzlePiece(Content.Load<Texture2D>(@"LevelObjects/CodePiece"), new Point(dx, dy), new Point(lx, ly), name, value, type,showType,showName,showValue));
                            break;
                        case "enemy":
                            string enemyPicture = node.Attributes.GetNamedItem("texture").Value;
                            string enemyBehavior = node.Attributes.GetNamedItem("behavior").Value;
                            Enemies.Add(new Enemy(game, new Vector2(lx, ly), Content.Load<Texture2D>(@enemyPicture), new Point(dx, dy)));
                            break;
                        case "entrance":
                            string side = node.Attributes.GetNamedItem("side").Value;
                            string nextRoom = node.Attributes.GetNamedItem("nextRoom").Value;
                            bool isLocked = false;

                            if (node.Attributes.GetNamedItem("isLocked").Value == "true")
                            {
                                isLocked = true;
                            }
                            doors.Add(new RoomEntrance(Content.Load<Texture2D>(@"LevelObjects/" + side), Content.Load<Texture2D>(@"LevelObjects/" + side + "Open"), new Point(dx, dy), new Point(lx, ly), nextRoom, side, isLocked));
                            break;
                        case "blackBrick":
                            objects.Add(new Brick(Content.Load<Texture2D>(@"LevelObjects/blackBlock"), new Point(dx, dy), new Point(lx, ly)));
                            break;
                        case "blueBrick":
                            objects.Add(new Brick(Content.Load<Texture2D>(@"LevelObjects/blueBlock"), new Point(dx, dy), new Point(lx, ly)));
                            break;
                        case "redBrick":
                            objects.Add(new Brick(Content.Load<Texture2D>(@"LevelObjects/redBlock"), new Point(dx, dy), new Point(lx, ly)));
                            break;
                        case "greenBrick":
                            objects.Add(new Brick(Content.Load<Texture2D>(@"LevelObjects/greenBlock"), new Point(dx, dy), new Point(lx, ly)));
                            break;
                        case "treasureChest":
                            objects.Add(new TreasureChest(Content.Load<Texture2D>(@"LevelObjects/treasureChest"), new Point(dx, dy), new Point(lx, ly)));
                            break;
                        case "SavePoint":
                            objects.Add(new SaveFileHandler(Content.Load<Texture2D>(@"LevelObjects/savePoint"), new Point(dx, dy), new Point(lx, ly)));
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Creates a single object to be attached to the Terminal
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        private LevelObject creatTerminalObjectFromXmlFile(ContentManager Content, XmlNode node)
        {
            Texture2D blank = new Texture2D(game.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            blank.SetData(new[] { Color.Black });
            LevelObject lvlObject = null;
            int lx = 0, ly = 0, dx = 0, dy = 0;
            string name = "", value = "", type = "";
            bool showType=true,showName=true,showValue=true;
            if (node.NodeType == XmlNodeType.Element)
            {
                lx = Convert.ToInt32(node.Attributes.GetNamedItem("lx").Value);
                ly = Convert.ToInt32(node.Attributes.GetNamedItem("ly").Value);
                dx = Convert.ToInt32(node.Attributes.GetNamedItem("dx").Value);
                dy = Convert.ToInt32(node.Attributes.GetNamedItem("dy").Value);
                lx += (int)xOffset;
                ly += (int)yOffset;

                switch (node.Name)
                {
                    case "entrance":
                        string side = node.Attributes.GetNamedItem("side").Value;
                        string nextRoom = node.Attributes.GetNamedItem("nextRoom").Value;
                        bool isLocked = false;

                        if (node.Attributes.GetNamedItem("isLocked").Value == "true")
                        {
                            isLocked = true;
                        }
                        lvlObject=new RoomEntrance(Content.Load<Texture2D>(@"LevelObjects/" + side), Content.Load<Texture2D>(@"LevelObjects/" + side + "Open"), new Point(dx, dy), new Point(lx, ly), nextRoom, side, isLocked);
                        doors.Add(lvlObject as RoomEntrance);
                        break;
                    case "turret":
                        int chargeTime, shotSizeX, shotSizeY, shotSpeed, shotTime, damage, initialTime;
                        chargeTime = Convert.ToInt32(node.Attributes.GetNamedItem("chargeTime").Value);
                        shotSizeX = Convert.ToInt32(node.Attributes.GetNamedItem("shotSizeX").Value);
                        shotSizeY = Convert.ToInt32(node.Attributes.GetNamedItem("shotSizeY").Value);
                        shotSpeed = Convert.ToInt32(node.Attributes.GetNamedItem("shotSpeed").Value);
                        shotTime = Convert.ToInt32(node.Attributes.GetNamedItem("shotTime").Value);
                        damage = Convert.ToInt32(node.Attributes.GetNamedItem("damage").Value);
                        initialTime = Convert.ToInt32(node.Attributes.GetNamedItem("startTime").Value);
                        lvlObject = new Turret(Content.Load<Texture2D>(@"LevelObjects/turretSprite"), new Point(dx, dy), new Point(lx, ly), chargeTime, initialTime, new Point(shotSizeX, shotSizeY), shotSpeed, shotTime, damage, game);
                        objects.Add(lvlObject);
                        break;
                    case "spawner":
                            int locX = Convert.ToInt32(node.Attributes.GetNamedItem("lx").Value);
                            int locY = Convert.ToInt32(node.Attributes.GetNamedItem("ly").Value);
                            int sizeX = Convert.ToInt32(node.Attributes.GetNamedItem("dx").Value);
                            int sizeY = Convert.ToInt32(node.Attributes.GetNamedItem("dx").Value);
                            string enemyPic = "";
                            foreach (XmlNode grandchild in node.ChildNodes)
                            {
                                if (grandchild.Name == "enemy")
                                {
                                    enemyPic = grandchild.Attributes.GetNamedItem("texture").Value;
                                }
                            }
                            Enemy badGuy = (new Enemy(game, new Vector2(locX, locY), Content.Load<Texture2D>(@enemyPic), new Point(sizeX, sizeY)));
                            Spawner spawnPoint = new Spawner(new Point(locX, locY), badGuy, game);
                            bool activateSpawners = true;
                            spawns.Add(spawnPoint);
                        break;
                    default:
                        break;
                }
            }
            return lvlObject;
        }

        public void Initialize()
        {
            gameWindow = game.Window.ClientBounds;
        }

        
        /// <summary>
        /// Converts the txt version of the room file to an xml file in the bin/debug folder
        /// </summary>
        /// <param name="file"></param>
        private void convertToXml(string file)
        {
            XmlDocument doc = new XmlDocument();
            XmlWriter writer = XmlTextWriter.Create(file+".xml");
            
            doc.CreateXmlDeclaration("1.0","utf-8",string.Empty);
            
            System.IO.StreamReader reader = new System.IO.StreamReader(file);
            string line = "";
            XmlNode rootTemp =  doc.CreateNode(XmlNodeType.Element, "Room", "");
            doc.CreateWhitespace("\n");
            doc.AppendChild(rootTemp);
            while ((line = reader.ReadLine()) != null) 
            {
                string brickType = "";
                if (line.Contains("wall"))
                {
                    XmlNode node = doc.CreateNode(XmlNodeType.Element, "wall", "");
                    rootTemp.AppendChild(node);
                    convertLocDimToXML(ref line, ref brickType,ref doc,ref node);
                    
                    XmlAttribute attrb = doc.CreateAttribute("dy");
                    attrb.Value = line.Substring(0, line.IndexOf(' '));
                    node.Attributes.Append(attrb);
                    line = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1));

                    attrb = doc.CreateAttribute("thickness");
                    attrb.Value = line;
                    node.Attributes.Append(attrb);
                }
                else if(line.Contains("enemy")){
                    XmlNode node = doc.CreateNode(XmlNodeType.Element, "enemy", "");
                    rootTemp.AppendChild(node);
                    convertLocDimToXML(ref line, ref brickType, ref doc, ref node);

                    XmlAttribute attrb = doc.CreateAttribute("dy");
                    attrb.Value = line.Substring(0, line.IndexOf(' '));
                    node.Attributes.Append(attrb);

                    line = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1));
                    attrb = doc.CreateAttribute("texture");
                    attrb.Value = line.Substring(0, line.IndexOf(' '));
                    node.Attributes.Append(attrb);
                    
                    line = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1));
                    attrb = doc.CreateAttribute("behavior");
                    attrb.Value = "";//attrb.Value = line
                    node.Attributes.Append(attrb);

                    attrb = doc.CreateAttribute("enemyType");
                    attrb.Value = "";
                    node.Attributes.Append(attrb);
                }
                else if (line.Contains("terminal"))
                {
                    XmlNode node = doc.CreateNode(XmlNodeType.Element, "terminal", "");
                    rootTemp.AppendChild(node);
                    convertLocDimToXML(ref line, ref brickType, ref doc, ref node);
                    //create dy attribute
                    XmlAttribute attrb = doc.CreateAttribute("dy");
                    attrb.Value = line.Substring(0, line.IndexOf(':'));
                    node.Attributes.Append(attrb);

                    //create xmlfilename attribute
                    attrb = doc.CreateAttribute("xmlfilename");
                    attrb.Value = line.Substring(line.IndexOf(":") + 1, line.LastIndexOf("::") - 3);
                    node.Attributes.Append(attrb);                
                }
                else if (line.Contains("entrance")){
                    XmlNode node = doc.CreateNode(XmlNodeType.Element, "entrance", "");
                    rootTemp.AppendChild(node);
                    convertLocDimToXML(ref line, ref brickType, ref doc, ref node);
                    //create dy attribute
                    XmlAttribute attrb = doc.CreateAttribute("dy");
                    attrb.Value = line;
                    node.Attributes.Append(attrb);

                    attrb = doc.CreateAttribute("side");
                    attrb.Value = brickType;
                    node.Attributes.Append(attrb);

                    attrb = doc.CreateAttribute("nextRoom");
                    attrb.Value = reader.ReadLine();
                    node.Attributes.Append(attrb);

                    attrb = doc.CreateAttribute("isLocked");
                    attrb.Value = "false";
                    node.Attributes.Append(attrb);
                }
                else if (line.Contains("puzzlePiece"))
                {
                    XmlNode node = doc.CreateNode(XmlNodeType.Element, "puzzlePiece", "");
                    rootTemp.AppendChild(node);
                    convertLocDimToXML(ref line, ref brickType, ref doc, ref node);
                    //create dy attribute
                    XmlAttribute attrb = doc.CreateAttribute("dy");
                    attrb.Value = line.Substring(0, line.IndexOf(':'));
                    line = line.Substring(line.IndexOf(":") + 1);
                    node.Attributes.Append(attrb);

                    //create Name Attribute
                    attrb = doc.CreateAttribute("name");

                    attrb.Value = line.Substring(line.IndexOf("n/") + 2, line.IndexOf(":") - 2);
                    line = line.Substring(line.IndexOf(":") + 1);
                    node.Attributes.Append(attrb);

                    //create value Attribute
                    attrb = doc.CreateAttribute("value");
                    attrb.Value = line.Substring(line.IndexOf("v/") + 2, line.IndexOf(":") - 2);
                    line = line.Substring(line.IndexOf(":") + 1);
                    node.Attributes.Append(attrb);

                    //create Type Attribute
                    attrb = doc.CreateAttribute("type");
                    attrb.Value = line.Substring(line.IndexOf("t/") + 2, line.IndexOf(":") - 2);
                    line = line.Substring(line.IndexOf(":") + 1);
                    node.Attributes.Append(attrb);
                }
            }
            
            writer.WriteRaw(doc.OuterXml);
            writer.Close();
            reader.Close();            
        }

        /// <summary>
        /// Handles converting the Location and Dimension property from the text file
        /// </summary>
        /// <param name="line"></param>
        /// <param name="brickType"></param>
        /// <param name="doc"></param>
        /// <param name="node"></param>
        private void convertLocDimToXML(ref string line,ref string brickType,ref XmlDocument doc,ref XmlNode node)
        {
            brickType = line.Substring(0, line.IndexOf(' '));
            line = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1));

            XmlAttribute attrb = doc.CreateAttribute("lx");
            attrb.Value = line.Substring(0, line.IndexOf(' '));
            node.Attributes.Append(attrb);
            line = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1));

            attrb = doc.CreateAttribute("ly");
            attrb.Value = line.Substring(0, line.IndexOf(' '));
            node.Attributes.Append(attrb);
            line = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1));

            attrb = doc.CreateAttribute("dx");
            attrb.Value = line.Substring(0, line.IndexOf(' '));
            node.Attributes.Append(attrb);
            line = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1));
        }

        public List<LevelObject> GetObjects()
        {
            return objects;
        }

        public List<RoomEntrance> GetEntrances()
        {
            return doors;
        }

        public List<Wall> GetWalls()
        {
            return walls;
        }

        public List<Enemy> GetEnemies()
        {
            return Enemies;
        }

        public List<messageTriggerObj> GetMessageObjects()
        {
            return messageTriggers;
        }
        public List<TripWire> GetTrips()
        {
            return trips;
        }
        public List<Spawner> GetSpawns()
        {
            return spawns;
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (messageTriggerObj msg in messageTriggers)
            {
                msg.Draw(gameTime, spriteBatch);
            }
            foreach (LevelObject o in objects)
            {
                o.Draw(gameTime, spriteBatch);
            }
            foreach (RoomEntrance e in doors)
            {
                e.Draw(gameTime, spriteBatch);
            }
            foreach (Wall w in walls)
            {
                w.Draw(spriteBatch, w.texture, w.width, w.color, w.point1, w.point2);
            }
        }
    }
}
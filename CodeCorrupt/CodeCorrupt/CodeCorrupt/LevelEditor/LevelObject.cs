﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace CodeCorrupt.LevelEditor
{
    public abstract class LevelObject
    {
        public Point location;
        public Point dimensions;
        public Texture2D texture;
        Rectangle drawBox;
        //create 2nd texture to draw after puzzle is solved
        public LevelObject(Texture2D Texture, Point Dimensions, Point Location)
        {
            texture = Texture;
            dimensions = Dimensions;
            location = Location;
            drawBox = new Rectangle(location.X, location.Y, dimensions.X, dimensions.Y);
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, new Rectangle(location.X, location.Y, dimensions.X, dimensions.Y), new Rectangle(0,0,dimensions.X,dimensions.Y), Color.White, 0f, Vector2.Zero, SpriteEffects.None, .002f);
        }

        public Rectangle GetDrawBox()
        {
            return drawBox;
        }

        public Point getLocation()
        {
            return location;
        }
        public string GetTextureName()
        {
            return texture.ToString();
        }

        public void updateAfterPuzzle()
        {

        }
    }
}
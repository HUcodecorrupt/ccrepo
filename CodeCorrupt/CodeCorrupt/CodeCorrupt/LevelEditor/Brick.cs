﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace CodeCorrupt.LevelEditor
{
    class Brick: LevelObject
    {
        //public Point location;
        //public Point dimensions;
        //public Texture2D texture;
        //Rectangle drawBox;

        public Brick(Texture2D Texture, Point Dimensions, Point Location)
        :base(Texture, Dimensions, Location)
        {
        }
    }
}

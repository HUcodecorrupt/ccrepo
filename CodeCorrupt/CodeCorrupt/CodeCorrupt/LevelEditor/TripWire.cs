﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using CodeCorrupt.LevelEditor;
using CodeCorrupt.Gameplay.Character;
#endregion

namespace CodeCorrupt.LevelEditor
{
    class TripWire
    {
        List<Spawner> spawnList;
        public bool tripped = false;
        Rectangle tripArea;

        public TripWire(List<Spawner> spawns, Rectangle wire)
        {
            spawnList = spawns;
            tripArea = wire;
        }
        public bool gettTripped()
        { return tripped; }
        public Rectangle getTripArea()
        { return tripArea; }
        public List<Spawner> getSpawnList()
        { return spawnList; }
    }
}

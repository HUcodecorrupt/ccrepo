﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using CodeCorrupt.LevelEditor;
using CodeCorrupt.Gameplay.Character;
#endregion

namespace CodeCorrupt.LevelEditor
{
    class Turret: LevelObject
    {
        List<Shot> shotList;
        enum TurretState { IDLE, ACTIVE, FIRING };
        TurretState state = TurretState.IDLE;
        int rechargeTime; //The amount of time it takes for the turret to wait betwen volleys
        int coldTime = 0; // The amount of time left before next volley
        int targetNum; //used in case we want to have a turret that can hit multiple targets at a time.
        int shotSpeed = 20;
        int shotTime = 3000;
        int damage = 15;
        int health = 100, currentHealth;
        bool exists = true;
        public bool readyToFire;
        Shot ammo;
        Vector2 target;
        Point turretSize;
        Point sheetSize = new Point(3,0);
        Point frameSize = new Point(100,100);
        Point currentFrame = new Point(0, 0);
        Point center;
        Point shotSize = new Point(25,25);
        Game game;

         public Turret(Texture2D Texture, Point TurretSize, Point Location, int chargeTime, int initialTime, Point ShotSize, int ShotSpeed, int ShotTime, int Damage, Game inputGame)
             : base(Texture, TurretSize, Location)
         {
             rechargeTime = chargeTime;
             turretSize = TurretSize;
             center = new Point(location.X + turretSize.X / 2, location.Y + turretSize.Y / 2);
             game = inputGame;
             shotSize = ShotSize;
             shotSpeed = ShotSpeed;
             shotTime = ShotTime;
             damage = Damage;
             coldTime = initialTime;
             currentHealth = health;
         }

        public void update(GameTime gameTime)
        {
            if (readyToFire) //(readyToFire && (state == TurretState.ACTIVE || state == TurretState.FIRING))
            {

            }
            else
            {
                coldTime -= gameTime.ElapsedGameTime.Milliseconds;
                if (coldTime <= 0)
                {
                    readyToFire = true;
                }
            }
            
            
        }

        public void suspendFire()
        {
            readyToFire = false;
            coldTime = rechargeTime;
        }

        public void damageTurret(int damagae)
        {
            currentHealth -= damage;
            if (currentHealth <= 0)
            {
                exists = false;
            }
        }

        public bool getExists()
        {
            return exists;
        }


        public void changeState(string stateString)
        {
            switch(stateString)
            {
                case "Idle":
                    state = TurretState.IDLE;
                    break;
                case "Active":
                    state = TurretState.ACTIVE;
                    break;
                case "Firing":
                    state = TurretState.FIRING;
                    break;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, new Rectangle((int)location.X, (int)location.Y, turretSize.X, turretSize.Y), new Rectangle(currentFrame.X * frameSize.X, currentFrame.Y * frameSize.Y, frameSize.X, frameSize.Y), Color.White, 0f, Vector2.Zero, SpriteEffects.None, .0301f);
        }

        public Shot fireAtTarget(Character player)
        {
            target = Vector2.Normalize(new Vector2(player.getCenter().X - center.X, player.getCenter().Y - center.Y));
            return new Shot(game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"), new Vector2(center.X, center.Y), new Point(68,69), shotSize, target, shotSpeed, shotTime, game, true, false);
        }

        public Shot fireAtTarget(Enemy enemy)
        {
            target = Vector2.Normalize(new Vector2(enemy.getCenter().X - center.X, enemy.getCenter().Y - center.Y));
            return new Shot(game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"), new Vector2(center.X, center.Y), new Point(68, 69), shotSize, target, shotSpeed, shotTime, game, false, true);
        }

        public void clearShotList()
        {
            shotList.Clear();
        }
    }
}

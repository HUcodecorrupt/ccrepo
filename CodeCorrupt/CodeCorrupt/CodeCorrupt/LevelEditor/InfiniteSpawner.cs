﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using CodeCorrupt.LevelEditor;
using CodeCorrupt.Gameplay.Character;
#endregion

namespace CodeCorrupt.LevelEditor
{
    class InfiniteSpawner:Spawner
    {
        public bool on;
        int timeToSpawn, spawnTime;
        public bool spawnNow;
        public InfiniteSpawner(Point SpawnPoint, Enemy Badguy, Game inputGame, int startTime, int interval, bool isActive)
            :base(SpawnPoint, Badguy, inputGame)
        {
            spawnTime = interval;
            timeToSpawn = startTime;
            on = isActive;
        }

        public void update(GameTime gameTime)
        {
            if (on) //(readyToFire && (state == TurretState.ACTIVE || state == TurretState.FIRING))
            {
                if (!spawnNow)
                {
                    timeToSpawn -= gameTime.ElapsedGameTime.Milliseconds;
                    if (timeToSpawn <= 0)
                    {
                        spawnNow = true;
                    }
                }
            }            
        }

        public void suspendSpawn()
        {
            spawnNow = false;
            timeToSpawn = spawnTime;
        }
        
    }
}

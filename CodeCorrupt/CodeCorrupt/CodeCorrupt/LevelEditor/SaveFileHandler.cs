﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;
using CodeCorrupt.LevelEditor;
using Microsoft.Xna.Framework.Content;

namespace CodeCorrupt.LevelEditor
{
    class SaveFileHandler : LevelObject
    {
        /* Save file needs
         * Room file of where the character saved. Characters can only save in Rooms with Save Points
         * Characters Position in the room
         * The PuzzlePiece List
         */
        List<PuzzlePiece> puzzlePieceList = new List<PuzzlePiece>();
        Vector2 char_pos = Vector2.Zero; //Characters Position
        int timeSaved = 0; //The number of times the player saved
        int fileNumber = 0;//the sequence number of the file saved
        string roomFile = "",saveFile=@"Content/SaveData/saveFile"; //roomFile:the FileName of the Room the player saved in |saveFile: File path of the save file
        DateTime date;//the Date the Player Saved
        Rectangle vicinity;
        //float saveDepth = 0.00f;//The depth of the savePoint
        bool saveSucess = false,loadSuccess=false;
        const int MAXTIME =500;
        int imgHop = 30,timer=MAXTIME;
        Point newLoc;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Texture"></param>
        /// <param name="Dimensions"></param>
        /// <param name="Location"></param>
        public SaveFileHandler(Texture2D Texture, Point Dimensions, Point Location)
            :base(Texture,Dimensions,Location)
        {
            newLoc = location;
            saveFile += "" + timeSaved+".xml";
            vicinity = new Rectangle(Location.X - 50, Location.Y - 50, Dimensions.X + 100, Dimensions.Y + 100);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Texture"></param>
        /// <param name="Dimensions"></param>
        /// <param name="Location"></param>
        /// <param name="filename">The name of the savefiles name</param>
        public SaveFileHandler(Texture2D Texture, Point Dimensions, Point Location, string filename)
            : base(Texture, Dimensions, Location)
        {
            newLoc = location;
            saveFile = filename;
            vicinity = new Rectangle(Location.X - 50, Location.Y - 50, Dimensions.X + 100, Dimensions.Y + 100);
        }

        //Saves the Characters Position, the filename of the room, and their puzzlepiece list
        public void saveCharacterData(Vector2 c_pos, string file, List<PuzzlePiece> pList)
        {
            saveSucess = false;
            char_pos = c_pos;
            roomFile = file;
            puzzlePieceList = pList;
            timer = MAXTIME;
            XmlDocument doc = new XmlDocument();
            doc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
            
            XmlWriter writer = XmlTextWriter.Create(saveFile);
            //XmlWriter writer = XmlTextWriter.Create("CodeCorrupt/CodeCorrupt/CodeCorrupt/Content/saveFile" + timeSaved + ".xml");
            XmlNode root = doc.CreateNode(XmlNodeType.Element, "SaveFile", "");
            XmlNode node1 = doc.CreateNode(XmlNodeType.Element, "CharacterPosition","");
            XmlAttribute Xpos = doc.CreateAttribute("lx"), Ypos = doc.CreateAttribute("ly");
            Xpos.Value = "" + char_pos.X; Ypos.Value = "" + char_pos.Y;
            node1.Attributes.Append(Xpos); node1.Attributes.Append(Ypos);

            XmlNode node2 = doc.CreateNode(XmlNodeType.Element, "Room", "");
            XmlAttribute rm_File = doc.CreateAttribute("filepath");
            rm_File.Value = roomFile;
            node2.Attributes.Append(rm_File);

            root.AppendChild(node1);
            root.AppendChild(node2);

            doc.CreateWhitespace("\n");
            //doc.AppendChild(root);

            foreach (PuzzlePiece child in pList)
            {
                //Creat the XmlNode and the XmlAttributes
                XmlNode node3 = doc.CreateNode(XmlNodeType.Element, "puzzlePiece", "");
                XmlAttribute lx = doc.CreateAttribute("lx"), ly = doc.CreateAttribute("ly"), 
                    dx = doc.CreateAttribute("dx"), dy = doc.CreateAttribute("dy")
                    , name = doc.CreateAttribute("name"), value = doc.CreateAttribute("value"),type = doc.CreateAttribute("type")
                , showT = doc.CreateAttribute("showT"), showN = doc.CreateAttribute("showN"), showV = doc.CreateAttribute("showV");

                //Set the values of the attributes
                lx.Value = "" + child.location.X; ly.Value = "" + child.location.Y;
                dx.Value = "" + child.dimensions.X; dy.Value = "" + child.dimensions.Y;
                name.Value = child.name; type.Value = child.type; value.Value = child.value;
                showT.Value = "" + child.showType; showN.Value = "" + child.showName; showV.Value = "" + child.showValue;
                
                //Add the XmlAttributes to the Node
                node3.Attributes.Append(lx); node3.Attributes.Append(ly);
                node3.Attributes.Append(dx); node3.Attributes.Append(dy);
                node3.Attributes.Append(name); node3.Attributes.Append(value); node3.Attributes.Append(type);
                node3.Attributes.Append(showT); node3.Attributes.Append(showN); node3.Attributes.Append(showV);

                //I add the nodes to the XmlDocument
                doc.CreateWhitespace("\n");
                root.AppendChild(node3);
            }
            doc.AppendChild(root);
            //writer.Settings.Indent = true;
            writer.WriteRaw(doc.OuterXml);

            //System.IO.StreamWriter swriter = new System.IO.StreamWriter(@"CodeCorrupt/CodeCorrupt/CodeCorrupt/Content/saveFile"+timeSaved+".xml");

            saveSucess = true;
            writer.Close();
        }

        /// <summary>
        /// Loads save file data
        /// </summary>
        /// <param name="c_pos"></param>
        /// <param name="file">The file path of the file to load from</param>
        /// <param name="pList"></param>
        public void loadCharacterData(ContentManager Content,string file, List<PuzzlePiece> pList)
        {
            loadSuccess = false;
            //char_pos = c_pos;
            //puzzlePieceList = pList;
            timer = MAXTIME;
            XmlDocument doc = new XmlDocument();
            doc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
            doc.Load(@"Content/LevelObjects/LevelFiles/" + file);
            //XmlWriter writer = XmlTextWriter.Create(saveFile);
            //XmlWriter writer = XmlTextWriter.Create("CodeCorrupt/CodeCorrupt/CodeCorrupt/Content/saveFile" + timeSaved + ".xml");
            XmlNode root=null;
            foreach (XmlNode node in doc.ChildNodes)
            {
                if (node.Name.ToUpper() == "SAVEFILE")
                    root = node;
            }

            if (root != null && root.ChildNodes.Count > 0)
            {
                foreach (XmlNode node in doc.ChildNodes)
                {
                    if (node.Name.ToUpper() == "CHARACTERPOSITION")
                    {
                        foreach (XmlAttribute attr in node.Attributes)
                        {
                            //double d= Convert.ToDouble(attr.Value);
                            if (attr.Name == "lx")
                                char_pos.X = float.Parse(attr.Value);
                            else if (attr.Name == "ly")
                                char_pos.X = float.Parse(attr.Value);
                        }
                    }
                    else if (node.Name.ToUpper() == "ROOM")
                    {
                        foreach (XmlAttribute attr in node.Attributes)
                        {
                            if (attr.Name.ToUpper() == "FILEPATH")
                                roomFile = attr.Value;
                        }
                    }
                    else if (node.Name.ToUpper() == "PUZZLEPIECE")
                    {
                        string name = "", value = "", type = "";
                        bool showType = true, showName = true, showValue = true;
                        Texture2D ptext = Content.Load<Texture2D>(@"LevelObjects/savePoint");
                        Point loc = Point.Zero, dim = Point.Zero;

                        foreach (XmlAttribute attr in node.Attributes)
                        {
                            if (attr.Name.ToUpper() == "LX")
                                loc.X = Convert.ToInt32(attr.Value);
                            else if (attr.Name.ToUpper() == "LY")
                                loc.X = Convert.ToInt32(attr.Value);
                            else if (attr.Name.ToUpper() == "DX")
                                dim.X = Convert.ToInt32(attr.Value);
                            else if (attr.Name.ToUpper() == "DY")
                                dim.X = Convert.ToInt32(attr.Value);
                            else if (attr.Name.ToUpper() == "NAME")
                                name = attr.Value;
                            else if (attr.Name.ToUpper() == "VALUE")
                                value = attr.Value;
                            else if (attr.Name.ToUpper() == "TYPE")
                                type = attr.Value;
                            else if (attr.Name.ToUpper() == "SHOWT" && attr.Value.ToUpper() == "FALSE")
                            {
                                showType = false;
                            }
                            else if (attr.Name.ToUpper() == "SHOWV" && attr.Value.ToUpper() == "FALSE")
                            {
                                showValue = false;
                            }
                            else if (attr.Name.ToUpper() == "SHOWN" && attr.Value.ToUpper() == "FALSE")
                            {
                                showName = false;
                            }
                        }        
                        puzzlePieceList.Add(new PuzzlePiece(Content.Load<Texture2D>(@"LevelObjects/CodePiece"), dim, loc, name, value, type, showType, showName, showValue));
                        
                    }

                }      
            }

            XmlAttribute rm_File = doc.CreateAttribute("filepath");
     

            foreach (PuzzlePiece child in pList)
            {
                //Creat the XmlNode and the XmlAttributes
                XmlNode node3 = doc.CreateNode(XmlNodeType.Element, "puzzlePiece", "");
                XmlAttribute lx = doc.CreateAttribute("lx"), ly = doc.CreateAttribute("ly"),
                    dx = doc.CreateAttribute("dx"), dy = doc.CreateAttribute("dy")
                    , name = doc.CreateAttribute("name"), value = doc.CreateAttribute("value"), type = doc.CreateAttribute("type")
                , showT = doc.CreateAttribute("showT"), showN = doc.CreateAttribute("showN"), showV = doc.CreateAttribute("showV");

                //Set the values of the attributes
                lx.Value = "" + child.location.X; ly.Value = "" + child.location.Y;
                dx.Value = "" + child.dimensions.X; dy.Value = "" + child.dimensions.Y;
                name.Value = child.name; type.Value = child.type; value.Value = child.value;
                showT.Value = "" + child.showType; showN.Value = "" + child.showName; showV.Value = "" + child.showValue;

                //Add the XmlAttributes to the Node
                node3.Attributes.Append(lx); node3.Attributes.Append(ly);
                node3.Attributes.Append(dx); node3.Attributes.Append(dy);
                node3.Attributes.Append(name); node3.Attributes.Append(value); node3.Attributes.Append(type);
                node3.Attributes.Append(showT); node3.Attributes.Append(showN); node3.Attributes.Append(showV);

                //I add the nodes to the XmlDocument
                doc.CreateWhitespace("\n");
                root.AppendChild(node3);
            }
            doc.AppendChild(root);
            //writer.Settings.Indent = true;
            //writer.WriteRaw(doc.OuterXml);

            //System.IO.StreamWriter swriter = new System.IO.StreamWriter(@"CodeCorrupt/CodeCorrupt/CodeCorrupt/Content/saveFile"+timeSaved+".xml");
            loadSuccess = true;
            //writer.Close();
        }
        public new void updateAfterPuzzle()
        {

        }

        public Rectangle Vicinity
        {
            get { return vicinity; }
        }

        public void makeImgHop()
        {
            newLoc.Y--;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

            if (saveSucess && timer > 0)
            {
                spriteBatch.Draw(texture, new Rectangle(newLoc.X, newLoc.Y, dimensions.X, dimensions.Y), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.002f);
                timer--;
            }
            else
            {
                spriteBatch.Draw(texture, new Rectangle(newLoc.X, newLoc.Y, dimensions.X, dimensions.Y), null, Color.Gray, 0f, Vector2.Zero, SpriteEffects.None, 0.002f);
            }
        }
    }
}

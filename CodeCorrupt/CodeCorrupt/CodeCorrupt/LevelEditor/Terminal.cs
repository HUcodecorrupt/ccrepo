﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using CodeCorrupt.Gameplay.Character;
using CodeCorrupt.Gameplay.Puzzle;
#endregion

namespace CodeCorrupt.LevelEditor
{
    public class Terminal : LevelObject
    {
        private string fileName = "";
        public string thingToBeChanged = "";
        bool puzzleFlag;
        public bool enableHp=false,enableCannon=false,hasEnableHP=false,hasEnableCannon=false;
        Rectangle vicinity;
        List<LevelObject> objsToChange = new List<LevelObject>();
        List<Spawner> spawnerList = new List<Spawner>();
        private int hp, cnAtk, cnFire, spd, armr;
        public Script scriptPuzzle, scriptErrors;
        messageTriggerObj messageTrigger;
        bool display_message;
        MessagePopUp popMessage;
        string dailogName="";

        public Terminal(Texture2D Texture, Point Dimensions, Point Location)
            : base(Texture, Dimensions, Location)
        {
            vicinity = new Rectangle(Location.X - 50, Location.Y - 50, Dimensions.X + 100, Dimensions.Y + 100);
        }
        public Terminal(Texture2D Texture, Point Dimensions, Point Location, string FileName, List<LevelObject> lvlObjects) //should we use an LevelObject type instead of a string???
            : base(Texture, Dimensions, Location)
        {
            fileName = FileName; objsToChange=lvlObjects;
            vicinity = new Rectangle(Location.X - 50, Location.Y - 50, Dimensions.X + 100, Dimensions.Y + 100);
        }

        public Terminal(Texture2D Texture, Point Dimensions, Point Location, string FileName, List<LevelObject> lvlObjects, string dName) //should we use an LevelObject type instead of a string???
            : base(Texture, Dimensions, Location)
        {
            dailogName = dName;
            fileName = FileName;
            objsToChange=lvlObjects;
            vicinity = new Rectangle(Location.X - 50, Location.Y - 50, Dimensions.X + 100, Dimensions.Y + 100);
        }
        
        public void setSpawner(List<Spawner> spList)
        {
            spawnerList = spList;
        }

        public void setBonus(int h, int cnA, int cnF, int s, int a)
        {
            hp = h; cnAtk = cnA; cnFire = cnF; spd = s; armr = a;
        }
        public string FileName
        {
            get { return fileName; }
        }
        public Rectangle getVicinity()
        {
            return vicinity;
        }

        public string ObjectToChange
        {
            get { return thingToBeChanged; }
        }

        public bool isPuzzleComplete
        {
            get { return puzzleFlag; }
            set { puzzleFlag = value; }
        }

        public void applyBonus(Character character)
        {

        }

        /// <summary>
        /// create Message Object
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="script"></param>
        /// <param name="dialogName"></param>
        /// <param name="messageBox"></param>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        /// <param name="lx"></param>
        /// <param name="ly"></param>
        /// <param name="game"></param>
        public void createMsgObjects(ContentManager Content, Script script, string dialogName, MessagePopUp messageBox, int dx, int dy, int lx, int ly, Game game)
        {
            //place dialog names inside of another class.  make dialog Names Array unique to every room
            //messageBox.LoadContent(game);
            //Use invisible_point or invisible_line sprite for texture
            messageTrigger = new messageTriggerObj(Content.Load<Texture2D>(@"LevelObjects/blackBlock"), new Point(dx, dy), new Point(lx, ly), script, messageBox, game);
            //create array for dialogName, use this array to send to messagePopUp class and set new name
            messageTrigger.DialogName = dialogName;
            popMessage = new MessagePopUp(game.GraphicsDevice.Viewport, game);
            popMessage.LoadContent(game);
        }

        public void checkPuzzle(Character character,GameTime gameTime,Game game)
        {
            //make a list of the answers used
            //general function to update the level object
            //function to update the character
            //in the puzzle have a property to tell me which ones to modify, and the answer used is the value
            if (puzzleFlag)// || Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                //need a scripts xmlfile, and a specific dailog im starting at
                /***********ATTTENTION need to
                 * give Terminal dialog tags and set the name to something!!!
                 * Actually put the messages in TerminalMessages.xml
                 * setup a new popMessage somewhere
                 
                createMsgObjects(game.Content,(game as MainGame).ReadScript(@"Content/LevelObjects/LevelFiles/TerminalMessages.xml"),
                    dailogName, popMessage, 0, 0, 0, 0, game);*/ 
                if (messageTrigger != null)
                {
                    messageTrigger.HandlePlayerInteraction(null);
                    messageTrigger.Update(gameTime);
                    if (messageTrigger.showingDialog)
                    {  //set puzzleIDE to display message pop up
                        display_message = true;
                        popMessage.ShowMessage = true;
                    }
                    else
                        display_message = false;
                }
                foreach (LevelObject child in objsToChange){
                    if (child is RoomEntrance)
                    {
                        ((RoomEntrance)child).updateAfterPuzzle();
                    }
                }
                applyBonus(character);
                if (hasEnableHP)
                    enableHp = true;
                if (hasEnableCannon)
                    enableCannon = true;
            }
        }

        /// <summary>
        /// The function activates the Spawner attached to the terminal
        /// </summary>
        /// <param name="isActive">IF TRUE will activate the Spawner</param>
        public List<Enemy> activateSpawner()
        {
            List<Enemy> enemiesToSpawn = new List<Enemy>();
            if (spawnerList.Count > 0)
            {
                foreach (Spawner spawner in spawnerList)
                {
                    enemiesToSpawn.Add(spawner.spawnEnemy());
                }
            }
            return enemiesToSpawn;
        }
    }

}
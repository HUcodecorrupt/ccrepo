﻿using CodeCorrupt.LevelEditor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeCorrupt.Gameplay.Puzzle
{
    public class messageTriggerObj : LevelObject //NPC
    {
        /*
 * Level objects will have dialogs associated with them.  
 * If the player is a certain distance between the level object, then the dialog/message will trigger
 * all level objects don't have to be visible or touchable, thus some level objects can exist only as event triggers.
 * */

        public Script script;
        public MessagePopUp dialog;
        string dialogName;
        public bool showingDialog;
        float speakingRadius;
        public Texture2D texture;
        Game nGame;

        public messageTriggerObj(Texture2D Texture, Point Dimensions, Point Location)
            : base(Texture, Dimensions, Location)
        {
        }

        public messageTriggerObj(Texture2D Texture, Point Dimensions, Point Location, Script _script, MessagePopUp _dialog, Game game)
            :base(Texture, Dimensions, Location)
        {
            texture = Texture;
            speakingRadius = 200f;
            script = _script;
            dialog = _dialog;
            dialog.Hide();
            nGame = game;
        }

        public bool ShowingDialog
        {
            get { return showingDialog; }
        }

        public string DialogName
        {
            get { return dialogName; }
            set { dialogName = value; }
        }

        public void Update(GameTime gameTime)
        {
            if (dialog.Enabled)
                dialog.Update(gameTime);
        }
        //to make object draw, add "override"
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (dialog.Enabled)
            {
                dialog.ShowMessage = showingDialog;
                dialog.Draw(gameTime,spriteBatch);
            }
 	      //base.Draw(gameTime, spriteBatch);
        }

        public void StartDialog(string dialogname)
        {
            if (script == null || dialog == null)
                return;
            dialog.msgObj = this;
            dialog.dialog = script[dialogname];
            showingDialog = true;
            dialog.Show();
        }
        public void StopDialog()
        {
            if (dialog == null)
                return;
            dialog.Hide();
            showingDialog = false;
        }

        public void HandlePlayerInteraction(Character.Character character)
        {
            if (!showingDialog && dialog.Played == false)
            {
                if ((nGame as MainGame).gameState == GameState.Dungeon)
                {
                    float distance = Vector2.Distance(new Vector2(location.X, location.Y), character.getRingsPosition());
                    if (distance < speakingRadius) //if player is a certain distance from object, trigger dialog
                    {
                        StartDialog(dialogName);
                       // showingDialog = true;
                    }
                }
                //starts dialog when player enters a puzzle, needs to be changed so that if a dialog is not in a room, it does not initiate
                else if ((nGame as MainGame).gameState == GameState.Puzzle) 
                {
                    StartDialog(dialogName);
                    //showingDialog = true;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;

namespace CodeCorrupt.Gameplay.Puzzle
{
    class AnswerBlock : CodeBlock
    {
        private CodeBlock submitSpace,codeBlock;//make submitspace rectangle later
        private AnswerBlock parent;
        private List<AnswerBlock> children;
        private bool needAnswer = false, hasBlockSnapped = false;
        private Point codeFrame = new Point(50, 50);
        private Texture2D indTex; //Texture for the Indent texture
        private string answerValue = "", ansType = "";
        private int indSpWid = 50,indSpHt=50,indOffset=15;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cb_pos"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="txtr"></param>
        /// <param name="dtext"></param>
        /// <param name="nodes"></param>
        public AnswerBlock(Vector2 cb_pos, int width, int height, string dtext, bool nAnswer) :
            base(cb_pos, width, height)
        {
            displayText = dtext;
            children = new List<AnswerBlock>();
            needAnswer = nAnswer;
            if (needAnswer)
            {
                submitSpace = new CodeBlock(new Vector2(cb_pos.X + (width - indOffset), (float)Rect.Center.Y - (codeFrame.Y / 2)), indSpWid, indSpHt);
                submitSpace.Depth = depth;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="cb_pos"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="txtr"></param>
        /// <param name="dtext"></param>
        /// <param name="nodes"></param>
        public AnswerBlock(AnswerBlock p, Vector2 cb_pos, int width, int height, string dtext, bool nAnswer)
            : base(cb_pos, width, height)
        {
            displayText = dtext;
            parent = p;
            children = new List<AnswerBlock>();
           
            needAnswer = nAnswer;
            if (needAnswer)
            {
                submitSpace = new CodeBlock(new Vector2(cb_pos.X + (width - indOffset), (float)Rect.Center.Y - (codeFrame.Y / 2)), indSpWid, indSpHt);
                submitSpace.Depth = depth;
            }
        }


        public AnswerBlock(Texture2D text,AnswerBlock p, Vector2 cb_pos, int width, int height, string dtext, bool nAnswer)
            : base(cb_pos, width, height)
        {
            displayText = dtext;
            parent = p;
            children = new List<AnswerBlock>();
            textureImage = text;
            needAnswer = nAnswer;
            if (needAnswer)
            {
                submitSpace = new CodeBlock(new Vector2(cb_pos.X + (width - indOffset), (float)Rect.Center.Y - (codeFrame.Y / 2)), indSpWid, indSpHt);
                submitSpace.Depth = depth;
            }
        }
        /// <summary>
        /// Adds AnswerBlocks to this AnswerBlock List.
        /// </summary>
        /// <param name="c">Is the AnswerBlock to be added as a child of this</param>
        public void addChild(AnswerBlock c)
        {
            children.Add(c);
            c.Depth = depth - 0.001f;
            if (c.needAnsBlock)
                c.submitSpace.Depth = c.depth + 0.0001f;
            c.StringDepth = string_depth;
        }

        /// <summary>
        /// Returns the codeblock submitted as an answer to this AnswerBlock
        /// </summary>
        /// <returns></returns>
        public CodeBlock getSubmitBlock()
        {
            return codeBlock;
        }

        /// <summary>
        /// Gets the LastChild in from the list of AnswerBlock children. If there are no children, 
        /// it returns null
        /// </summary>
        public AnswerBlock LastChild
        {
            get
            {
                if (children.Count > 0)
                    return children[children.Count - 1];
                else
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="game"></param>
        public void LoadContent(Game game)
        {
            indTex = game.Content.Load<Texture2D>(@"CodePuzzle/PuzzleImages/SubmitIndent");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool needAnsBlock
        {
            get {
                return needAnswer; 
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool hasBlock
        {
            get
            {
                return hasBlockSnapped;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public float getNexChildtHeight(int count)
        {
            if (children.Count > 0)
                return LastChild.Position.Y + LastChild.positionRect.Height;
            else
                return this.Position.Y + 40;
        }

        /// <summary>
        /// Compares the AnswerBlocks answer with the submitted CodeBlocks answer
        /// </summary>
        /// <returns></returns>
        public bool Compare()
        {
            string ans = answerValue.Substring(answerValue.IndexOf(":") + 1), submitted = codeBlock.Value;
            //make case that if ans is the wrong data type, incase they give true for integer
            if(submitted.Contains("="))
                submitted = submitted.Substring(submitted.IndexOf("=")+1);

            //string answer = codeBlock.getDataText().Substring(codeBlock.getDataText().LastIndexOf(" "));
            if (ansType == "match" && ans.ToUpper() == submitted.ToUpper())
                return true;
            if (codeBlock.Type.Contains("integer") || codeBlock.Type.Contains("float") || codeBlock.Type.Contains("double"))
            {
                if (ansType == "-num" && Convert.ToDouble(submitted) < Convert.ToDouble(ans)) //-num means the codeBlock values is less than the answer value
                    return true;
                else if (ansType == "num+" && Convert.ToDouble(submitted) > Convert.ToDouble(ans)) //num+ means the codeBlock values is greater than the answer value
                    return true;
            }
            return false;
        }
        
        /// <summary>
        /// Submits the CodeBlock to the AnswerBlock, and snaps it to the submit space
        /// </summary>
        /// <param name="cblock"></param>
        public void snapBlock(CodeBlock cblock)
        {
            if (!hasBlockSnapped && needAnswer)
            {
                float unusedSpace = Math.Abs(submitSpace.Rect.Height-cblock.Rect.Height),
                    yOffset = submitSpace.Position.Y+unusedSpace/2, 
                    xOffset = submitSpace.Position.X+submitSpace.Rect.Width/2;
                //yOffset = submitSpace's Y + the unused Space height/2
                //xOffset = submitSpaceX + SubmitSpace wid/2
                //The xOffset and y Offset is for placing the CodeBlock in the proper place in relation 
                //to the submit Space
                cblock.setPos(new Vector2(xOffset, yOffset));
                codeBlock = cblock;
                hasBlockSnapped = true;
            }

        }

        /// <summary>
        /// Returns the submit space 
        /// </summary>
        /// <returns></returns>
        public CodeBlock getSubmitSpace()
        {
            if (needAnswer)
                return submitSpace;
            return new CodeBlock(Vector2.Zero, 0, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Update()
        {
            if (hasBlockSnapped && !codeBlock.Rect.Intersects(submitSpace.Rect))
            {
                codeBlock = null;
                hasBlockSnapped = false;
            }
        }

        /// <summary>
        /// Draws the Answer
        /// </summary>
        /// <param name="spriteBatch"></param>
        public new void Draw(SpriteBatch spriteBatch)
        {
            if (needAnswer)//if the Answer block needs an answer, the indent texture is drawn slightly above it
                spriteBatch.Draw(indTex, submitSpace.Rect, null, color, 0f, Vector2.Zero, SpriteEffects.None, submitSpace.Depth);//+0.001f);
            spriteBatch.Draw(textureImage, Rect, null, color, 0f, Vector2.Zero, SpriteEffects.None, depth);
        }

        /// <summary>
        /// Draws the Answer
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch,SpriteFont font)
        {
            if (needAnswer)//if the Answer block needs an answer, the indent texture is drawn slightly above it
                spriteBatch.Draw(indTex, submitSpace.Rect, null, color, 0f, Vector2.Zero, SpriteEffects.None, submitSpace.Depth);//+0.001f);
            spriteBatch.Draw(textureImage, Rect, null, color, 0f, Vector2.Zero, SpriteEffects.None, depth);
            //spriteBatch.DrawString(font, "Depth " + Depth + " S_Depth " + string_depth, new Vector2((int)Position.X + 20, (int)Position.Y + 20), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, string_depth);
            spriteBatch.DrawString(font, displayText, new Vector2((int)Position.X + 20, (int)Position.Y + 20), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, string_depth);
        }

        /// <summary>
        /// Sets the Answer for the AnswerBlock
        /// </summary>
        /// <param name="ansValue">Is the value of the Answer </param>
        /// <param name="ansT">Is the type of answer being matched </param>
        /// <param name="dataType">Is the dataType of the Answer</param>
        public void setAnswers(string ansValue, string ansT, string dataType)
        {
            //change format AnswerType="match" Answer="value:FALSE"
            //to AnswerType="value:match" Answer="FALSE"
            answerValue = ansValue;ansType = ansT;   
        }

        /// <summary>
        /// Returns the text that is displayed on the answer block
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return displayText;
        }
    }
}
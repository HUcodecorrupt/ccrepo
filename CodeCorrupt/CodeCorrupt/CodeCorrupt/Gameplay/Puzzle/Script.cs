﻿using CodeCorrupt.LevelEditor;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CodeCorrupt.Gameplay.Puzzle
{

    public class Script
    {
        Dictionary<string, Dialog> dialogs;
        public Script(params Dialog[] dialogs)
        {
            this.dialogs = new Dictionary<string, Dialog>();
            foreach (Dialog dialog in dialogs)
            {
                this.dialogs.Add(dialog.Name, dialog);
            }
        }

        public Dialog this[string name]
        {
            get
            { return dialogs[name]; ; }
        }
    }

    public class Dialog
    {
        public Collection<DialogHandler> handlers;
        string name;
        string text;
        Vector2 location;

        public Dialog(string name, string text, Vector2 location, params DialogHandler[] handlers)
        {
            this.handlers = new Collection<DialogHandler>();
            this.name = name;
            this.text = text;
            this.location = location;
            foreach (DialogHandler handler in handlers)
                this.handlers.Add(handler);
        }

        public int HandlerCount
        {
            get { return handlers.Count; }
        }

        public string Name
        {
            get { return name; }
        }

        public string Text
        {
            get { return text; }
        }

        public Vector2 Location  //location for tutorial arrows/messages
        {
            get { return location; }
        }

        public void InvokeHandler(messageTriggerObj ma, int currentHandler)
        {
            handlers[currentHandler].Invoke(ma);
        }
    }

    public class DialogHandler
    {
        string caption;
        DialogAction[] actions;

        public DialogHandler(string caption, params DialogAction[] actions)
        {
            this.caption = caption;
            this.actions = actions;
        }

        public string Caption
        {
            get { return caption; }
        }

        public void Invoke(messageTriggerObj ma)
        {
            foreach (DialogAction action in actions)
                action.Invoke(ma);
        }
    }

    public class DialogAction
    {
        MethodInfo method;
        object[] parameters;

        public DialogAction(string methodName, object[] parameters)
        {
            method = typeof(messageTriggerObj).GetMethod(methodName);
            this.parameters = parameters;
        }

        public void Invoke(messageTriggerObj ma)
        {
            method.Invoke(ma, parameters);
        }
    }
}

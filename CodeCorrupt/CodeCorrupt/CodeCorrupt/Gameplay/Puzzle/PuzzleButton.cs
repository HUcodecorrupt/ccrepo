﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeCorrupt.Gameplay.Puzzle
{
    class PuzzleButton
    {
        public Rectangle buttonRect,txtRect;

        public bool pressed = false;
        bool hover = false;
        string type;
        string butText;
        Texture2D textureImage;
        Texture2D hoverImage;
        SpriteFont font;
        float but_depth = 0.998f,string_depth=0.995f;

        public PuzzleButton(Rectangle rect, string buttonType)
        {
            buttonRect = rect;
            type = buttonType;
        }

        public PuzzleButton(Rectangle FullRectangle, int size, Texture2D blankImage,Texture2D insideImage)
        {
            hoverImage = insideImage;
        }

        public void setTexture(Texture2D texture, Texture2D htexture)
        {
            textureImage = texture;
            hoverImage = htexture;
        }

        public void setTexture(Texture2D texture)
        {
            textureImage = texture;
        }

        public void setText(string text,SpriteFont sFont)
        {
            butText = text;
            font = sFont;
        }

        public void setText(string text, SpriteFont sFont,Rectangle tRect)
        {
            butText = text;
            font = sFont;
            txtRect = tRect;
        }

        public bool isPressed
        {
            get{  return pressed;}
            set { pressed = value; }
        }

        public Rectangle Rect
        {
            get
            {
                return buttonRect;
            }
        }

        public void Update(bool mouseDown, Rectangle mouseRect)
        {

            if (mouseDown)
            {
                if (buttonRect.Intersects(mouseRect))
                {
                    pressed = true;
                    hover = false;
                }
            }
            else if (buttonRect.Intersects(mouseRect))
            {
                hover = true;
                pressed = false;
            }
            else
            {
                hover = false;
                pressed = false;
            }

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (hover && hoverImage!=null)
                spriteBatch.Draw(hoverImage, buttonRect, null,Color.White,0f,Vector2.Zero,SpriteEffects.None,but_depth);
            else
                spriteBatch.Draw(textureImage, buttonRect,null,Color.White,0f,Vector2.Zero,SpriteEffects.None,but_depth);

            if (butText != null)
            {
                spriteBatch.DrawString(font, butText, new Vector2(txtRect.X, txtRect.Y), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None,string_depth);
            }
            /*
             * Code for guide drawing and colors
            if (type == "guide")
            {
                //set color programmatically
                Color[] color = new Color[textureImage.Width * textureImage.Height];//set the color to the amount of pixels in the textures
                for (int i = 0; i < color.Length; i++)//loop through all the colors setting them to whatever values we want
                {
                    color[i] = Color.White;
                }
                textureImage.SetData(color);
            }*/
        }

    }


}

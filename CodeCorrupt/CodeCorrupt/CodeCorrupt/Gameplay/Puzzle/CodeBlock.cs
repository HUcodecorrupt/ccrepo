﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;


namespace CodeCorrupt.Gameplay.Puzzle
{
    class CodeBlock
    {
        protected Rectangle positionRect;
        protected string name = "", value = "", type = "";
        protected Texture2D textureImage;
        protected float depth = 0.45f,string_depth=0.4f;
        protected Vector2 origin, current_pos;
        protected string displayText="";
        protected Vector2 offset = Vector2.Zero;//float scale = 1;
        public bool showType = true, showName = true, showValue = true;
        public Color color = Color.White;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cb_pos"></param>
        /// <param name="ans_pos"> Is the position of the Answer Block</param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public CodeBlock(Vector2 cb_pos, int width, int height)
        {
            positionRect = new Microsoft.Xna.Framework.Rectangle((int)cb_pos.X, (int)cb_pos.Y, width, height);
            origin = cb_pos;
            current_pos = origin;
        }

        /// <summary>
        /// sets the text of the strings used to check for answers
        /// </summary>
        /// <param name="t"></param>
        /// <param name="n"></param>
        /// <param name="v"></param>
        public void setDataText(string t, string n, string v)
        {
            name = n; value = v; type = t;
            
        }

        public void setDataShown(bool sType, bool sName, bool sValue)
        {
            showType = sType; showName = sName; showValue = sValue;
        }

        public string Name
        {
            get { return name; }
        }

        public string Value
        {
            get { return value; }
        }
        public string Type
        {
            get { return type; }
        }

        public void addDisplayText(string d)
        {
            displayText += d;
        }
        public string getDataText()
        {
            return type + " " + name + " " + value;
        }

        public string getDisplayText()
        {
            if (displayText == "")
                return type + " " + name + " " + value;
            return displayText;            
        }

        public void setTexture(Texture2D t)
        {
            textureImage = t;
        }

        /// <summary>
        /// Is a rectangle that contains the position and dimensions of the CodeBlock
        /// </summary>
        public Rectangle Rect
        {
            get
            {
                return positionRect = new Rectangle((int)current_pos.X+(int)offset.X, (int)current_pos.Y+(int)offset.Y, positionRect.Width, positionRect.Height);
            }
        }

        public float Depth
        {
            get { return depth; }
            set { depth = value; }
        }

        public float StringDepth
        {
            get { return string_depth;}
            set { string_depth = value; }
        }

        public Vector2 Position
        {
            get { return current_pos; }
        }

        public Vector2 Origin
        {
            get { return origin; }
        }

        public void setPos(Vector2 p)
        {
            current_pos = p;
        }

        public void Update(GameTime gameTime)
        {
            //rect = new Rectangle((int)current_pos.X, (int)current_pos.Y, rect.Width, rect.Height);
        }

        /// <summary>
        /// Draws the textureImage at based upon a rectangle's position and size (Texture is stretched to the rectangle)
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(textureImage, Rect, null, color, 0f, Vector2.Zero, SpriteEffects.None, depth);
        }

        /// <summary>
        /// Draws the textureImage at based upon a rectangle's position and size (Texture is stretched to the rectangle)
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="offset">The amount to offset where the texture is drawn on the screen</param>
        public void Draw(SpriteBatch spriteBatch, Vector2 ofs)
        {
            if(offset.X != ofs.X)
                offset = ofs;
            if (offset.Y != ofs.Y)
            {
                offset = ofs;
                //setPos(new Vector2(Rect.X, Origin.Y - offset.Y));
            }
            spriteBatch.Draw(textureImage, new Rectangle(Rect.X, Rect.Y, Rect.Width, Rect.Height), null, color, 0f, Vector2.Zero, SpriteEffects.None, depth);
        }
    }
}
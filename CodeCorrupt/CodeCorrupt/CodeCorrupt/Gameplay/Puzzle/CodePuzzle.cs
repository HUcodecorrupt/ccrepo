﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CodeCorrupt.Gameplay.Puzzle
{
    class CodePuzzle  //represents code sheet
    {
        private Rectangle Rect,gameRect;
        private List<AnswerBlock> ansBlocks; //Test Inventory Code Blocks
        private Texture2D compltPuzzText,redPopUp,needAnsText,regularText,correctAnsText,incorrectAnsText; 
        private Vector2 current_pos;
        private Color color = Color.White;
        private float popUpDepth = 0.0f,ansTextDepth=0.5f,ansBlckDepth = 0.56f;
        private XmlNode root;
        private string xmlFilename="Content/CodePuzzle/OpenDoor1.xml";
        private int numAnswers = 0, timeToShow = 20, showCnt= 0,numCorrect=0;
        private SpriteFont font_small;
        private SpriteFont font_large;
        public string puzzleName = "OpenDoor1.xml",objectiveText="";
        public Script scriptPuzzle,scriptErrors;
        messageTriggerObj messageTrigger;
        public bool showCompleted = false,showFail = false,puzzleFail = false;
        private bool startAttempts = false, display_message;
        public int attempts = 3;
        List<messageTriggerObj> messageList;
       
        //Add constructor to have CodeBlocks already attached to it
        //give xml file a has CodePiece property
        //add a puzzle peice as a child to the puzzleFile
        //Then have the Child PuzzlePeice drawn in the proper pl
        public CodePuzzle(Vector2 pos, int width, int height, string filename)
        {
            showCnt = timeToShow;
            Rect = new Rectangle((int)pos.X, (int)pos.Y, width, height);
            current_pos = pos;
            ansBlocks = new List<AnswerBlock>();
            messageList = new List<messageTriggerObj>();
            xmlFilename = filename;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="game"></param>
        public void LoadContent(Game game)
        {
            gameRect = game.Window.ClientBounds;
            needAnsText = game.Content.Load<Texture2D>(@"CodePuzzle/PuzzleImages/AnswerBlockYellow");
            regularText = game.Content.Load<Texture2D>(@"CodePuzzle/PuzzleImages/AnswerBlockBlue");
            correctAnsText = game.Content.Load<Texture2D>(@"CodePuzzle/PuzzleImages/AnswerBlockGreen");
            incorrectAnsText = game.Content.Load<Texture2D>(@"CodePuzzle/PuzzleImages/AnswerBlockRed");
            compltPuzzText = game.Content.Load<Texture2D>(@"CodePuzzle/PuzzleImages/CompletePuzzle");
            redPopUp = game.Content.Load<Texture2D>(@"CodePuzzle/PuzzleImages/redPopUp");
            font_small = game.Content.Load<SpriteFont>(@"CodePuzzle/sampleFont");
            font_large = game.Content.Load<SpriteFont>(@"CodePuzzle/big_font");
            LoadPuzzle(xmlFilename);
            scriptPuzzle = (game as MainGame).ReadScript(xmlFilename); //read in dialog when Puzzle Starts
            scriptErrors = (game as MainGame).ReadScript(@"Content/CodePuzzle/PuzzleFiles/ErrorMessages.xml");
            foreach (AnswerBlock child in ansBlocks)
            {
                child.LoadContent(game);
            }
            //set the text to show the objective on IDE
            objectiveText = scriptPuzzle["objective"].Text;
           
        }

        /// <summary>
        /// Sets up and resets the properties for a new CodePuzzle
        /// </summary>
        /// <param name="game"></param>
        public void Intialize(Game game)
        {
            attempts = 3;
            showFail = true;
            showCompleted = false;
            //puzzleFail = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private AnswerBlock LastAnswerBlock
        {
            get
            {
                if (ansBlocks.Count > 0)
                    return ansBlocks[ansBlocks.Count - 1];
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<AnswerBlock> AnswerBlocks
        {
            get { return ansBlocks; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return puzzleName; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool displayMessage
        {
            get { return display_message; }
            set { display_message = value; }
        }

        /// <summary>
        /// Function Parses a string for special characters
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string ParseSpecialChars(string value)
        {
            string editValue = value;
            string quotes = "%q",chars="%c";
            //Using backslashes in a regular string, but in Replace it replaces with \" not just " . 
            //So I use a @-delimited string. I also use two single quotes instead of double quotes
            //because I need to have two pairs of \" or double quotes to replace %q with " (is probably a general rule about use \")
            editValue=editValue.Replace(quotes,@"''");
            editValue = editValue.Replace(chars, @"'");
            return editValue;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cBlock"></param>
        public void addAnsBlocks(AnswerBlock cBlock,AnswerBlock parent)
         {
            ansBlocks.Add(cBlock);
            
          //  if (ansBlckDepth <= ansBlocks.Last<AnswerBlock>().Depth)
            //ansBlocks.Last<AnswerBlock>().Depth = dep - 0.001f;
        }

        /// <summary>
        /// Updates the Visuals of the AnswerBlocks that are correct/incorrect
        /// </summary>
        public void updateNumCorrect()
        {
            numCorrect = 0;
            bool hasIncorrect = false;
            foreach (AnswerBlock child in ansBlocks)
            {
                if (child.hasBlock && child.Compare())
                {
                    child.color = Color.LightGreen;
                    child.getSubmitSpace().color = Color.LightGreen;
                    numCorrect++;
                    child.setTexture(correctAnsText);
                    //child.getSubmitSpace();
                    //Do i want to change the color of the submit space? Or make the submit space invsible in the future?
                }
                else if (child.needAnsBlock)
                {
                    hasIncorrect =  true;
                    child.color = Color.Red;
                    child.getSubmitSpace().color = Color.Red;
                    //child.setTexture(incorrectAnsText);
                }
            }
            if(hasIncorrect)
                attempts--;
        }
        /// <summary>
        /// Checks Answers
        /// </summary>
        /// <returns></returns>
        public bool CheckAnswers(Game game, MessagePopUp popMessage)
        {
            //Add new smg pop up stuff
            //use createmsgobject function and send in your own error msg script file
            //Create a switch case of possible errorNames to refer too,
            //also make a switch case for the correct answer!
            //string errorName = CheckError(); //Is the error that I want to refer to
            //createMsgObjects(game.Content, scriptErrors, errorName, popMessage, 0, 0, 0, 0, game);

            //change the texture of the block if answer is right or wrong

            if (numCorrect == numAnswers)
            {
                showCompleted = true;
                if (showCnt <= 0)
                {
                    showCnt = timeToShow;
                    showCompleted = false;
                    return true;
                }
            }
            else if (numCorrect < numAnswers && attempts <= 0)
            {
                showFail = true;
                if (showCnt <= 0)
                {
                    showCnt = timeToShow;
                    showFail = false;
                    //puzzleFail = true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        public void flickerRed(AnswerBlock child,int time)
        {
            /*Work on Function to change the colors of the blocks for 1.5 seconds
             */
            Color clr = Color.White;
            for (int x = 0; x <= time; x++)
            {
                if (x % time == 0)
                {
                    clr = Color.Red;
                }
                else
                    clr = Color.White;

                child.color = clr;
                child.getSubmitSpace().color = clr;
            }
                
        }

        string CheckError()
        {
            string error="";
            /*	At the end of all errors say where error happens, at<insert AnswerBlock>
             * Type mismatch - Can not give a <insert DATA VALUE> into a <Insert Data Type here>
             * Say <insert AnswerBlock> is missing data, it needs a CodePiece attached before you can run this code
             * Can not run <insert function here>
             * Error! Possibility of Divinding by Zero 
             * 
             * Error Class/Struct
             * Need source of Error - the AnswerBlocks Type Name Value + the Submit Block
             * The type of error
             * 
             */ 
            return error;

        }

        /// <summary>
        /// create Message Object
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="script"></param>
        /// <param name="dialogName"></param>
        /// <param name="messageBox"></param>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        /// <param name="lx"></param>
        /// <param name="ly"></param>
        /// <param name="game"></param>
        public void createMsgObjects(ContentManager Content, Script script, string dialogName, MessagePopUp messageBox, int dx, int dy, int lx, int ly, Game game)
        {
            //place dialog names inside of another class.  make dialog Names Array unique to every room
            //messageBox.LoadContent(game);
            //Use invisible_point or invisible_line sprite for texture
            messageTrigger = new messageTriggerObj(Content.Load<Texture2D>(@"LevelObjects/blackBlock"), new Point(dx, dy), new Point(lx, ly), script, messageBox,game);
            //create array for dialogName, use this array to send to messagePopUp class and set new name
            messageTrigger.DialogName = dialogName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlFilename"></param>
        public void LoadPuzzle(string xmlFilename)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFilename);

            root = doc.ChildNodes[1];
            LoadNodes(doc.ChildNodes, null,0);
           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="parent"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        private AnswerBlock createBlock(XmlNode node, AnswerBlock parent, int level)
        {
            string name = "", type = "", value = "", dtext = "",ans="",ansT="";
            int wd = 100, hg = 1000;
            Texture2D newTexture;
            bool needSubmit = false, showType = true, showName = true, showValue = true;
            newTexture = regularText; 
            for (int i = 0; node.Attributes.Count > 0 && i < node.Attributes.Count; i++)
            {
                if (node.Attributes[i].Name.ToUpper() == "NAME")
                {
                    name = node.Attributes[i].Value;
                }
                else if (node.Attributes[i].Name.ToUpper() == "VALUE")
                {
                    value = ParseSpecialChars(node.Attributes[i].Value);
                }
                else if (node.Attributes[i].Name.ToUpper() == "CONDITION")
                {
                    name = ParseSpecialChars(node.Attributes[i].Value) + "\n";
                }
                else if (node.Attributes[i].Name.ToUpper() == "TYPE")
                {
                    type = node.Attributes[i].Value;
                }
                else if (node.Attributes[i].Name.ToUpper() == "WID")
                {
                    wd = Convert.ToInt32(node.Attributes[i].Value);
                }
                else if (node.Attributes[i].Name.ToUpper() == "HGT")
                {
                    hg = Convert.ToInt32(node.Attributes[i].Value);
                }
                else if (node.Attributes[i].Name.ToUpper() == "ANSWER")
                {
                    ans = node.Attributes[i].Value;//ParseSpecialChars(node.Attributes[i].Value);
                    newTexture = needAnsText;
                    numAnswers++;
                }
                else if (node.Attributes[i].Name.ToUpper() == "ANSWERTYPE")
                {
                    ansT=node.Attributes[i].Value;
                }
                else if (node.Attributes[i].Name.ToUpper() == "SHOWT" && node.Attributes[i].Value.ToUpper() == "FALSE")
                {
                    showType = false;
                }
                else if (node.Attributes[i].Name.ToUpper() == "SHOWV" && node.Attributes[i].Value.ToUpper() == "FALSE")
                {
                    showValue = false;
                }
                else if (node.Attributes[i].Name.ToUpper() == "SHOWN" && node.Attributes[i].Value.ToUpper() == "FALSE")
                {
                    showName = false;
                }
            }

            if(showType)
                dtext = type + " ";
            if (showName)
                dtext += name + " ";
            if (needSubmitBlock(node) && showValue)
            {
                needSubmit = true;
                dtext += value + "\n";
            }
            else
            {
                if (type.ToUpper() == "STRING")
                    dtext += "\"" + value + "\"\n";
                else if (type.ToUpper() == "CHAR" || type.ToUpper() == "CHARACTER")
                    dtext += "'" + value + "'\n";
                else if (type.ToUpper() == "BOOL" || type.ToUpper() == "BOOLEAN")
                    dtext += value.ToUpper() + "\n"; 
            }

            //here the AnswerBlock is finally created
            if (parent != null)
            {
                parent.addChild(new AnswerBlock(new Vector2(parent.Position.X + level*30, parent.getNexChildtHeight(node.ParentNode.ChildNodes.Count)),
                    wd, hg, dtext, needSubmit));
                parent.LastChild.setDataText(type, name, value);
                parent.LastChild.setAnswers(ans, ansT,type);
                parent.LastChild.setTexture(newTexture);
                parent.LastChild.setDataShown(showType, showName, showValue);
                parent.LastChild.StringDepth = ansTextDepth;
                return parent.LastChild;
            }
            else//if this is the first AnswerBlock it will go to this case
            {
                AnswerBlock block = new AnswerBlock(current_pos + new Vector2(current_pos.X * 0.2f, current_pos.Y* 0.2f), wd, hg, dtext, needSubmit);
                block.setDataText(type, name, value);
                block.setAnswers(ans, ansT,type);
                block.setTexture(newTexture);
                block.setDataShown(showType, showName, showValue);
                block.Depth = ansBlckDepth;
                block.StringDepth = ansTextDepth;
                return block;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private bool needSubmitBlock(XmlNode node)
        {
            if (node.Attributes.GetNamedItem("Answer") != null)
                return true;
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeList"></param>
        /// <param name="parent"></param>
        /// <param name="level"></param>
        private void LoadNodes(XmlNodeList nodeList, AnswerBlock parent,int level)
        {
            foreach (XmlNode node in nodeList)
            {
                switch (node.NodeType)
                {
                    case XmlNodeType.Element:
                        if (node.Name == "Puzzle")
                        {
                            foreach (XmlAttribute att in node.Attributes)
                            {
                                if (att.Name.ToUpper() == "NAME")
                                    puzzleName = att.Value;
                            }
                            
                            if (node.HasChildNodes)
                                LoadNodes(node.ChildNodes, null, level);
                        }
                        else if (node.Name == "Block")
                        {
                            AnswerBlock current = createBlock(node, parent, level);
                            ansBlocks.Add(current);
                            //addAnsBlocks(current,parent);
                            if (node.HasChildNodes)
                                LoadNodes(node.ChildNodes, current, level+1);
                        }
                        else if (node.Name == "text")
                        {
                            string name = "", type = "", value = "", dtext = "";
                            for (int i = 0; node.Attributes.Count > 0 && i < node.Attributes.Count; i++)
                            {
                                if (node.Attributes[i].Name == "name")
                                {
                                    name = node.Attributes[i].Value;
                                }
                                else if (node.Attributes[i].Name == "value")
                                {
                                    value = node.Attributes[i].Value;
                                }
                                else if (node.Attributes[i].Name == "condition")
                                {
                                    name = node.Attributes[i].Value + " then\n";
                                }
                                else if (node.Attributes[i].Name == "type")
                                {
                                    type = node.Attributes[i].Value;
                                }
                            }
                            dtext = type + " " + name + " " + value + " \n";
                            parent.setDataText(type, name, value);
                            parent.addDisplayText(dtext);
                        }
                        //add code for setting dialogName array
                        break;
                }

            }
        }

        public void Update(GameTime gameTime)
        {
            foreach (AnswerBlock child in ansBlocks)
            {
                child.Update();
            }
            /*make a tag for all the words to be highlighted in every message in the xml file.
             * The wrap text function will acquire this list and compare the words to the list.
             * If a word in the text matches the list, the text will split at that point.  
             * Each split part of the text will be added to a struct array(contains the text and a number indicating if it should be highlighted)
             * that will contain the text.
             * WrapText will then return the array to the previous class. 
             * When the text is outputted, if it's struct number = 1, then it will be highlighted.
             */
            messageTrigger.HandlePlayerInteraction(null);
            messageTrigger.Update(gameTime);
            if (messageTrigger.showingDialog)  //set puzzleIDE to display message pop up
                display_message = true;
            else
                display_message = false;
        }

        /// <summary>
        /// Checks if a CodeBlock collides with another CodeBlock used for answering AnswerBlocks
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool CollideWithCodeBlock(CodeBlock obj)
        {
            foreach (AnswerBlock child in ansBlocks)
            {
                if (new Rectangle(child.Rect.X, child.Rect.Y, child.Rect.Width + 75, child.Rect.Height/2).Intersects(obj.Rect) && child.needAnsBlock)
                {

                    if (child.hasBlock)
                    {
                        child.getSubmitBlock().setPos(child.getSubmitBlock().Origin);
                        child.Update();
                    }
                    child.snapBlock(obj);
                    return true;
                }
            }
            return false;
        }
        
         /// <summary>
        /// 
        /// </summary>
        public void compareOverLap()
        {
        }


        public string WrapText(string text, Rectangle box,SpriteFont spriteFont, ref List<string> highlight_words) //use 'ref' to send List paramater, to call by reference       
        {
            StringBuilder sb = new StringBuilder();
            float spaceWidth = spriteFont.MeasureString(" ").X;
            float length = 0f;
            /*string[] words1 = text.Split('*');
            for (int i = 0; i < words1.Length; i++)
            {
                if (words1[i].StartsWith("*"))
                {
                    words1[i].Remove(0, 1);
                    //get list of words to highlight
                    highlight_words[i] = words1[i];
                    words1[i].Remove(words1[i].Length, 1);
                }
            }*/
            string[] words = text.Split(' '); //split text at every space and get array of words
            foreach (string word in words)
            {

                float size = spriteFont.MeasureString(word).X;
                if (length + size < box.Width - 30)
                {
                    sb.Append(word + " ");
                    length += size + spaceWidth;
                }
                else //if length is same or grater than message box width, append "new line" character to string
                {
                    sb.Append("\n" + "   " + word + " ");
                    length = size + spaceWidth;
                }
            }
            return sb.ToString();
        }

        public void DrawPzleCmpl(SpriteBatch spriteBatch)
        {
            //Have a variable that doesn't allow puzzle complete flag to go up until a certain amount of time passes
            //need a newFlag called showComplete
            //need a counter for how many milliseconds to run it
            //need a texture to draw
            spriteBatch.Draw(compltPuzzText, new Rectangle(gameRect.Width / 3, gameRect.Height / 2 - 100,300,100),null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, popUpDepth);
            showCnt--;
        }
        
        /// <summary>
        /// Draws the Failure Screen for the puzzle
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void DrawPzlFail(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(redPopUp, new Rectangle(gameRect.Width / 3, gameRect.Height / 2 - 100, 300, 100), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, popUpDepth);
            showCnt--;
        }

        public void Draw(GameTime gameTime,SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(backgroundText, Rect, null, color, 0f, Vector2.Zero, SpriteEffects.None, puzzleDepth);
            foreach (AnswerBlock child in ansBlocks)
            {
                child.Draw(spriteBatch, font_small);
            }

            if (showCompleted)
                DrawPzleCmpl(spriteBatch);
            if(showFail)
                DrawPzlFail(spriteBatch);
            messageTrigger.Draw(gameTime, spriteBatch);
        }

    }
}
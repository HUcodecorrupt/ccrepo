﻿/*
This code is released Open Source under the MIT license. Copyright 2009 UberGeekGames, 
All Rights Reserved. You may use it free of charge for any purposes, provided that 
UberGeekGames' copyright is reproduced in your use, and that you indemnify and hold 
harmless UberGeekGames from any claim arising out of any use (or lack of use or lack of 
ability of use) you make of it. This software is provided as-is, without any 
warranty or guarantee, including any implicit guarantee of merchantability or fitness 
for any particular purpose. Use at your own risk!
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CodeCorrupt.Gameplay.Puzzle
{
    /*
     * Directions:
     * 1) Create an instance of ScrollerBar with the values you want.
     * 2) Call scrollerBarInstance.Draw(SpriteBatch) in your Draw loop.
     * 3) Hook scrollerBarInstance.Scroll up to a keyboard or gamepad input.
     * 4) Use scrollerBarInstance.Offset for drawing your objects that you want affected by the scroll bar.
     * 5) (optional) When creating the ScrollerBar object, you can use the overload to make it a horizontal scroll bar!
     * 6) (optional) At any time you can change the colors of the scroll bar by using Color_Background or Color_Scroller!
     * 
     * Note that most variables are private, but have a public get method. This way you won't accidentally change something that
     * can't be changed after initialization, and the vars that can be changed automatically update everything else if they are changed.
     * 
     * "image.jpg" is from the RPG Starter Kit available on the XNA Creators Club site.
     * Original scroll bar code by Fabian Viking.
    */

    #region Orientation enum

    //Enum for scroll bar orientation.
    public enum ScrollBarOrientation
    {
        Vertical, Horizontal
    }

    #endregion

    public class ScrollBar
    {
        #region Variables

        /// <summary>
        /// Orientation of the scroll bar.
        /// </summary>
        private ScrollBarOrientation orientation = ScrollBarOrientation.Vertical;

        //getter for orientation - you don't want to accidentally change it mid program!
        /// <summary>
        /// Orientation of the scroll bar.
        /// </summary>
        public ScrollBarOrientation Orientation
        {
            get
            {
                return orientation;
            }
        }

        /// <summary>
        /// Total height of whatever content that will be scrolled. This could be an image or a set of objects, or anything else!
        /// </summary>
        private int heightOfContent;

        /// <summary>
        /// Total height of whatever content that will be scrolled. This could be an image or a set of objects, or anything else!
        /// You can change it at any time, and everything will be automatically updated to reflect the new values!
        /// </summary>
        public int HeightOfContent
        {
            get
            {
                return heightOfContent;
            }
            set
            {
                heightOfContent = value;
                Init(destRect, value, blank, barImage);
            }
        }

        /// <summary>
        /// Height of the inside scroll bar. This will never be larger than the height of the background scroll bar.
        /// </summary>
        private int scrollBarHeight;

        /// <summary>
        /// Height of the inside scroll bar. This will never be larger than the height of the background scroll bar.
        /// </summary>
        public int ScrollBarHeight
        {
            get
            {
                return scrollBarHeight;
            }
        }


        /// <summary>
        /// Maximum position of the inside scroll bar.
        /// </summary>
        private int scrollBarMaxPos;

        /// <summary>
        /// Maximum position of the inside scroll bar.
        /// </summary>
        public int ScrollBarMaxPos
        {
            get
            {
                return scrollBarMaxPos;
            }
        }

        /// <summary>
        /// Position of the inside scroll bar.
        /// </summary>
        private int scrollBarPos = 0;
        bool hover = false;

        /// <summary>
        /// Position of the inside scroll bar. Note that it can be either in the Y or X axis direction, depending on the orientation of the scroll bar.
        /// </summary>
        public int ScrollBarPosition
        {
            get
            {
                return scrollBarPos;
            }
            set
            {
                scrollBarPos = value;
            }
        }

        /// <summary>
        /// Color for the background of the scroll bar.
        /// </summary>
        public Color Color_Background = Color.White;

        /// <summary>
        /// Color for the inside of the scroll bar.
        /// </summary>
        public Color Color_Scroller = Color.LightSkyBlue;

        /// <summary>
        /// Blank texture that is used to draw the scroll bars. For interesting effects, you could change this to an image with a pattern on it
        /// </summary>
        private Texture2D blank;
        private Texture2D barImage;

        /// <summary>
        /// The full position, width, and height of the scroll bar to draw.
        /// </summary>
        private Rectangle destRect;

        /// <summary>
        /// The full position, width, and height of the scroll bar to draw.
        /// You can change it at any time, and everything else will be updated to reflect the new values!
        /// </summary>
        public Rectangle SizePositionRectangle
        {
            get
            {
                return destRect;
            }
            set
            {
                destRect = value;
                Init(value, heightOfContent, blank, barImage);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// The current position offset of the scroll bar. Use this for the positioning of images or anything else you're using the scroll bar for.
        /// Note that it's called 'Offset' since it could be horizontal.
        /// </summary>
        public int Offset
        {
            get
            {
                //accounts for horizontal and vertical orientations
                if (orientation == ScrollBarOrientation.Vertical)
                {
                    //calculate position of offset
                    float showProcent = (float)scrollBarPos / ScrollBarMaxPos;
                    return (int)((heightOfContent - (heightOfContent+destRect.Height)) * showProcent);
                }
                else //scroll bar must be horizontal
                {
                    //calculate position of offset
                    float showProcent = (float)scrollBarPos / ScrollBarMaxPos;
                    return (int)((heightOfContent - destRect.Width) * showProcent);
                }
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new ScrollBar object. Overload: sets the orientation.
        /// </summary>
        /// <param name="FullRectangle">The position, width, and height of your scroll bar</param>
        /// <param name="heightOfContent">The maximum height of your content that the scrollbar will be moving</param>
        /// <param name="blankImage">A blank, white, 1x1 texture</param>
        /// <param name="orientation">Orientation of the scroll bar</param>
        public ScrollBar(Rectangle FullRectangle, int sizeOfContent, Texture2D blankImage, Texture2D insideImage, ScrollBarOrientation orientation)
        {
            this.orientation = orientation;
            Init(FullRectangle, sizeOfContent, blankImage, insideImage);
        }

        /// <summary>
        /// Creates a new ScrollBar object. Defaults to a Horizontal orientation.
        /// </summary>
        /// <param name="FullRectangle">The position, width, and height of your scroll bar</param>
        /// <param name="heightOfContent">The maximum height of your content that the scrollbar will be moving</param>
        /// <param name="blankImage">A blank, white, 1x1 texture</param>
        public ScrollBar(Rectangle FullRectangle, int sizeOfContent, Texture2D blankImage, Texture2D insideImage)
        {
            Init(FullRectangle, sizeOfContent, blankImage, insideImage);
        }

        private int MouseToScrollPosition(int mousePosition)
        {
            float Range = (float)this.scrollBarMaxPos - this.Offset;
            float y = (float)mousePosition;
            float ratio = Range / (this.SizePositionRectangle.Height - (float)this.scrollBarHeight);
            float position = ratio * y;

            try
            {
                return Convert.ToInt32(position);
            }
            catch
            {
                return 0;
            }
        }


        //Initialization that is shared between constructors
        private void Init(Rectangle FullRectangle, int sizeOfContent, Texture2D blankImage, Texture2D insideImage)
        {
            //sets some variables
            this.destRect = FullRectangle;
            this.blank = blankImage;
            this.heightOfContent = sizeOfContent;
            this.barImage = insideImage;

            //calculates the maximum position for the inside scroll bar.
            //if it's a horizontal scroll bar then we use the width instead of height.
            if (orientation == ScrollBarOrientation.Vertical)
            {
                float showProcent = (float)destRect.Height / (float)heightOfContent;
                scrollBarHeight = (int)(destRect.Height * showProcent);

                scrollBarMaxPos = destRect.Height - scrollBarHeight;
            }
            else if (orientation == ScrollBarOrientation.Horizontal)
            {
                float showProcent = (float)destRect.Width / (float)heightOfContent;
                scrollBarHeight = (int)(destRect.Width * showProcent);

                scrollBarMaxPos = destRect.Width - scrollBarHeight;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Scrolls the scrollbar
        /// </summary>
        /// <param name="dir">Direction and velocity to scroll</param>
        public void Scroll(int dir)
        {
            scrollBarPos += dir;
            if (scrollBarPos < 0) { scrollBarPos = 0; }
            else if (scrollBarPos > ScrollBarMaxPos) { scrollBarPos = ScrollBarMaxPos; }
        }

        public void ScrollDrag(int mouseOffset,int mousePos)
        {
            float mouseRelative = mousePos - ((destRect.Height + scrollBarHeight) / 2);
            //mouseRelative = Math.Min(mouseRelative, destRect.Height - scrollBarHeight);
            //mouseRelative = Math.Max(0, mouseRelative);
            float value = scrollBarMaxPos + (mouseRelative / (destRect.Height - scrollBarHeight)) * ScrollBarMaxPos;
            scrollBarPos = (int)value;//mousePos+mouseOffset;
            if (scrollBarPos < 0) 
             scrollBarPos = 0; 
            else if (scrollBarPos > ScrollBarMaxPos) { 
                scrollBarPos = ScrollBarMaxPos; }
        }

        public void hoverUpdate(Rectangle mouseRect)
        {
            if (new Rectangle(destRect.X, destRect.Y + scrollBarPos, destRect.Width, scrollBarHeight).Intersects(mouseRect))
            {
                hover = true;
            }
            else
            {
                hover = false;
            }
        }

        /// <summary>
        /// Draws the entire scroll bar.
        /// </summary>
        /// <param name="spriteBatch">The active SpriteBatch to use for drawing</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            //draw the background scroll bar
            spriteBatch.Draw(blank, destRect,
                null,
                Color_Background, 0, Vector2.Zero, SpriteEffects.None, 0.2f);

            if (hover)
                spriteBatch.Draw(blank,
                    new Rectangle(destRect.X, destRect.Y + scrollBarPos, destRect.Width, scrollBarHeight), null,
                    Color.Black, 0, Vector2.Zero, SpriteEffects.None, 0.1f);
            else
            {
                //draw the inside scroll bar.
                //if it's a horizontal scroll bar then it uses width instead of height.
                if (orientation == ScrollBarOrientation.Vertical)
                {
                    spriteBatch.Draw(barImage,
                        new Rectangle(destRect.X, destRect.Y + scrollBarPos, destRect.Width, scrollBarHeight), null,
                        Color_Scroller, 0, Vector2.Zero, SpriteEffects.None, 0.1f);
                }
                else if (orientation == ScrollBarOrientation.Horizontal)
                {
                    spriteBatch.Draw(blank,
                        new Rectangle(destRect.X + scrollBarPos, destRect.Y, scrollBarHeight, destRect.Height), null,
                        Color_Scroller, 0, Vector2.Zero, SpriteEffects.None, 0.1f);
                }
            }
        }

        #endregion
    }
}

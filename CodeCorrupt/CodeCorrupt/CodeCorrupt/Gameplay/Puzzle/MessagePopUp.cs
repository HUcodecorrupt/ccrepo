﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*
 * How this message pop up works
 * Create a new instance of the class sending it an array of messages and the viewport
 * The texture used for the box is set in this class
 * call the update and draw functions in your code 
 **/
namespace CodeCorrupt.Gameplay.Puzzle
{
    public class MessagePopUp : Microsoft.Xna.Framework.DrawableGameComponent//DialogComponent
    {
        private Rectangle msgBox;
        private Rectangle mouseRect;
        private Rectangle msgButEnd;
        private Rectangle msgButNext;
        private Rectangle arrowRect;
        private MouseState mState;
        private MouseState mOldState;
        private KeyboardState kState;
        private KeyboardState kOldState;
        Texture2D msgTexture;
        Texture2D msgOutline;
        Texture2D nxtButTexture;
        Texture2D endButTexture;
        Texture2D arrowTexture;
        bool showMessage = false;
        public bool Played = false; //bool that dialog has been played
        public string[] messages;
        int msgNum = 0;
        SpriteFont spriteFont;
        public Dialog dialog = null;
        public messageTriggerObj msgObj = null;
        int currentHandler = 0;
        SpriteBatch spriteBatch;
        List<string> highlight_words = new List<string>();

        //make a list of messages to be drawn on message box
        //messages can be clicked through using the next button
        //public MessagePopUp(string[] Messages, Viewport view, Game game)
        public MessagePopUp(Viewport view, Game game)
            :base(game)
        {
            //messages = Messages;
            msgBox = new Rectangle(view.Width / 3-50, view.Height - view.Height/3, view.Width / 2 - 50, view.Height / 3-50);
            msgButEnd = new Rectangle(msgBox.Left + msgBox.Width / 2 - 70, msgBox.Bottom - 50, 60, 25);
            msgButNext = new Rectangle(msgBox.Right - msgBox.Width / 2 + 20, msgBox.Bottom - 50, 60, 25);
            spriteBatch = new SpriteBatch(game.GraphicsDevice);
            //spriteBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));
            
        }

        public void LoadContent(Game game)
        {
            msgTexture = game.Content.Load<Texture2D>(@"LevelObjects/BlueBox");
            msgOutline = game.Content.Load<Texture2D>(@"UI/box_outline");
            nxtButTexture = game.Content.Load<Texture2D>(@"UI/NEXT_button");
            endButTexture = game.Content.Load<Texture2D>(@"UI/EXIT_button");
            spriteFont = game.Content.Load<SpriteFont>(@"CodePuzzle/sampleFont");
            arrowTexture = game.Content.Load<Texture2D>(@"Images/arrow_pointR");
        }

        public bool ShowMessage
        {
            get { return showMessage; }
            set { showMessage = value; }
        }

        //updates along with game
        public override void Update(GameTime gametime) 
        {

            mState = Mouse.GetState();
            kState = Keyboard.GetState();
            bool mouseDown = mState.LeftButton == ButtonState.Pressed && mOldState.LeftButton == ButtonState.Released;
            bool mouseUp = mState.LeftButton == ButtonState.Released && mOldState.LeftButton == ButtonState.Pressed;

            mState = Mouse.GetState();
            mOldState = mState;
            mouseRect = new Rectangle(mState.X, mState.Y, 50, 50);

            if (dialog != null && msgObj != null)
            {
                //OK button has been pressed, end the guide
                if (mouseDown || Keyboard.GetState().IsKeyDown(Keys.Enter))
                {
                    if (mouseRect.Intersects(msgButEnd) || Keyboard.GetState().IsKeyDown(Keys.Enter))
                    {
                        showMessage = false;
                        msgObj.StopDialog(); //stop showing dialog
                    }
                    //if next button is pressed, display next message
                    else if (mouseRect.Intersects(msgButNext))
                    {
                        //set msgObj.dialogName to new name
                        dialog.InvokeHandler(msgObj, currentHandler);
                        currentHandler = 0;

                        //if (msgNum < messages.Length - 1)
                        //    msgNum++;
                    }
                }
                else if (CheckKey(Keys.Space) || CheckKey(Keys.Enter))
                {
                    dialog.InvokeHandler(msgObj, currentHandler);
                    currentHandler = 0;
                }
            }
            kOldState = kState;
            base.Update(gametime);
        } 

        private bool CheckKey(Keys theKey)
        {
            return kOldState.IsKeyDown(theKey) && kState.IsKeyUp(theKey);
        }

        private string WrapText(string text)
        {
            StringBuilder sb = new StringBuilder();
            float spaceWidth = spriteFont.MeasureString(" ").X;
            float length = 0f;
            string[] words = text.Split(' '); //split text at every space and get array of words
            for (int i = 0; i < words.Length; i++)
            {
                //handle enter key characters from xml files
                if (words[i].Contains("\r") || words[i].Contains("\t") || words[i].Contains("\n"))
                {
                    words[i] = words[i].Replace("\n", "");
                    words[i] = words[i].Replace("\r", "");
                    words[i] = words[i].Replace("\t", "");
                }
                sb.Append(words[i] + " ");
            }

 /*           foreach (string word in words)
            {
                float size = spriteFont.MeasureString(word).X;
                if (length + size < msgBox.Width - 80)
                {
                    sb.Append(word + " ");
                    length += size + spaceWidth;
                }
                else //if length is same or grater than message box width, append "new line" character to string
                {
                    sb.Append("\n" + word + " ");
                    length = size + spaceWidth;
                }
            }*/
            return sb.ToString();
           // return text;
        }

        public void Draw(GameTime gameTime,SpriteBatch spriteBatch) //draws along with game
        {
            //base.Draw(gameTime);
            if (showMessage)
            {
                
                string text = WrapText(dialog.Text); //set text to display
                //Create a loop to look through text for each word that should be highlighted.
                //the words to be highlighted have been indicated by the xml file.

                //spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied);
                spriteBatch.Draw(msgTexture, msgBox, null, Color.White,0f, Vector2.Zero, SpriteEffects.None,0.2f);
                spriteBatch.Draw(msgOutline, msgBox,null, Color.White,0f, Vector2.Zero, SpriteEffects.None,0.1f);
                if (dialog.Location.X > 0 && dialog.Location.Y > 0)  //only draw if location has a value.
                {
                    arrowRect = new Rectangle((int)dialog.Location.X, (int)dialog.Location.Y, 30, 30);
                    spriteBatch.Draw(arrowTexture, arrowRect, null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.1f);
                }
                Vector2 textPosition = new Vector2(msgBox.X + 50, msgBox.Y + 30);
                string[] sub = text.Split(' ');
                float spaceWidth = spriteFont.MeasureString(" ").X;
                float length = 0f;
                float size = 0f;
                float prevSize = 0f;
                foreach (string word in sub)
                {
                    size = spriteFont.MeasureString(word).X;
                    if (size + length < msgBox.Width - 80)
                    {
                        textPosition.X = (textPosition.X + prevSize) + spaceWidth;
                        length += size + spaceWidth;
                    }
                    else //if length is same or grater than message box width, append "new line" character to string
                    {
                        textPosition.X = msgBox.X + 50;

                        textPosition.Y = textPosition.Y + 20;
                        length = size + spaceWidth;
                    }
                    //Checks if word is one of the indicated words to highlight
                    //for (int i = 0; i < highlight_words.Count; i++)
                    //{
                    if (word.StartsWith("*"))
                    {
                        spriteBatch.DrawString(spriteFont, word.Remove(0, 1), textPosition, Color.Red, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                        size = spriteFont.MeasureString(word.Remove(0, 1)).X;
                    }
                    //}
                    //if normal word, then write text normally
                    else
                    {
                        spriteBatch.DrawString(spriteFont, word, textPosition, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                    }
                    prevSize = size;
                                          
                    
                }
                //spriteBatch.DrawString(spriteFont, text, textPosition, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                /*for (int i = 0; i < dialog.handlers.Count; i++)
                {
                    text = dialog.handlers[i].Caption;
                    textPosition.Y += spriteFont.LineSpacing;
                    spriteBatch.DrawString(spriteFont, text, textPosition, Color.Black);
                }*/
                //spriteBatch.DrawString(spriteFont, messages[msgNum], new Vector2(msgBox.X + 50, msgBox.Y + 50), Color.Black,0f, Vector2.Zero,1f, SpriteEffects.None,0f);
                spriteBatch.Draw(endButTexture, msgButEnd, null, Color.White,0f, Vector2.Zero, SpriteEffects.None,0f);
                spriteBatch.Draw(nxtButTexture, msgButNext, null, Color.White,0f, Vector2.Zero, SpriteEffects.None,0f);

                //spriteBatch.End();
            }
        }

        public void Show()
        {
            Enabled = true;
            Visible = true;
            Played = true;
        }
        public void Hide()
        {
            Enabled = false;
            Visible = false;
        }
    }
}

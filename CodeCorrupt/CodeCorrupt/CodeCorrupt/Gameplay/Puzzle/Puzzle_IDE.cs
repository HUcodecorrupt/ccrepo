﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeCorrupt;
using CodeCorrupt.LevelEditor;

namespace CodeCorrupt.Gameplay.Puzzle 
{
    class Puzzle_IDE
    {
        //Mouse & Mouse Tracking Variables
        MouseState curState;
        Rectangle mouseRect;
        MouseState prevState;
        Vector2 mouseOffset;
        int mOffset;
        private SpriteFont spriteFont;
        private SpriteFont font_large;

        public int ideGridWidth = 0;
        public int ideGridHeight = 0;
        public const int codeGridWidth = 750;
        public const int codeGridHeight = 590;
        public const int blockInventoryHeight = 0;
        public const int blockInventoryWidth = 100;
        Vector2 ideGridOrigin = new Vector2(0, 0);
        private List<CodeBlock> codeInvBlocks;
        Vector2 inventoryDisplayOrigin = new Vector2(776, 89); //set top left hand corner origin for inventory
        Rectangle EmptyBlock = new Rectangle(1, 247, 149, 75); //temporary coordinates
        Rectangle codeRec; //rectangle surrounding code sheet grid
        Rectangle inventoryRec; //rectangle surrounding of inventory
        Rectangle topRec;
        Rectangle msgRec;
        Rectangle guideBox;

        Rectangle gridLine;
        //string[] codeSheets; Array for all codeSheets
        //public CodeBlock[,] codeInventory = new CodeBlock[1, 4]; 2d array for code acquired, 1st row gives code sheet it's in, 2nd row gives location in that sheets inventory
        CodeBlock selectedBlock, prevBlock;
        CodePuzzle codePuz1;
        Texture2D sheetOutline;
        Texture2D invOutline;
        Texture2D topOutline;
        Texture2D msgOutline;

        bool tutorialOn = false; // if true, tutorial will show
        bool puzzle_exit = false, puzzle_complete = false,puzzleFail = false;

        Texture2D puzzleScreen;
        Texture2D gridPixel;
        Texture2D guideOverlay;
        Texture2D guideBoxTexture;
        Texture2D grayBoxTexture;
        int inventoryNum = 3;
        int msgCount = 0;
        int hintNum = 0; 
        int hintCount = 0; //number of hint messages for puzzle
        //string[] messages;

        List<string> MBOPTIONS = new List<string>(); //message box list

        //buttons
        PuzzleButton hintButton;
        PuzzleButton runButton;
        PuzzleButton compileButton;
        PuzzleButton exitButton;

        private Texture2D scrollbarTexture;
        private Texture2D scrollbarTextureIn;
        Rectangle scrollbarRec;
        ScrollBar vScrollBar;
        Texture2D scroll_blank;
        private const int V_OFFSET = 35;
        bool scroll = false;
        Viewport viewportINV;
        Viewport viewportGame;
        MessagePopUp popMessage;

        List<PuzzlePiece> puzzlePieceList;
        List<string> highlight_words = new List<string>();

        float UI_boxDepth = 0.998f;
        float string_depth = 0.995f;
        //String used for Testing do no delete
        string outputBackEnd = "";
        public Puzzle_IDE(Game game, GraphicsDeviceManager g)
        {
            viewportGame = g.GraphicsDevice.Viewport;

            ideGridHeight = viewportGame.Height - 200;
            ideGridWidth = viewportGame.Width / 3-10;       
            topRec = new Rectangle(0, 0, viewportGame.Width, 50);
            msgRec = new Rectangle(viewportGame.Width - ideGridWidth - 20, topRec.Bottom + 25, viewportGame.Width / 3, 100);
            codeRec = new Rectangle(10, topRec.Bottom + 25, viewportGame.Width - ideGridWidth - 40, viewportGame.Height-70); //rectangle surrounding code sheet grid
            scrollbarRec = new Rectangle(0, 0, ideGridWidth - 10, ideGridHeight);
            viewportINV = new Viewport(viewportGame.Width-ideGridWidth-20, msgRec.Bottom + 25, ideGridWidth+10, ideGridHeight); //set rectangle of inventory viewport
            runButton = new PuzzleButton(new Rectangle(topRec.Left + 200, 5, 45, topRec.Bottom - 10), "run");
            compileButton = new PuzzleButton(new Rectangle(runButton.buttonRect.Left - 60, runButton.buttonRect.Y, 40, topRec.Bottom - 10), "compile");
            hintButton = new PuzzleButton(new Rectangle(msgRec.Left, msgRec.Top - 25, 45, 25), "hint");
            
            exitButton = new PuzzleButton(new Rectangle(topRec.Left, topRec.Top, 50, 50), "exit");
            
            gridLine = new Rectangle(0, 50, ideGridWidth, 1);
        }

        public void LoadPuzzle(Game game, String filename, List<PuzzlePiece> invBlocks)
        {
            int i = 0;
            //*******Reset Variables when a New Puzzle is Loaded********//
            puzzle_complete = false;
            puzzle_exit = false;
            puzzleFail = false;
            hintButton.isPressed = false;
            runButton.isPressed = false;
            compileButton.isPressed = false;
            exitButton.isPressed = false;

            codeInvBlocks = new List<CodeBlock>();
            codePuz1 = new CodePuzzle(new Vector2(codeRec.X , codeRec.Y), codeRec.Width, codeRec.Height, filename);
            puzzlePieceList = invBlocks;
            foreach (PuzzlePiece child in invBlocks)
            {
                string dtext = "";
                int pixX = (int)scrollbarRec.X + 30;
                int pixY = (int)scrollbarRec.Y + (i * 70); //change y position down inventory block.  Max is set by division
                codeInvBlocks.Add(new CodeBlock(new Vector2(pixX, pixY + 5), child.dimensions.X, child.dimensions.Y));

                if (child.showType)
                    dtext = child.type;
                
                if(child.showName)
                    dtext+=" " + child.name + " ";

                //Sets how the Value of the CodeBlock will be displayed.
                if (child.showValue)
                {
                    if (child.type.ToUpper() == "STRING")
                        dtext += "\"" + child.value + "\"";
                    else if (child.type.ToUpper() == "CHAR" || child.type.ToUpper() == "CHARACTER")
                        dtext += "'" + child.value + "'";
                    else if (child.type.ToUpper() == "BOOL" || child.type.ToUpper() == "BOOLEAN")
                        dtext += child.value.ToUpper();
                    else
                        dtext += child.value;
                }
                codeInvBlocks[i].setDataText(child.type, child.name, child.value);
                codeInvBlocks[i].addDisplayText(dtext);
                codeInvBlocks[i].setDataShown(child.showType, child.showName, child.showValue);
                //dynamically get size of each code block
                i++;
            }

            //Loads Puzzles Content
            codePuz1.LoadContent(game);
            //Load the CodeBlocks
            foreach (CodeBlock child in codeInvBlocks)
            {
                child.setTexture(game.Content.Load<Texture2D>(@"CodePuzzle/PuzzleImages/CodeInv small"));
            }

            popMessage = new MessagePopUp(viewportGame, game);
            popMessage.LoadContent(game);
            codePuz1.createMsgObjects(game.Content, codePuz1.scriptPuzzle, codePuz1.puzzleName, popMessage, 0, 0, 0, 0, game); //create new message object
        }

        /// <summary>
        /// Updates the PuzzlePieceList based upon the current CodeBlocks not used in the Puzzle. Also deletes the used answers from the Inventory
        /// </summary>
        public void updatePuzzlePieces()
        {
            PuzzlePiece temp = puzzlePieceList[0];

            //Delete the answers already used
            foreach (AnswerBlock child in codePuz1.AnswerBlocks)
            {
                if (child.hasBlock)
                {
                    codeInvBlocks.Remove(child.getSubmitBlock());
                }
            }

            //Empties the PuzzlePeice inventory and restocks it with the unused answers
            puzzlePieceList.Clear();
            foreach (CodeBlock child in codeInvBlocks)
            {
                puzzlePieceList.Add(new PuzzlePiece(temp.texture, temp.dimensions, temp.location, child.Name, child.Value, child.Type,child.showType,child.showName,child.showValue));
            }
        }
        
        public void LoadContent(Game game)
        {
            puzzleScreen = game.Content.Load<Texture2D>(@"UI/bkg_puzzle");
            sheetOutline = game.Content.Load<Texture2D>(@"UI/codeSheetB");
            invOutline = game.Content.Load<Texture2D>(@"UI/inventoryB");
            topOutline = game.Content.Load<Texture2D>(@"UI/horizontalBoxS_BL");
            msgOutline = game.Content.Load<Texture2D>(@"UI/horizontalBoxB");
            runButton.setTexture(game.Content.Load<Texture2D>(@"UI/RUN_button2"), game.Content.Load<Texture2D>(@"UI/RUN_buttonG2"));
            hintButton.setTexture(game.Content.Load<Texture2D>(@"UI/horizontalBoxGray"), game.Content.Load<Texture2D>(@"UI/horizontalBoxGreen"));
            exitButton.setTexture(game.Content.Load<Texture2D>(@"UI/exit_box"), game.Content.Load<Texture2D>(@"UI/exit_boxR"));
            gridPixel = game.Content.Load<Texture2D>(@"UI/black_line");
            spriteFont = game.Content.Load<SpriteFont>(@"CodePuzzle/sampleFont");
            font_large = game.Content.Load<SpriteFont>(@"CodePuzzle/big_font");
            hintButton.setText("HINT", spriteFont,hintButton.Rect);
            scrollbarTexture = game.Content.Load<Texture2D>(@"UI/inventoryOutline");
            scrollbarTextureIn = game.Content.Load<Texture2D>(@"UI/inventory");
            guideOverlay = game.Content.Load<Texture2D>(@"LevelObjects/blackBlock");
            guideBoxTexture = game.Content.Load<Texture2D>(@"LevelObjects/BlueBox");
            grayBoxTexture = game.Content.Load<Texture2D>(@"UI/horizontalBoxGray");
            
            vScrollBar = new ScrollBar(new Rectangle(ideGridWidth-20, 15, 25, 200), ideGridHeight, scrollbarTexture,scrollbarTextureIn);
            scrollbarRec = new Rectangle(5, vScrollBar.Offset + 10, ideGridWidth, ideGridHeight);
        }
        public bool puzzleEnd
        {
            get { return puzzle_exit; }
            set { puzzle_exit = value; }
        }

        public bool puzzleComplete
        {
            get { return (puzzle_exit && puzzle_complete); }
        }

        public bool puzzleFailed
        {
             get { return codePuz1.puzzleFail;}
        }

        //keyboard input for scoll bar
        private void handle_keyboard()
        {
            KeyboardState keyboardState = Keyboard.GetState();

            if (keyboardState.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Down))
            {
                vScrollBar.Scroll(2);
            }
            else if (keyboardState.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Up))
            {
                vScrollBar.Scroll(-2);
            }
        }
             
        //code for pausing the puzzle 
        /*private void BeginPause(bool initiate)
        {
            pauseGuide = initiate;
        }

        private void EndPause()
        {
            pauseGuide = false;
        }

        
        private void checkPauseGuide()
        {
            // Pause if the Guide is up
            if ( popMessage.ShowMessage == true)
            {
                BeginPause(true);
                //Guide.BeginShowMessageBox("Tutorial", "welcome to the puzzle", MBOPTIONS, 0, MessageBoxIcon.None, GetMBResult, null);
            }
            // If we paused for the guide, unpause if the guide
            // went away
            else if (pauseGuide && !guideVisible)
            {
                EndPause();
            }
        }*/

        public void Update(GameTime gameTime,MainGame game)
        {
            /*if (tutorialOn && codePuz1.Name == "OpenDoor1.xml") //show tutorial if first open door puzzle
            {
                guideVisible = true;
                popMessage.ShowMessage = true;
            }*/

            prevState = curState;
            curState = Mouse.GetState();
            mouseRect = new Rectangle(curState.X, curState.Y, 1, 1);

            // Was the mouse button pressed this frame?  
            //mouseDown and mouseUp are not updating correctly, Why?
            bool mouseDown = curState.LeftButton == ButtonState.Pressed && prevState.LeftButton == ButtonState.Released;
            bool mouseUp = curState.LeftButton == ButtonState.Released && prevState.LeftButton == ButtonState.Pressed;
            bool mouseMove = curState.LeftButton == ButtonState.Pressed && prevState.LeftButton == ButtonState.Pressed;

            //checkPauseGuide();
            if (codePuz1.displayMessage)
            {
                codePuz1.Update(gameTime);
                popMessage.ShowMessage = true;
            }
            else if (!codePuz1.showCompleted && !codePuz1.showFail)//to not do PuzzleIDE updates if the Puzzle is that its been Showing Completed
            {
                popMessage.ShowMessage = false;
                //If game is not paused
                //if (!pauseGuide)
                //{
                mouseDragDrop(mouseDown, mouseUp, mouseMove);
                UI_Update(mouseRect, mouseUp, game);
                codePuz1.Update(gameTime);
                //}
            }
            AnswerUpdate(game);
        }

        public void mouseDragDrop(bool mouseDown,bool mouseUp, bool mouseMove)
        {
            // Was the mouse button pressed this frame?  

            //mInvRect is a mouse for Inventor View Port 
            Rectangle mInvRect = new Rectangle(mouseRect.X - viewportINV.X, mouseRect.Y - viewportINV.Y, mouseRect.Width, mouseRect.Height);
            //mGameRect is a mouse for Game View Port
            //Rectangle mGameRect = new Rectangle(mouseRect.X - viewportGame.X, mouseRect.Y - viewportGame.Y, mouseRect.Width, mouseRect.Height);
            Rectangle mRect = mouseRect;

            if (mouseRect.Intersects(viewportINV.Bounds) && mouseRect.X + mouseRect.Width > viewportINV.X)
                mRect = mInvRect;
            else 
                mRect = mouseRect;

            outputBackEnd = "Mouse " + mRect+" ";
            Vector2 mouseHit = new Vector2(mRect.X, mRect.Y);

            //current mouse Location
            int mHit = mRect.Y;
            //if mouse is pressed, initiate these actions and checks
            if (mouseDown)
            {
                if (mRect.Intersects(viewportGame.Bounds) && mRect.Intersects(viewportINV.Bounds) == false)
                    vScrollBar.ScrollBarPosition = 0;
                // Test each MouseSprite  
                foreach (CodeBlock child in codeInvBlocks)
                {
                    if (mRect == mInvRect)
                    {
                        if (child.Rect.Intersects(mRect) && child.Rect.Intersects(inventoryRec))
                        {
                            //mRect = mouseRect;
                            //mouseHit = new Vector2(mRect.X, mRect.Y);
                            // We have a hit.  
                            selectedBlock = child;
                            prevBlock = child;
                            mouseOffset = child.Position - mouseHit;
                            break;
                        }
                    }
                    else 
                    {
                        //BUG - code blocks exist in inventory and on large game screen.  It exists twice.
                        //if user clicks the right location, they can pick up code peices outside the inventory even if it is only being
                        //drawn in the inventory.
                        if(child.Rect.Intersects(mRect) && child.Rect.Intersects(viewportINV.Bounds) == false )
                        {
                            //mRect = mouseRect;
                            //mouseHit = new Vector2(mRect.X, mRect.Y);
                            // We have a hit.  
                            selectedBlock = child;
                            prevBlock = child;
                            mouseOffset = child.Position - mouseHit;
                            break;
                        }
                    }
                }
                //move scroll bar based upon mouse input
                if (mRect == mInvRect && mRect.Intersects(vScrollBar.SizePositionRectangle))
                {
                    mouseOffset = new Vector2(vScrollBar.SizePositionRectangle.X, vScrollBar.SizePositionRectangle.Y) - mouseHit;
                    //vScrollBar.ScrollDrag(mOffset, mHit);
                    scroll = true;

                }
            
            }
            // If the mouse button is up, drop whatever we have (if anything).
            else if (mouseUp)
            {

                //if there is no selected  block but there was one, the mouse has let go of the block
                if (prevBlock != null)
                {
                    //if CollideWithCodeBlock return false then reset the previous block's position
                    if (!codePuz1.CollideWithCodeBlock(prevBlock))
                        prevBlock.setPos(prevBlock.Origin);
                    prevBlock = null;
                }
                selectedBlock = null;

            }
            else if(mouseMove)
            { 
                //stops scrollbar from moving when mouse it outside of inventory
                if (scroll)
                {
                    if (mRect != mInvRect)
                        scroll = false;
                }
            }

            // If a block is selected by the mouse make its position based upon the mouse  
            if (selectedBlock != null)
            {
                selectedBlock.setPos(new Vector2(mouseRect.X, mouseRect.Y) + mouseOffset);
            }



            //mouse snaps funny at the top, FIX!!
            if (scroll)
                vScrollBar.ScrollDrag((int)mouseOffset.Y, mHit);
            if (scroll && mouseUp)
            {
                scroll = false;
            }
            //code to make scroll bar change color when mouse is over it.
            //vScrollBar.hoverUpdate(mRect);
        }

        /*private float DoScrollBar(int id, Rectangle rectangle, Texture2D gripTexture, float max, float value, bool vertical)
        {
            //Position the grip relative on the scrollbar and make sure the grip stays inside the scrollbar
            //Note that the grip's width if vertical/height if horizontal is the scrollbar's smallest dimension.
            int gripPosition;
            Rectangle grip;
            Rectangle mRect = new Rectangle(curState.X, curState.Y, 10, 10);
            if (rectangle.Intersects(mRect))
            {
                hotItem = id;
                if (activeItem == -1 && curState.LeftButton == ButtonState.Pressed)
                    activeItem = id;
            }


            gripPosition = rectangle.Bottom - rectangle.Width - (int)((rectangle.Height - rectangle.Width) * (value / max));
            grip = new Rectangle(rectangle.Left, gripPosition, rectangle.Width, rectangle.Width);

            if (activeItem == id)
            {
                //same as horizontal bit in the end we inverse value,
                //because we want the bottom to be 0 instead of the top
                //while in y coordinates the top is 0.
                float mouseRelative = curState.Y - (rectangle.Y + grip.Height / 2);
                mouseRelative = Math.Min(mouseRelative, rectangle.Height - grip.Height);
                mouseRelative = Math.Max(0, mouseRelative);
                value = max - (mouseRelative / (rectangle.Height - grip.Height)) * max;
            }
            return value;
        }*/

        public void UI_Update(Rectangle mRect, bool mouseDown, MainGame game)
        {
            handle_keyboard();

            if (popMessage.ShowMessage == false) //check to turn tutorial off
                tutorialOn = false;

            //update buttons
            hintButton.Update(mouseDown, mRect);
            runButton.Update(mouseDown, mRect);
            exitButton.Update(mouseDown, mRect);
            compileButton.Update(mouseDown, mRect);

            if (hintButton.isPressed)
            {
                //messages[hintNum] = "hint #1";
                //messages[hintNum] = "hint #2";
                //popMessage.messages = messages;
                popMessage.ShowMessage = true;
            }
            if (exitButton.isPressed || Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.E))
            {
                puzzle_exit = true;
            }

            if ((runButton.isPressed || Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.R)))
            {
                codePuz1.updateNumCorrect();
                //get script errors from codePuzzle
                //specify location of message box ex. (100,100,50,50)
                //specify the dialogName
                //EX. codePuz1.createMsgObjects(game.Content, codePuz1.scriptErrors, dialogName, popMessage, 100, 100, 50, 50, game);
                //set popMessage.showMessage = true
            }
        }

        /// <summary>
        /// Handles the Update for the Puzzle Completion Flag
        /// </summary>
        public void AnswerUpdate(Game game)
        {
            puzzle_complete =  codePuz1.CheckAnswers(game, popMessage);
            if (codePuz1.attempts <= 0 && !codePuz1.showFail )//If Attempts is <=0 and the Puzzle is done showing Failure then exit the puzzle
            {
                puzzle_exit = true;
                puzzleFail = true;
            }
            else if (puzzle_complete)
            {
                puzzle_exit = true;
                updatePuzzlePieces();
            }
        }

        public string BackEndUpdate(Game game)
        {
               // return "Mouse(" + Mouse.GetState().X + ", " + Mouse.GetState().Y + ") Block(" + codeInvBlocks[0].Rect + " Inv Rect " + viewportINV.Bounds + " Game Rect " + viewportGame.Bounds;
            if (selectedBlock != null)
                outputBackEnd += " SelBlock " + selectedBlock.Rect;
            return outputBackEnd;
            //  return "Mouse(" + Mouse.GetState().X + ", " + Mouse.GetState().Y + ") Block() " + " Inv Rect " + viewportINV.Bounds + " Game Rect " + viewportGame.Bounds;
        }

       public void Draw(SpriteBatch spriteBatch,Game game, Viewport viewportGameplay, GameTime gameTime)
        {
            game.GraphicsDevice.Clear(Color.Gray);
            spriteBatch.Draw(puzzleScreen, new Rectangle(0, 0, game.Window.ClientBounds.Width, game.Window.ClientBounds.Height), new Rectangle(0, 0, puzzleScreen.Width, puzzleScreen.Height), Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.999f);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied);
            int viewOffsetX = viewportGameplay.Width - viewportINV.Width;
            int viewOffsetY = viewportGameplay.Height - viewportINV.Height;
            bool insideInventory = false;

            game.GraphicsDevice.Viewport = viewportINV;
            inventoryRec = new Rectangle(5, 0, ideGridWidth, ideGridHeight);
            scrollbarRec = new Rectangle(5, vScrollBar.Offset, ideGridWidth, ideGridHeight);
            spriteBatch.Draw(sheetOutline, inventoryRec, null, Color.White * 0.8f, 0f, Vector2.Zero, SpriteEffects.None, UI_boxDepth);

            foreach (CodeBlock child in codeInvBlocks)
            {
                //fix scroll bar to be based upon position of codeblock
                if (child.Position.X == child.Origin.X)
                {
                    child.setPos(new Vector2(child.Position.X,child.Origin.Y + vScrollBar.Offset));
                    child.Draw(spriteBatch);
                    spriteBatch.DrawString(spriteFont, child.getDisplayText(), new Vector2((int)(child.Position.X + 15), (int)(child.Position.Y + child.Rect.Height / 2 - 14)), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, child.StringDepth);
                }
            }
        //IDE Draw
            //Draw inventory, insert "Inventory" words at the top of inventory
            //Draw scrollBar
            vScrollBar.Draw(spriteBatch);

           //draw code blocks in inventory
           
            //Draw grid lines of code sheet, Also draw inventory to fit inside grid lines 
            int pixY = (int)inventoryRec.Y + (EmptyBlock.Height);
            /*for (int y = 0; pixY < 3; y++)
            {
                pixY = (int)inventoryRec.Y + (y * EmptyBlock.Height);
                //draw pieces initial pieces on screen
                gridLine = new Rectangle(inventoryRec.Left, pixY, inventoryRec.Width, 5);
                spriteBatch.Draw(gridPixel, gridLine, Color.White);
            }*/
            spriteBatch.End();

           //set spriteBatch to it's default spritesortmode
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied);

            DrawGamePlay(spriteBatch,game,viewportGameplay, gameTime);

            spriteBatch.End();
         }

       public void DrawGamePlay(SpriteBatch spriteBatch, Game game, Viewport viewportGameplay, GameTime gameTime)
       {
           //set viewport back to original
           game.GraphicsDevice.Viewport = viewportGameplay;

           //draw code sheet and text
           spriteBatch.Draw(grayBoxTexture, new Rectangle(codeRec.X + 2, codeRec.Y - 22, codeRec.Width - 10, 25), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, UI_boxDepth);
           spriteBatch.DrawString(spriteFont, "<CODE SHEET", new Vector2(codeRec.X + 2, codeRec.Top - 20), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, string_depth);
           spriteBatch.Draw(sheetOutline, codeRec, null, Color.White * 0.8f, 0, Vector2.Zero, SpriteEffects.None, UI_boxDepth);

           //draw inventory textures and text
           spriteBatch.Draw(grayBoxTexture, new Rectangle(viewportINV.X + 4, viewportINV.Y - 22, viewportINV.Width - 10, 22), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, UI_boxDepth);
           spriteBatch.DrawString(spriteFont, "<INVENTORY", new Vector2(viewportINV.X + 10, viewportINV.Y - 20), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, string_depth);

           spriteBatch.Draw(topOutline, topRec, null, Color.White * 0.8f, 0, Vector2.Zero, SpriteEffects.None, UI_boxDepth);
           //write name of code puzzle
           spriteBatch.DrawString(font_large, codePuz1.Name, new Vector2(topRec.Right - 300, topRec.Y), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, string_depth);
           //draw hint/objective box
           spriteBatch.Draw(msgOutline, msgRec, null, Color.White * 0.8f, 0f, Vector2.Zero, SpriteEffects.None, UI_boxDepth);
           //display objective on screen.
           string objText = codePuz1.WrapText(codePuz1.objectiveText,msgRec,spriteFont,ref highlight_words);
           spriteBatch.DrawString(spriteFont, objText, new Vector2(msgRec.X + 10, msgRec.Y + 5), Color.Red, 0f, Vector2.Zero, 1f, SpriteEffects.None, string_depth);
           spriteBatch.DrawString(font_large, "Attempts Remaining: " + codePuz1.attempts, new Vector2(runButton.buttonRect.X + 100, runButton.buttonRect.Y), Color.Red, 0f, Vector2.Zero, 1f, SpriteEffects.None, string_depth);
           //Draw code puzzle
           codePuz1.Draw(gameTime,spriteBatch);

           foreach (CodeBlock child in codeInvBlocks)
           {
               if (child.Position.X != child.Origin.X)
               {
                   if (child.Position.Y != child.Origin.Y)
                   {
                       child.Draw(spriteBatch);
                   spriteBatch.DrawString(spriteFont, child.getDisplayText(), new Vector2((int)(child.Position.X + 15), (int)(child.Position.Y + child.Rect.Height / 2 - 14)), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, child.StringDepth);
                   }
               }
           }

           //Draw Buttons
           hintButton.Draw(spriteBatch);
           runButton.Draw(spriteBatch);
           exitButton.Draw(spriteBatch);
       }

    }
}
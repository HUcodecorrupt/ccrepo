﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeCorrupt.Gameplay.Puzzle;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using CodeCorrupt.LevelEditor;
using CodeCorrupt.Gameplay.Character;

namespace CodeCorrupt.Gameplay.Character
{
    public class Enemy
    {
        public int enemyoffset = 20;
        public float enemyspeed = 5;
        Texture2D enemy;
        public Vector2 enemyPos;
        public Vector2 e_prevpos;
        public Vector2 origPos;
        Point enemy1Size = new Point(140, 114);
        Point frameSize = new Point(140, 113);
        Point enemy1frame = new Point(0, 0);
        Point enemy1sheet = new Point(5, 1);
        Character mainChar; 
        int wander_dis = 0;
        bool wandering = false;
        public int health = 30;
        public bool E_IsAlive = true;
        const float StartledDistance = 400.0f;
        float hysteriadistance;
        Character get_main;
        public Enemy(Game game)
        {
            game.TargetElapsedTime = new System.TimeSpan(0, 0, 0, 0, 80);
            enemy = game.Content.Load<Texture2D>(@"Images/trojan");
        }
        public Enemy (Vector2 enemypos, Texture2D enemysprite, Point e_size)
        {
            enemyPos = enemypos;
            origPos = enemypos;
            enemy = enemysprite;
            enemy1Size = e_size;
        }

        public Enemy(Game game, Vector2 enemypos, Texture2D enemysprite, Point e_size)
        {
            game.TargetElapsedTime = new System.TimeSpan(0, 0, 0, 0, 80);
            enemyPos = enemypos;
            origPos = enemypos;
            enemy = enemysprite;
            enemy1Size = e_size;
        }

        public void LoadContent(Game game)
        {
            enemy = game.Content.Load<Texture2D>(@"Images/trojan"); // Loaging the sprite
            enemyPos.X = enemyPos.X + 500;
            enemyPos.Y = enemyPos.Y + 350;
        }
        public bool Collide(LevelObject obj)
        {
            Rectangle Rect = new Rectangle((int)enemyPos.X + enemyoffset,
            (int)enemyPos.Y + enemyoffset, enemy1Size.X - (enemyoffset * 2), enemy1Size.Y - (enemyoffset * 2));
            Rectangle objectRect = obj.GetDrawBox();
            return Rect.Intersects(objectRect);
        }
        public bool Collide(Shot sh)
        {
            Rectangle Recta = new Rectangle((int)enemyPos.X + enemyoffset,
            (int)enemyPos.Y + enemyoffset, enemy1Size.X - (enemyoffset * 2), enemy1Size.Y - (enemyoffset * 2));
            return Recta.Intersects(sh.Shot_Rec);
        }
        public bool Collide(Wall wall)
        {
            Rectangle Rect = new Rectangle((int)enemyPos.X + enemyoffset,
            (int)enemyPos.Y + enemyoffset, enemy1Size.X - (enemyoffset * 2), enemy1Size.Y - (enemyoffset * 2));
            Rectangle wallBox = new Rectangle((int)wall.point1.X, (int)wall.point1.Y, Math.Abs((int)(wall.point1.X - wall.point2.X)), Math.Abs((int)(wall.point1.Y - wall.point2.Y)));
            return Rect.Intersects(wallBox);
        }
        public void Pursue_Hys(Character mainCharacter)
        {
            hysteriadistance = Vector2.Distance(mainCharacter.getRingsPosition(), enemyPos); //continue to calculate distance
            if (hysteriadistance < StartledDistance - hysteriadistance)
            {
                wandering = false;
                //CHASING UPDATE - Hard Code
                Vector2 direction = mainCharacter.getRingsPosition() - enemyPos;
                direction.Normalize();
                Vector2 velocity = direction * enemyspeed;
                
                //++enemy1frame.X;
                //if (enemy1frame.X >= enemy1sheet.X)
                //{
                //    enemy1frame.X = 0;
                //    ++enemy1frame.Y;
                //    if (enemy1frame.Y >= enemy1sheet.Y)
                //        enemy1frame.Y = 0;
                //}
                enemyPos += velocity;
                // Statement END
            }
        }
        public void Wander_Hys(Character mainCharacter)
        {
            hysteriadistance = Vector2.Distance(mainCharacter.getRingsPosition(), enemyPos); //continue to calculate distance
            if (hysteriadistance > StartledDistance + hysteriadistance)
            {
                wandering = true;
            }
        }

        public void Update(GameTime gameTime)
        {
            e_prevpos = enemyPos;
            //Evade_Hys(get_main);
            //Wander_Hys(get_main);
            //Pursue_Hys(get_main);
            /*while (wandering)
            {
                if (wander_dis > 10) // Dummy Behavior
                {
                    wander_dis--;
                    enemyPos.X += enemyspeed;
                    if (wander_dis == 10)
                    {
                        wander_dis = 0;
                    }

                }
                if (wander_dis < 10)
                {
                    wander_dis++;
                    enemyPos.X -= enemyspeed;
                    if (wander_dis == 10)
                    {
                        wander_dis = 20;
                    }
                }
            }
            */
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(enemy, enemyPos, new Rectangle(enemy1frame.X * enemy1Size.X, enemy1frame.Y * enemy1Size.Y, enemy1Size.X, enemy1Size.Y), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, .3f);
            spriteBatch.Draw(enemy, new Rectangle((int)enemyPos.X, (int)enemyPos.Y, enemy1Size.X, enemy1Size.Y), new Rectangle(enemy1frame.X * frameSize.X, enemy1frame.Y * frameSize.Y, frameSize.X, frameSize.Y), Color.White, 0f, Vector2.Zero, SpriteEffects.None, .031f);
        }
        public Point getEnemyFrameSize()
        { return enemy1Size; }

        public Texture2D getEnemyTexture()
        {
            return enemy;
        }

        public Rectangle getRectangle()
        {
           return new Rectangle((int)enemyPos.X + enemyoffset,
           (int)enemyPos.Y + enemyoffset, enemy1Size.X - (enemyoffset * 2), enemy1Size.Y - (enemyoffset * 2));
        }

        public Point getCenter()
        {
            return new Point((int)enemyPos.X + enemy1Size.X / 2, (int)enemyPos.Y + enemy1Size.Y / 2);
        }
        public void set_e_pos(Vector2 vect)
        {
            enemyPos += vect * 35;
        }
    }
}

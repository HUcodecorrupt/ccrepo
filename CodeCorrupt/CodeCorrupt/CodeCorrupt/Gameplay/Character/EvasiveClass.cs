﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeCorrupt.Gameplay.Puzzle;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using CodeCorrupt.LevelEditor;
using CodeCorrupt.Gameplay.Character;
namespace CodeCorrupt.Gameplay.Character
{
    public class EvasiveClass : Enemy
    {
        public int enemyoffset = 5;
        public float enemyspeed = 5;
        Texture2D e_22;
        public Vector2 e_22pos = Vector2.Zero;
        public Vector2 e_22prevpos;
        public Vector2 origPos;
        Point enemy2Size = new Point(71, 64); //tempsize
        Point enemy2frame = new Point(0, 0);
        Point enemy2sheet = new Point(1, 1);//need to change for animation
        Character mainChar;
        int wander_dis = 0;
        bool wandering = false;
        public int health = 30;
        public bool E_IsAlive = true;
        const float StartledDistance = 400.0f;
        float hysteriadistance;
        public EvasiveClass(Game game)
            :base(game)
        {
            game.TargetElapsedTime = new System.TimeSpan(0, 0, 0, 0, 80);
        }
        public void LoadContent(Game game)
        {
            e_22 = game.Content.Load<Texture2D>(@"Images/Malware"); // Loaging the sprite
        }

        public EvasiveClass(Vector2 enemypos, Texture2D enemysprite, Point e_size)
            :base(enemypos,enemysprite,e_size)
        {
            e_22pos = enemypos;
            origPos = enemypos;
            e_22 = enemysprite;
            enemy2Size = e_size;
        }

        public EvasiveClass(Game game, Vector2 enemypos, Texture2D enemysprite, Point e_size)
            :base(game)
        {
            game.TargetElapsedTime = new System.TimeSpan(0, 0, 0, 0, 80);
            e_22pos = enemypos;
            origPos = enemypos;
            e_22 = enemysprite;
            enemy2Size = e_size;
        }
        public bool Collide(LevelObject obj)
        {
            Rectangle Rect = new Rectangle((int)e_22pos.X + enemyoffset,
            (int)e_22pos.Y + enemyoffset, enemy2Size.X - (enemyoffset * 2), enemy2Size.Y - (enemyoffset * 2));
            Rectangle objectRect = obj.GetDrawBox();
            return Rect.Intersects(objectRect);
        }
        public bool Collide(Shot sh)
        {
            Rectangle Recta = new Rectangle((int)e_22pos.X + enemyoffset,
            (int)e_22pos.Y + enemyoffset, enemy2Size.X - (enemyoffset * 2), enemy2Size.Y - (enemyoffset * 2));
            return Recta.Intersects(sh.Shot_Rec);
        }
        public void Evade_Hys(Character mainCharacter)
        {
            hysteriadistance = Vector2.Distance(mainCharacter.getRingsPosition(), enemyPos); //continue to calculate distance
            if (hysteriadistance < StartledDistance - hysteriadistance)
            {
                //Evading UPDATE - Hard Code
                Vector2 direction;
                //Vector2.Reflect(enemyPos, mainCharacter.getRingsPosition());
                //Vector2 velocity = direction * enemyspeed;
                //direction.Reflect(direction,velocity);
                direction = Vector2.Reflect(e_22pos, mainCharacter.getRingsPosition());
                direction = Vector2.Normalize(e_22pos - mainCharacter.getRingsPosition());
                Vector2 velocity = direction * enemyspeed;
                enemyPos += velocity;
                //set_e_pos(direction.X, direction.Y);
                //enemyPos -= velocity;
                wandering = false;
            }
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(e_22, e_22pos, new Rectangle(enemy2frame.X * enemy2Size.X, enemy2frame.Y * enemy2Size.Y, enemy2Size.X, enemy2Size.Y), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, .3f);
        }

    }
}

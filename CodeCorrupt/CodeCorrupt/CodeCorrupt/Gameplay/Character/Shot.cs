﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeCorrupt.Gameplay.Puzzle;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using CodeCorrupt.LevelEditor;
namespace CodeCorrupt.Gameplay.Character
{
    public class Shot
    {
        public Texture2D DrawTexture { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Direction { get; set; }
        public float Speed { get; set; }
        public int ActiveTime { get; set; }
        public int TotalActiveTime { get; set; }
        public int EndTime;
        public int Damage = 15;
        public bool shotExists = true, hitsPlayer, hitsEnemy;
        public Rectangle Shot_Rec;
        Point ballframe = new Point(0, 0);
        Point ballsheet = new Point(4, 0);
        Point ballsize = new Point(52, 66); //size of the shot in the texture for the source rectangle
        Point shotSize; //size of the shot on screen for the destination rectangle
        Texture2D energyball, eball;
        Point prevPosition;
        public Shot(Texture2D texture, Vector2 position, Vector2 direction, float speed, int activeTime, Game game)
        {
             this.DrawTexture = texture;
             this.Position = position;
             this.Direction = direction;
             this.Speed = speed;
             this.EndTime = activeTime;
             this.TotalActiveTime = 0;
             this.Shot_Rec = new Rectangle((int)position.X, (int)position.Y, 68, 69);
             this.shotSize = ballsize;
             game.TargetElapsedTime = new System.TimeSpan(0, 0, 0, 0, 80);
             //LoadContent(game);
        }

        public Shot(Texture2D texture, Vector2 position, Point BallSize, Point ShotSize, Vector2 direction, float speed, int activeTime, Game game)
        {
            this.DrawTexture = texture;
            this.Position = position;
            this.ballsize = BallSize;
            this.shotSize = ShotSize;
            this.Direction = direction;
            this.Speed = speed;
            this.EndTime = activeTime;
            this.TotalActiveTime = 0;
            this.Shot_Rec = new Rectangle((int)position.X, (int)position.Y, shotSize.X, shotSize.Y);
            game.TargetElapsedTime = new System.TimeSpan(0, 0, 0, 0, 80);
            //LoadContent(game);
        }

        public Shot(Texture2D texture, Vector2 position, Point BallSize, Point ShotSize, Vector2 direction, float speed, int activeTime, Game game, bool hitsYou, bool hitsThem)//used to fire turrets to hit player
        {
            this.DrawTexture = texture;
            this.Position = position;
            this.ballsize = BallSize;
            this.shotSize = ShotSize;
            this.Direction = direction;
            this.Speed = speed;
            this.EndTime = activeTime;
            this.TotalActiveTime = 0;
            this.Shot_Rec = new Rectangle((int)position.X, (int)position.Y, shotSize.X, shotSize.Y);
            game.TargetElapsedTime = new System.TimeSpan(0, 0, 0, 0, 80);
            this.hitsPlayer = hitsYou;
            this.hitsEnemy = hitsThem;
            //LoadContent(game);
        }

        public void LoadContent(Game game)
        {
            energyball = game.Content.Load<Texture2D>(@"Images/new_kurai_mephiles_sprites_by_epicsprites-d5xql5a"); // Loaging the sprite
            eball = energyball;
        }

        public void Update(GameTime gameTime)
        {
            if (TotalActiveTime > EndTime)
            {
                shotExists = false;
            }
             this.Position += Direction * Speed;
             this.Shot_Rec = new Rectangle((int)Position.X, (int)Position.Y, shotSize.X, shotSize.Y); 
             this.TotalActiveTime += gameTime.ElapsedGameTime.Milliseconds;
             if (ballframe.X < ballsheet.X)
             {
                 ++ballframe.X;
             }
             else
             {
                 if (ballframe.Y < ballsheet.Y)
                 {
                     ++ballframe.Y;
                 }
                 else
                 {
                     ballframe.Y = 0;
                 }
                 ballframe.X = 0;
             }
        }
        public Point getBallSize()
        { return ballsize; }
        public void Draw(SpriteBatch spriteBatch)
        {
             //spriteBatch.Draw(DrawTexture,Position,null,Color.White,0f,new Vector2(DrawTexture.Width / 2,DrawTexture.Height / 2),1.0f,SpriteEffects.None,1.0f);
             //spriteBatch.Draw(DrawTexture, Position, new Rectangle(ballframe.X*ballsize.X, ballframe.Y*ballsize.Y, ballsize.X, ballsize.Y), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, .031f);
            spriteBatch.Draw(DrawTexture, new Rectangle((int)Position.X, (int)Position.Y, shotSize.X, shotSize.Y), new Rectangle(ballframe.X * ballsize.X, ballframe.Y * ballsize.Y, ballsize.X, ballsize.Y), Color.White, 0f, Vector2.Zero, SpriteEffects.None, .031f);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeCorrupt.Gameplay.Puzzle;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using CodeCorrupt.Gameplay.Character;
using CodeCorrupt.LevelEditor;

namespace CodeCorrupt.Gameplay.Character
{
    class SpriteClass
    {
        Texture2D textureImage; // sprite or sprite sheet of image drawn
        Texture2D enemy;
        protected Point frameSize; // Size of each individual frame
        int collisionOffset; // modify rectangle
        public Point currentFrame; // current frame index
        public Point sheetSize; // number of columns and rows in sheet
        public int timeSinceLastFrame; // number of milliseconds since last frame
        int millisecsPerFrame = 0; // milliseconds of wait time
        public Vector2 speed; // x and y speed
        protected Vector2 position; // Position to draw sprite
        const int defaultMillisecondsPerFrame = 16;
        
        public SpriteClass(Texture2D textureImage, Vector2 position, Point frameSize,
        int collisionOffset, Point currentFrame, Point sheetSize, Vector2 speed)
        : this(textureImage, position, frameSize, collisionOffset, currentFrame,
        sheetSize, speed, defaultMillisecondsPerFrame)
        {
        }

        public SpriteClass(Texture2D textureImage, Vector2 position, Point frameSize,
        int collisionOffset, Point currentFrame, Point sheetSize, Vector2 speed,
        int millisecondsPerFrame)
        {
            this.textureImage = textureImage;
            this.position = position;
            this.frameSize = frameSize;
            this.collisionOffset = collisionOffset;
            this.currentFrame = currentFrame;
            this.sheetSize = sheetSize;
            this.speed = speed;
            this.millisecsPerFrame = millisecsPerFrame;
        }
        public virtual void Update(GameTime gameTime, Rectangle clientBounds)
        {
            timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastFrame > millisecsPerFrame)
            {
                timeSinceLastFrame = 0;
                ++currentFrame.X;
                if (currentFrame.X >= sheetSize.X)
                {
                    currentFrame.X = 0;
                    ++currentFrame.Y;
                    if (currentFrame.Y >= sheetSize.Y)
                        currentFrame.Y = 0;
                }
            }
        }
        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(textureImage,
            position,
            new Rectangle(currentFrame.X * frameSize.X,
            currentFrame.Y * frameSize.Y,
            frameSize.X, frameSize.Y),
            Color.White, 0, Vector2.Zero,
            1f, SpriteEffects.None, 0);
        }
        //public abstract Vector2 direction
        //{
        //    get;
        //}
        public Rectangle collisionRect
        {

            get
                {
                return new Rectangle(
                (int)position.X + collisionOffset,
                (int)position.Y + collisionOffset,
                frameSize.X - (collisionOffset * 2),
                frameSize.Y - (collisionOffset * 2));
                }
        }
    }
}

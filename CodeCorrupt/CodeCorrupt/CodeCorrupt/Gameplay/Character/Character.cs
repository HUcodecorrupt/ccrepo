﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeCorrupt.Gameplay.Puzzle;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using CodeCorrupt.LevelEditor;
namespace CodeCorrupt.Gameplay.Character
{
    public class Character
    {
        public Vector2 ringsPosition = new Vector2(1000,1000); // position and speed
        public Vector2 ballPosition = Vector2.Zero;
        public float ringsSpeed = 22;
        public float ballspeed = 11;
        Texture2D spriteleft,spriteright,spriteup,spritedown,spritekey, sp;  //For the sprite texture
        public bool isDpressed = false,canShoot;
        public int e_offset = 1;
        public int max_health,c_health,rechargeTime=500,rechargeLeft=0;
        bool O_set = false;
        Shot shot;
        SoundEffect shoot;
        Point frameSize = new Point(74, 108); //Labeling the frame size and rate=
        Point currentFrame = new Point(0, 0);
        Point sheetSize = new Point(4, 1); // edited based on sprites
        public Point charSize = new Point(74, 108);
        Rectangle x;
        Texture2D sp2;
        public string shoot_direction;
       // int charTimeSinceLastFrame = 0;
        const int charMillisecondsPerFrame = 10;
        public int ringsCollisionRectOffset = 27;
        //HealthBar health;
        public Vector2 prevPosition;
        private List<PuzzlePiece> inventory = new List<PuzzlePiece>();
        public Character(Game game)
        {
            game.TargetElapsedTime = new System.TimeSpan(0, 0, 0, 0, 80);
        }

        public void LoadContent(Game game)
        {
            sp = game.Content.Load<Texture2D>(@"Images/main"); // Loaging the sprite
            spriteleft = game.Content.Load<Texture2D>(@"Images/sprites_left"); // Loaging the sprite
            spriteright = game.Content.Load<Texture2D>(@"Images/sprites_right"); // Loaging the sprite
            spriteup = game.Content.Load<Texture2D>(@"Images/sprites_up"); // Loaging the sprite
            spritedown = game.Content.Load<Texture2D>(@"Images/sprites_down"); // Loaging the sprite
            //spritekey = game.Content.Load<Texture2D>(@"Images/wait state"); // Loaging the sprite
            sp2 = game.Content.Load<Texture2D>(@"Images/main"); // Loaging the sprite;
            max_health = 100;
            c_health = max_health;
            shoot = game.Content.Load<SoundEffect>(@"SoundFX/shot"); // Loaging the sprite;
            SoundEffectInstance instanceA = shoot.CreateInstance();
            //health.fullhealth = max_health;
            //health.currenthealth = max_health;
        }

       public bool Collide(LevelObject obj)
        {
            Rectangle Rect = new Rectangle((int)ringsPosition.X + ringsCollisionRectOffset,
            (int)ringsPosition.Y + ringsCollisionRectOffset, getFrameSize().X - (ringsCollisionRectOffset *2), getFrameSize().Y - (ringsCollisionRectOffset * 2));
            Rectangle objectRect = obj.GetDrawBox();
            return Rect.Intersects(objectRect);
        }
       public bool E_Collide(Enemy danger)
       {
           Rectangle C_Rect = new Rectangle((int)ringsPosition.X + e_offset,
           (int)ringsPosition.Y + e_offset, getFrameSize().X - (e_offset * 2), getFrameSize().Y - (e_offset * 2));
           Rectangle enemy_rec = danger.getRectangle();
           return C_Rect.Intersects(enemy_rec);

       }

        public void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            prevPosition = ringsPosition;
            if (isDpressed == true) { shoot.Play(); shot.Update(gameTime); }
            if (keyboardState.IsKeyDown(Keys.Left))
            {
                isDpressed = false;
                O_set = false;
                sp = spriteleft;  // THIS WILL BE ADDED WHEN WE HAVE A CHARACTER
                ++currentFrame.X;
                if (currentFrame.X >= sheetSize.X)
                {
                    currentFrame.X = 0;
                    ++currentFrame.Y;
                    if (currentFrame.Y >= sheetSize.Y)
                        currentFrame.Y = 0;
                }
                ringsPosition.X -= ringsSpeed; //sprites going left
                shoot_direction = "left";
                O_set = true;
            }
             else if (keyboardState.IsKeyDown(Keys.Right))
            {
                isDpressed = false;
                O_set = false;
                sp = spriteright; // THIS WILL BE ADDED WHEN WE HAVE A CHARACTER
                ++currentFrame.X;
                if (currentFrame.X >= sheetSize.X)
                {
                    currentFrame.X = 0;
                    ++currentFrame.Y;
                    if (currentFrame.Y >= sheetSize.Y)
                        currentFrame.Y = 0;
                }
                ringsPosition.X += ringsSpeed; //sprites going right
                shoot_direction = "right";
                O_set = true;
            }
             else if (keyboardState.IsKeyDown(Keys.Up))
            {
                isDpressed = false;
                O_set = false;
                sp = spriteup; // THIS WILL BE ADDED WHEN WE HAVE A CHARACTER
                ++currentFrame.X;
                if (currentFrame.X >= sheetSize.X)
                {
                    currentFrame.X = 0;
                    ++currentFrame.Y;
                    if (currentFrame.Y >= sheetSize.Y)
                        currentFrame.Y = 0;
                }
                ringsPosition.Y -= ringsSpeed; //sprites going up
                shoot_direction = "up";
                O_set = true;
            }
             else if(keyboardState.IsKeyDown(Keys.Down))
            {
                isDpressed = false;
                O_set = false;
                sp = spritedown;  // THIS WILL BE ADDED WHEN WE HAVE A CHARACTER
                ++currentFrame.X;
                if (currentFrame.X >= sheetSize.X)
                {
                    currentFrame.X = 0;
                    ++currentFrame.Y;
                    if (currentFrame.Y >= sheetSize.Y)
                        currentFrame.Y = 0;
                }
                ringsPosition.Y += ringsSpeed; //sprites going down
                shoot_direction = "down";
                O_set = true;
            }
             else if (O_set == true)
            {
                
                sp = sp2;
                ringsPosition.X = ringsPosition.X + 10;
                O_set = false;
                currentFrame.X = 0;
                currentFrame.Y = 0;
                isDpressed = false;
            }

            if (!this.canShoot)
            {
                this.rechargeLeft -= gameTime.ElapsedGameTime.Milliseconds;
                if (rechargeLeft <= 0)
                {
                    this.canShoot = true;
                }
            }
        }
        
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
                spriteBatch.Draw(sp, ringsPosition, new Rectangle(currentFrame.X * frameSize.X, currentFrame.Y * frameSize.Y, frameSize.X, frameSize.Y), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, .4f);
        
        }
        //Getters And Setters
        public Vector2 getRingsPosition()
        { return ringsPosition; }
        public void setRingsPosition(float x, float y)
        {
            if (x < 0) { ringsPosition.X = (ringsPosition.X + 35); }
            if (x > 0) { ringsPosition.X = (ringsPosition.X - 35); }
            if (y < 0) { ringsPosition.Y = (ringsPosition.Y - 35); }
            if (y < 0) { ringsPosition.Y = (ringsPosition.Y + 35); }
        }
        public List<PuzzlePiece> getInventory()
        { return inventory; }
        public Point getFrameSize()
        { return frameSize; }
        public int getHealth()
        { return c_health; }

        public Rectangle getRectangle()
        {
            return new Rectangle(((int)ringsPosition.X - 7) + (ringsCollisionRectOffset-7),
            ((int)ringsPosition.Y - 7) + (ringsCollisionRectOffset-7), (getFrameSize().X -7) - ((ringsCollisionRectOffset -7) * 2), (getFrameSize().Y -7) - ((ringsCollisionRectOffset-7) * 2));
        }

        public Point getCenter()
        {
            return new Point((int)ringsPosition.X + frameSize.X / 2, (int)ringsPosition.Y + frameSize.Y / 2);
        }

    }   

}
